(define-sort Integer () (Tuple Int Int))

(define-fun integer.mkZero ()  Integer
	    (tuple 0 0 ))

(define-fun integer.mkOne ()  Integer
	    (tuple 1 1))

(define-fun integer ((i Integer) (x Int) (y Int)) Bool
	    (and
	     (<= x y)
	     (= i (tuple x y))))

(define-fun integer.singleton ((i Integer) (x Int))  Bool
	    (integer i x x))

(define-fun integer.zero ((i Integer))  Bool
	    (integer.singleton i 0))

(define-fun integer.one ((i Integer))  Bool
	    (integer.singleton i 1))

(define-fun integer.range ((i Integer) (r Integer)) Bool
	    (exists ((a Int) (b Int))
		    (and
		     (integer i a b)
		     (integer.singleton r (+ 1 (- b a))))))


(define-fun integer.min ((i Integer) (r Integer)) Bool
	    (exists ((a Int) (b Int))
		    (and
		     (integer i a b)
		     (integer r a a))))

(define-fun integer.max ((i Integer) (r Integer)) Bool
	    (exists ((a Int) (b Int))
		    (and
		     (integer i a b)
		     (integer r b b))))


(define-fun integer.minOf ((i Integer) (j Integer) (r Integer)) Bool
	    (exists ((a Int) (b Int) (c Int) (d Int))
		    (and
		     (integer i a b)
		     (integer j c d)
		     (ite (< a c)
			  (integer r a a)
			  (integer r c c)
			  ))))

(define-fun integer.maxOf ((i Integer) (j Integer) (r Integer)) Bool
	    (exists ((a Int) (b Int) (c Int) (d Int))
		    (and
		     (integer i a b)
		     (integer j c d)
		     (ite (> b d)
			  (integer r b b)
			  (integer r d d)
			  ))))

(define-fun integer.contains ((i Integer) (j Integer)) Bool
	    (exists ((a Int) (b Int) (c Int) (d Int))
		    (and
		     (integer i a b)
		     (integer j c d)
		     (<= a d)
		     (<= c b))))

(define-fun integer.contains0 ((i Integer)) Bool
	    (exists ((a Int) (b Int))
		    (and
		     (integer i a b)
		     (<= a 0)
		     (<= 0 b))))

(define-fun integer.sign ((i Integer) (r Integer)) Bool
	    (exists ((a Int) (b Int) (sa Int) (sb Int))
		    (and
		     (integer i a b)
		     (= sa (ite (< a 0) (- 1) (ite (= a 0) 0 1)))
		     (= sb (ite (< b 0) (- 1) (ite (= b 0) 0 1)))
		     (integer r sa sb))))

(define-fun integer.union ((i Integer) (j Integer) (r Integer)) Bool
	    (exists ((a Int) (b Int))
		    (and
		     (exists ((t Integer))
			      (and (integer.singleton t a) (integer.minOf i j t)))
		     (exists ((t Integer))
			      (and (integer.singleton t b) (integer.maxOf i j t)))
		     (integer r a b))))

(define-fun integer.intersection ((i Integer) (j Integer) (r Integer)) Bool
	    (exists ((a Int) (b Int) (c Int) (d Int) (x Int) (y Int))
		    (and
		     (integer i a b)
		     (integer j c d)
		     (= x (ite (< a c) c a))
		     (= y (ite (< b d) b d))
		     (integer r x y))))

;; a value in the range of the integer
(define-fun integer.value ((i Integer) (r Int))  Bool
	    (exists ((s Integer))
		    (and
		     (integer.singleton s r)
		     (integer.contains i s))))

(define-fun integer.lt ((i Integer) (j Integer)) Bool
	    (exists ((a Int) (b Int))
		    (and
		     (integer.value i a)
		     (integer.value j b)
		     (< a b))))

(define-fun integer.lte ((i Integer) (j Integer)) Bool
	    (exists ((a Int) (b Int))
		    (and
		     (integer.value i a)
		     (integer.value j b)
		     (<= a b))))

(define-fun integer.gt ((i Integer) (j Integer)) Bool
	    (exists ((a Int) (b Int))
		    (and
		     (integer.value i a)
		     (integer.value j b)
		     (> a b))))

(define-fun integer.gte ((i Integer) (j Integer)) Bool
	    (exists ((a Int) (b Int))
		    (and
		     (integer.value i a)
		     (integer.value j b)
		     (>= a b))))

		     
(define-fun integer.add ((i Integer) (j Integer) (r Integer)) Bool
	    (exists ((a Int) (b Int) (c Int) (d Int))
		    (and
		     (integer i a b)
		     (integer j c d)
		     (integer r (+ a c) (+ b d)))))

(define-fun integer.neg ((i Integer) (r Integer)) Bool
	    (exists ((a Int) (b Int))
		    (and
		     (integer i a b)
		     (integer r (- b) (- a)))))

(define-fun integer.sub ((i Integer) (j Integer) (r Integer)) Bool
	    (exists ((jj Integer))
		    (and
		     (integer.neg j jj)
		     (integer.add i jj r))))

(define-fun integer.succ ((i Integer) (r Integer)) Bool
	    (exists ((one Integer))
		   (and
		    (integer.one one)
		    (integer.add i one r))))

(define-fun integer.pred ((i Integer) (r Integer)) Bool
	    (exists ((one Integer))
		   (and
		    (integer.one one)
		    (integer.sub i one r))))


(declare-const x Integer)
(assert (integer x 1 10))
(declare-const y Integer)
(assert (integer y (- 1) 5))
(push)
(assert (exists ((t Integer)) (and
			       (integer.singleton t 2) (integer.min x t))))
(check-sat)
(pop)

(push)
(assert (exists ((t Integer)) (and
			       (integer.singleton t 1) (integer.min x t))))
(check-sat)
(pop)

(push)
(declare-const r Integer)
(assert (integer.add x y r))
(assert (exists ((t Integer)) (and
			       (integer.singleton t 0) (integer.min r t))))
(check-sat)
(get-value (r))
(pop)

(push)
(declare-const r Integer)
(assert (integer.union x y r))
(check-sat)
(get-value (r))
(pop)

(push)
(declare-const r Integer)
(declare-const s Integer)
(declare-const t Integer)
(declare-const i Integer)
(declare-const sgn Integer)
(assert (integer.sub x y r))
(assert (integer.sub y x s))
(assert (integer.range s t))
(assert (integer.sign r sgn))
(assert (integer.intersection r s i))
(check-sat)
(get-value (r))
(get-value (s))
(get-value (t))
(get-value (sgn))
(get-value (i))
(pop)

