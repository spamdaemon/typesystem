#include <mylang/typesystem/eval/Eval.h>
#include <mylang/Integer.h>
#include <mylang/Self.h>
#include <mylang/typesystem/RBTree.h>
#include <mylang/typesystem/RBTree.h>
#include <mylang/typesystem/types/BType.h>
#include <mylang/typesystem/types/IType.h>
#include <mylang/typesystem/types/LType.h>
#include <mylang/typesystem/TypeDefinition.h>
#include <mylang/typesystem/typesystem.h>
#include <stddef.h>
#include <cassert>
#include <memory>
#include <vector>
#include <mylang/typesystem/exprs/ConditionalExpr.h>
#include <mylang/typesystem/exprs/TLiteral.h>
#include <mylang/typesystem/exprs/TypeExpr.h>
#include <mylang/typesystem/exprs/ConstructExpr.h>
#include <mylang/typesystem/exprs/ErrorExpr.h>
#include <mylang/typesystem/Environment.h>
#include <mylang/typesystem/types/SType.h>
#include <mylang/typesystem/VarName.h>
#include <iostream>
#include <utility>
#include <typeinfo>
#include <mylang/typesystem/exprs/BLiteral.h>
#include <mylang/typesystem/exprs/BuiltinExpr.h>
#include <mylang/typesystem/exprs/ExprTransform.h>
#include <mylang/typesystem/exprs/ILiteral.h>
#include <mylang/typesystem/exprs/ListLiteral.h>
#include <mylang/typesystem/exprs/VarExpr.h>

using namespace mylang;
using namespace mylang::typesystem;
using namespace mylang::typesystem::exprs;
using namespace mylang::typesystem::eval;
using namespace mylang::typesystem::types;

static Tx<ExprPtr> evaluate(ExprPtr e, const Environment &env)
{
  auto res = Eval::eval(e, env);
  ::std::cerr << "EVAL RESULT " << typeid(*res.actual).name() << ::std::endl;
  return res;
}

static Environment makeEnv(const ::std::vector<::std::pair<VarName, ExprPtr>> &v)
{
  Environment env;
  env.bindings = Environment::Bindings::of(v);
  return env;
}

static void expectNoChange(ExprPtr e, const Environment &env = Environment())
{
  auto res = evaluate(e, env);
  assert(res.isNew == false);
}

static ::std::shared_ptr<const TypeExpr> expectTypeExpr(ExprPtr e, const Environment &env)
{
  auto res = evaluate(e, env);
  auto ty = res.actual->self<TypeExpr>();
  assert(ty);
  return ty;
}

static void expectVariable(ExprPtr e, const VarName &name, TypePtr type, const Environment &env =
    Environment(), bool expectNew = true)
{
  auto res = evaluate(e, env);
  auto var = res.actual->self<VarExpr>();
  assert(res.isNew == expectNew);
  assert(var);
  assert(var->name == name);
  assert(var->type->unifiable(type));
}

static void expectILiteral(ExprPtr e, Integer i, const Environment &env = Environment(),
    bool expectNew = true)
{
  auto res = evaluate(e, env);
  auto literal = res.actual->self<ILiteral>();
  assert(res.isNew == expectNew);
  assert(literal);
  assert(literal->value == i);
}

static void expectBLiteral(ExprPtr e, bool b, const Environment &env = Environment(),
    bool expectNew = true)
{
  auto res = evaluate(e, env);
  auto literal = res.actual->self<BLiteral>();

  assert(res.isNew == expectNew);
  assert(literal);
  assert(literal->value == b);
}

static void expectIList(ExprPtr e, const ::std::vector<Integer> &list, const Environment &env =
    Environment(), bool expectNew = true)
{
  auto res = evaluate(e, env);
  auto literal = res.actual->self<ListLiteral>();
  assert(res.isNew == expectNew);
  assert(literal);
  assert(literal->entries.size() == list.size());
  for (size_t i = 0; i < list.size(); ++i) {
    auto x = literal->entries.at(i)->self<ILiteral>();
    assert(x);
    assert(x->value == list.at(i));
  }
}
static void expectError(ExprPtr e, const Environment &env = Environment())
{
  auto res = evaluate(e, env);
  auto ty = res.actual->self<ErrorExpr>();
  assert(ty);
}

void utest_eval_and()
{
  {
    auto e = BuiltinExpr::andOf(BuiltinExpr::andOf(BLiteral::of(true), BLiteral::of(true)),
        BLiteral::of(true));
    expectBLiteral(e, true);
  }
  {
    auto e = BuiltinExpr::andOf(BuiltinExpr::andOf(BLiteral::of(true), BLiteral::of(false)),
        BLiteral::of(true));
    expectBLiteral(e, false);
  }
}

void utest_cons()
{
  {

    auto e = BuiltinExpr::consOf( //
        ILiteral::of(1), BuiltinExpr::consOf( //
            ILiteral::of(2), BuiltinExpr::consOf( //
                ILiteral::of(3), ListLiteral::of(LType::create(IType::create())))));
    expectIList(e, { 1, 2, 3 });
  }
}

void utest_at()
{
  {
    auto e = BuiltinExpr::atIndexOf(ILiteral::of(1), BuiltinExpr::consOf( //
        ILiteral::of(1), BuiltinExpr::consOf( //
            ILiteral::of(2), BuiltinExpr::consOf( //
                ILiteral::of(3), ListLiteral::of(LType::create(IType::create()))))));
    expectILiteral(e, 2);
  }
}

void utest_eq()
{
  {
    auto e = BuiltinExpr::eqOf(ILiteral::of(1), ILiteral::of(2));
    expectBLiteral(e, false);
  }
  {
    auto e = BuiltinExpr::eqOf(BLiteral::of(true), BLiteral::of(false));
    expectBLiteral(e, false);
  }

  {
    auto e = BuiltinExpr::eqOf(TLiteral::of("one"), TLiteral::of("one"));
    expectBLiteral(e, true);
  }

  {
    auto e = BuiltinExpr::eqOf(ErrorExpr::of(IType::create()), ErrorExpr::of(IType::create()));
    expectError(e, { });
  }

  {
    auto vList = VarExpr::of(LType::create(IType::create()), VarName::of("list"));
    auto vTwo = VarExpr::of(IType::create(), VarName::of("two"));

    auto env = makeEnv( { //
        { vList->name, ListLiteral::of(LType::create(IType::create()), { ILiteral::of(1), vTwo,
            ILiteral::of(3) }) }, //
            { vTwo->name, ILiteral::of(2) } });

    auto e1 = BuiltinExpr::consOf( //
        ILiteral::of(1), BuiltinExpr::consOf( //
            vTwo, BuiltinExpr::consOf( //
                ILiteral::of(3), ListLiteral::of(LType::create(IType::create())))));

    auto e = BuiltinExpr::eqOf(e1, vList);

    expectBLiteral(e, true, env);
  }

  {

    auto e1 = ConstructExpr::of( { //
        ILiteral::of(1), //
        ILiteral::of(2)
        //
        });//
    auto e2 = ConstructExpr::of( { //
        ILiteral::of(1), //
        ILiteral::of(2)  //
        });//

    auto e = BuiltinExpr::eqOf(e1, e2);

    expectBLiteral(e, true);
  }
}

void utest_lt()
{
  expectBLiteral(BuiltinExpr::ltOf(ILiteral::of(1), ILiteral::of(2)), true);
  expectBLiteral(BuiltinExpr::ltOf(ILiteral::of(1), ILiteral::of(1)), false);
  expectBLiteral(BuiltinExpr::ltOf(ILiteral::of(2), ILiteral::of(1)), false);

  expectBLiteral(BuiltinExpr::ltOf(TLiteral::of("a"), TLiteral::of("b")), true);
  expectBLiteral(BuiltinExpr::ltOf(TLiteral::of("a"), TLiteral::of("a")), false);
  expectBLiteral(BuiltinExpr::ltOf(TLiteral::of("b"), TLiteral::of("a")), false);
}

void utest_length()
{
  {
    auto e = BuiltinExpr::lengthOf(BuiltinExpr::consOf( //
        ILiteral::of(1), BuiltinExpr::consOf( //
            ILiteral::of(2), BuiltinExpr::consOf( //
                ILiteral::of(3), ListLiteral::of(LType::create(IType::create()))))));
    expectILiteral(e, 3);
  }
}

void utest_eval_negate()
{
  {
    auto e = BuiltinExpr::negationOf(ILiteral::of(-1));
    expectILiteral(e, 1);
  }
  {
    VarName foo;
    auto e = BuiltinExpr::negationOf(BuiltinExpr::negationOf(VarExpr::of(IType::create(), foo)));
    expectVariable(e, foo, IType::create());
  }
}

void utest_eval_not()
{
  {
    auto e = BuiltinExpr::notOf(BLiteral::of(true));
    expectBLiteral(e, false);
  }
  {
    auto e = BuiltinExpr::notOf(BLiteral::of(false));
    expectBLiteral(e, true);
  }
  {
    VarName foo;
    auto e = BuiltinExpr::notOf(BuiltinExpr::notOf(VarExpr::of(BType::create(), foo)));
    expectVariable(e, foo, BType::create());
  }
}

void utest_eval_sum()
{
  auto e = BuiltinExpr::sumOf(BuiltinExpr::sumOf(ILiteral::of(1), ILiteral::of(2)),
      ILiteral::of(3));

  expectILiteral(e, 6);
}

void utest_eval_conditional()
{
  auto cond = BuiltinExpr::eqOf(ILiteral::of(3), BuiltinExpr::lengthOf(BuiltinExpr::consOf( //
      ILiteral::of(1), BuiltinExpr::consOf( //
          ILiteral::of(2), BuiltinExpr::consOf( //
              ILiteral::of(3), ListLiteral::of(LType::create(IType::create())))))));

  auto e = ConditionalExpr::of(cond, BLiteral::of(true), BLiteral::of(false));
  expectBLiteral(cond, true);
}

void utest_eval_var()
{
  {
    // a bound variable
    auto var = VarExpr::of(IType::create(), VarName::of("two"));

    auto env = makeEnv( { //
        { var->name, ILiteral::of(2) } });
    expectILiteral(var, 2, env);
  }
  {
    // an unbound variable
    auto var = VarExpr::of(IType::create(), VarName::of("two"));
    expectVariable(var, var->name, var->type, { }, false);
  }
}

