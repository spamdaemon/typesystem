#include <mylang/typesystem/predicates/IsConstant.h>
#include <mylang/typesystem/RBTree.h>
#include <mylang/typesystem/exprs/ErrorExpr.h>
#include <mylang/typesystem/exprs/BLiteral.h>
#include <mylang/typesystem/exprs/BuiltinExpr.h>
#include <mylang/typesystem/exprs/ILiteral.h>
#include <mylang/typesystem/exprs/ListLiteral.h>
#include <mylang/typesystem/types/IType.h>
#include <mylang/typesystem/types/LType.h>
#include <mylang/typesystem/types/SType.h>
#include <mylang/typesystem/typesystem.h>
#include <cassert>
#include <memory>
#include <mylang/typesystem/exprs/ConditionalExpr.h>
#include <mylang/typesystem/exprs/TLiteral.h>
#include <mylang/typesystem/exprs/ConstructExpr.h>
#include <mylang/typesystem/exprs/TypeExpr.h>
#include <mylang/typesystem/exprs/VarExpr.h>

using namespace mylang;
using namespace mylang::typesystem;
using namespace mylang::typesystem::exprs;
using namespace mylang::typesystem::predicates;
using namespace mylang::typesystem::types;

static void expectConstant(const ExprPtr &p)
{
  auto e = IsConstant::test(p);
  assert(e);
}

static void expectNoConstant(const ExprPtr &p)
{
  auto e = IsConstant::test(p);
  assert(e == false);
}

void utest_eval_builtin()
{
  expectNoConstant(BuiltinExpr::andOf(BLiteral::of(true), BLiteral::of(true)));
}

void utest_eval_and()
{
  expectNoConstant(BuiltinExpr::andOf(BLiteral::of(true), BLiteral::of(true)));
}

void utest_cons()
{
  expectNoConstant( //
      BuiltinExpr::consOf( //
          ILiteral::of(2), BuiltinExpr::consOf( //
              ILiteral::of(3), ListLiteral::of(LType::create(IType::create())))));
}

void utest_at()
{
  expectNoConstant(BuiltinExpr::consOf( //
      ILiteral::of(1), BuiltinExpr::consOf( //
          ILiteral::of(2), BuiltinExpr::consOf( //
              ILiteral::of(3), ListLiteral::of(LType::create(IType::create()))))));
}

void utest_eq()
{
  expectNoConstant(BuiltinExpr::eqOf(ILiteral::of(1), ILiteral::of(2)));
}

void utest_lt()
{
  expectNoConstant(BuiltinExpr::ltOf(ILiteral::of(1), ILiteral::of(2)));
}

void utest_length()
{
  expectNoConstant(BuiltinExpr::lengthOf(BuiltinExpr::consOf( //
      ILiteral::of(1), BuiltinExpr::consOf( //
          ILiteral::of(2), BuiltinExpr::consOf( //
              ILiteral::of(3), ListLiteral::of(LType::create(IType::create())))))));
}

void utest_negate()
{
  expectNoConstant(BuiltinExpr::negationOf(ILiteral::of(-1)));
}

void utest_not()
{
  expectNoConstant(BuiltinExpr::notOf(BLiteral::of(true)));
}

void utest_sum()
{
  expectNoConstant(BuiltinExpr::sumOf(ILiteral::of(1), ILiteral::of(2)));
}

void utest_conditional()
{
  auto cond = BuiltinExpr::eqOf(ILiteral::of(3), ILiteral::of(1));
  expectNoConstant(ConditionalExpr::of(cond, BLiteral::of(true), BLiteral::of(false)));
}

void utest_literals()
{
  expectConstant(BLiteral::of(true));
  expectConstant(ILiteral::of(1));
  expectConstant(TLiteral::of("Hello"));
}

void utest_error()
{
  expectConstant(ErrorExpr::of(IType::create()));
}

void utest_variable()
{
  expectNoConstant(VarExpr::of(IType::create(), VarName::of("v")));
}

void utest_list()
{
  expectConstant(ListLiteral::of(LType::create(IType::create())));
  expectConstant(
      ListLiteral::of(LType::create(IType::create()), { ILiteral::of(1), ILiteral::of(2) }));
  expectNoConstant(
      ListLiteral::of(LType::create(IType::create()),
          { ILiteral::of(1), VarExpr::of(IType::create(), VarName::of("v")) }));
}

void utest_construct()
{
  expectConstant(ConstructExpr::of( { }));
  expectConstant(ConstructExpr::of( { ILiteral::of(2) }));
  expectNoConstant(ConstructExpr::of( { VarExpr::of(IType::create(), VarName::of("v")) }));
}

void utest_type_expr()
{
  const VarName a("a");
  expectConstant(TypeExpr::of("foo"));
  expectConstant(TypeExpr::of("foo", ConstructExpr::of( { ILiteral::of(1) })));
  expectNoConstant(
      TypeExpr::of("foo", ConstructExpr::of( { VarExpr::of(IType::create(), VarName::of("v")) })));
}
