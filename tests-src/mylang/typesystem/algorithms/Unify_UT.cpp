#include <mylang/typesystem/algorithms/Unify.h>
#include <mylang/typesystem/eval/Eval.h>
#include <mylang/typesystem/RBTree.h>
#include <mylang/typesystem/exprs/ErrorExpr.h>
#include <mylang/typesystem/exprs/BLiteral.h>
#include <mylang/typesystem/exprs/BuiltinExpr.h>
#include <mylang/typesystem/exprs/ILiteral.h>
#include <mylang/typesystem/exprs/ListLiteral.h>
#include <mylang/typesystem/types/BType.h>
#include <mylang/typesystem/types/IType.h>
#include <mylang/typesystem/types/LType.h>
#include <mylang/typesystem/types/SType.h>
#include <mylang/typesystem/types/TType.h>
#include <mylang/typesystem/types/XType.h>
#include <mylang/typesystem/types/WType.h>
#include <mylang/typesystem/typesystem.h>
#include <mylang/typesystem/exprs/ConditionalExpr.h>
#include <mylang/typesystem/exprs/TLiteral.h>
#include <mylang/typesystem/exprs/ConstructExpr.h>
#include <mylang/typesystem/exprs/TypeExpr.h>
#include <mylang/typesystem/exprs/VarExpr.h>
#include <mylang/typesystem/Environment.h>
#include <mylang/typesystem/TypeDefinition.h>
#include <mylang/typesystem/TypeDefinition.h>
#include <mylang/test/Types.h>
#include <cassert>
#include <memory>
#include <iostream>

using namespace mylang;
using namespace mylang::typesystem;
using namespace mylang::typesystem::exprs;
using namespace mylang::typesystem::algorithms;
using namespace mylang::typesystem::types;
using namespace mylang::test;

static Unify::Result runUnifyEvalLoop(const Unify &U, const ExprPtr &xleft, const ExprPtr &xright,
    const Unify::Bindings &xenv = { })
{
  Unify::Bindings env(xenv);
  Pattern left(xleft);
  Pattern right(xright);

  while (true) {
    auto res = Unify::apply(left, right, env);
    ::std::cerr << "ONE ROUND OF UNIFY YIELDED " << res.second << ::std::endl;
    if (res.second == Unify::BINDINGS) {
      env = res.first;
    } else {
      ::std::cerr << "(2) Result " << res.second << ::std::endl;
      return res;
    }
  }
}

static Unify::Bindings expectUnified(const Pattern &left, const Pattern &right,
    const Unify::Bindings &env = { })
{
  auto e = Unify::apply(left, right, env);
  assert(e.second == Unify::UNIFIED);
  return e.first;
}

static Unify::Bindings expectUnified(const ExprPtr &left, const ExprPtr &right,
    const Unify::Bindings &env = { })
{
  return expectUnified(Pattern(left), Pattern(right));
}

static Unify::Bindings expectBindings(const ExprPtr &left, const ExprPtr &right,
    const Unify::Bindings &env = { })
{
  auto e = Unify::apply(Pattern(left), Pattern(right), env);
  assert(e.second == Unify::BINDINGS);
  return e.first;
}

static void expectFailed(const ExprPtr &left, const ExprPtr &right,
    const Unify::Bindings &env = { })
{
  auto e = Unify::apply(Pattern(left), Pattern(right), env);
  assert(e.second == Unify::FAILED);
}

void utest_typemismatch()
{
  expectFailed(BLiteral::of(true), ILiteral::of(1));
}

void utest_unify_bool_literals()
{
  expectUnified(BLiteral::of(true), BLiteral::of(true));
  expectUnified(BLiteral::of(false), BLiteral::of(false));
  expectFailed(BLiteral::of(true), BLiteral::of(false));
  expectBindings(BLiteral::of(true), VarExpr::of(BType::create(), VarName::of("a")));
  expectBindings(VarExpr::of(BType::create(), VarName::of("a")), BLiteral::of(true));
}

void utest_unify_int_literals()
{
  expectUnified(ILiteral::of(1), ILiteral::of(1));
  expectFailed(ILiteral::of(1), ILiteral::of(2));
  expectBindings(ILiteral::of(2), VarExpr::of(IType::create(), VarName::of("a")));
  expectBindings(VarExpr::of(IType::create(), VarName::of("a")), ILiteral::of(2));
}

void utest_unify_text_literals()
{
  expectUnified(TLiteral::of("Hello"), TLiteral::of("Hello"));
  expectFailed(TLiteral::of("Hello"), TLiteral::of("Hallo"));
  expectBindings(TLiteral::of("X"), VarExpr::of(TType::create(), VarName::of("a")));
  expectBindings(VarExpr::of(TType::create(), VarName::of("a")), TLiteral::of("X"));
}

void utest_unify_list_literals()
{
  auto ty = LType::create(BType::create());
  auto ty1 = LType::create(BType::create());
  auto bTrue = BLiteral::of(true);
  auto bFalse = BLiteral::of(false);
  auto bVar = VarExpr::of(BType::create());
  auto bVar1 = VarExpr::of(BType::create());

  expectUnified(ListLiteral::of(ty), ListLiteral::of(ty1));
  expectUnified(ListLiteral::of(ty, { bTrue, bFalse, bFalse }), ListLiteral::of(ty1, { bTrue,
      bFalse, bFalse }));
  expectBindings(ListLiteral::of(ty, { bTrue, bVar, bFalse }), ListLiteral::of(ty1, { bTrue, bVar1,
      bFalse }));
  expectBindings(ListLiteral::of(ty), VarExpr::of(ty1, VarName::of("a")));
  expectBindings(VarExpr::of(ty, VarName::of("a")), ListLiteral::of(ty1));

  expectFailed(ListLiteral::of(ty), ListLiteral::of(LType::create(IType::create())));
  expectFailed(ListLiteral::of(ty, { }), ListLiteral::of(ty1, { bTrue }));
  expectFailed(ListLiteral::of(ty, { bTrue, bFalse }), ListLiteral::of(ty1, { bTrue, bTrue }));
}

void utest_unify_structs()
{
  auto bTy = BType::create();
  auto iTy = IType::create();

  auto ty = SType::create( { bTy, iTy });
  auto ty1 = SType::create( { IType::create(), BType::create() });

  auto bTrue = BLiteral::of(true);
  auto bFalse = BLiteral::of(false);
  auto z = ILiteral::of(0);
  auto bVar = VarExpr::of(bTy);
  auto iVar = VarExpr::of(iTy);

  expectUnified(ConstructExpr::of( { bTrue, z }), ConstructExpr::of( { bTrue, z }));

  expectBindings(ConstructExpr::of( { bVar, z }), ConstructExpr::of( { bTrue, iVar }));
  expectBindings(ConstructExpr::of( { bTrue, z }), VarExpr::of(ty, VarName::of("a")));
  expectBindings(VarExpr::of(ty, VarName::of("a")), ConstructExpr::of( { bTrue, iVar }));

  expectFailed(ConstructExpr::of( { bTrue, z }), ConstructExpr::of( { z, bTrue }));
}

void utest_typeexpr()
{
  // we don't need to put the type definitions in the environment
  auto def = IntegerType::getDefinition();
  auto zero = IntegerType::getValue(Integer::ZERO());

  expectUnified( //
      def->getConstructorPattern( { Pattern(ILiteral::of(0)), Pattern(ILiteral::of(1)) }), //
      def->getConstructorPattern( { Pattern(ILiteral::of(0)), Pattern(ILiteral::of(1)) }));
  expectBindings(def->callConstructor(), zero);
  expectBindings(def->callConstructor(), VarExpr::of(XType::create(WType::create())));
}

