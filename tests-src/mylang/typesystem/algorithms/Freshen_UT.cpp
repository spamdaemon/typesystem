#include <mylang/typesystem/algorithms/Freshen.h>
#include <mylang/typesystem/exprs/exprs.h>
#include <mylang/typesystem/types/types.h>

using namespace mylang;
using namespace mylang::typesystem;
using namespace mylang::typesystem::exprs;
using namespace mylang::typesystem::algorithms;
using namespace mylang::typesystem::types;

void utest_freshen_let()
{
  auto e = LetExpr::of(ILiteral::of(1), [&](VarPtr v) {return v;})->self<LetExpr>();
  auto f = Freshen::apply(e);
  assert(f.isNew && "Should have been new");
  auto let = f.actual->self<LetExpr>();
  assert(let != nullptr && "Should not have changed the expression type");
  assert(let->variable != e->variable);
}

void utest_freshen_nothing()
{
  assert(Freshen::apply(ILiteral::of(1)).isNew == false && "Should not have freshened anything");
  assert(
      Freshen::apply(VarExpr::of(IType::create())).isNew == false
          && "Should not have freshened non-local variables");
}
