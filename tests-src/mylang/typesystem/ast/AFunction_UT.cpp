#include <mylang/test/AstEnvironment.h>
#include <mylang/test/Types.h>
#include <mylang/typesystem/ast/AExpression.h>
#include <mylang/typesystem/ast/AFunctionCall.h>
#include <mylang/typesystem/ast/AValue.h>
#include <mylang/typesystem/ast/AFunction.h>
#include <mylang/typesystem/Environment.h>
#include <mylang/typesystem/exprs/exprs.h>
#include <mylang/typesystem/types/types.h>

using namespace mylang;
using namespace mylang::typesystem;
using namespace mylang::typesystem::ast;
using namespace mylang::typesystem::exprs;
using namespace mylang::typesystem::types;

// the type environment
using TypeEnv = mylang::typesystem::Environment;

// a simple not function for booleans only
void utest_boolean_not()
{
  auto env = mylang::test::AstEnvironment::of();

  mylang::test::BoolType::putDefinition(*env);
  auto boolTy = mylang::typesystem::ast::AType::ofPattern(TypeExpr::of("bool"));
  auto fn = env->putF("not",
      AFunction::ofUnary(boolTy, boolTy, [&](VarPtr, VarPtr) {return BLiteral::of(true);}));
  auto expr = AFunctionCall::of(fn, { AValue::of(boolTy) });
}

// an equality function that works on all types
void utest_eq()
{
  auto env = mylang::test::AstEnvironment::of();

  auto boolDef = mylang::test::BoolType::putDefinition(*env);

  auto boolTy = mylang::typesystem::ast::AType::of(boolDef);
  auto lhs = mylang::typesystem::ast::AType::ofPattern(VarExpr::of(XType::create(WType::create())));
  auto rhs = mylang::typesystem::ast::AType::ofPattern(VarExpr::of(XType::create(WType::create())));

  auto fn = env->putF("eq", AFunction::ofBinary(boolTy, lhs, rhs, [&](VarPtr, VarPtr l, VarPtr r) {
    return BuiltinExpr::eqOf(l,r);}));
  auto expr = AFunctionCall::of(fn, { AValue::of(lhs), AValue::of(rhs) });
}

// a simple test to just test out the structures
void utest_int_negate()
{
  auto env = mylang::test::AstEnvironment::of();

  // types we need
  auto integerTy = mylang::test::IntegerType::putDefinition(*env);
  auto intTy = AType::of(integerTy);
  auto retTy = AType::of(integerTy);

  auto fn = env->putF("integer.negate", AFunction::ofUnary(retTy, { intTy }, [&](VarPtr, VarPtr) {
    return BLiteral::of(true);
  }));
  auto expr = AFunctionCall::of(fn, { AValue::of(intTy) });
}

