#include <mylang/typesystem/ast/ATypeChecker.h>
#include <mylang/typesystem/ast/AFunctionCall.h>
#include <mylang/typesystem/ast/AValue.h>
#include <mylang/typesystem/Environment.h>
#include <mylang/typesystem/exprs/exprs.h>
#include <mylang/typesystem/types/types.h>
#include <mylang/typesystem/visitors/Printer.h>
#include <mylang/typesystem/solver/Solver.h>
#include <mylang/test/Types.h>
#include <mylang/test/Functions.h>
#include <mylang/test/AstEnvironment.h>
#include <iostream>

using namespace mylang;
using namespace mylang::test;
using namespace mylang::typesystem;
using namespace mylang::typesystem::exprs;
using namespace mylang::typesystem::types;
using namespace mylang::typesystem::ast;
using namespace mylang::typesystem::solver;

enum Satisfiability
{
  SATISFIABLE, UNSATISFIABLE
};

static Satisfiability constraintCheck(ExprPtr e, const Environment &env)
{
  auto newBinds = Solver::eval(e, env);
  return newBinds.has_value() ? SATISFIABLE : UNSATISFIABLE;
}

static ExprPtr passTypeCheck(AExpression::Ptr ae, const AEnvironment::Ptr &env,
    std::optional<Satisfiability> checkSatisfiable)
{
  try {
    auto newEnv = ATypeChecker::typecheck(env, ae);
    auto e = ae->toExpr(*newEnv);
    ::std::cerr << "Typechecked expression:" << ::std::endl;
#if 1
    visitors::Printer::print(::std::cerr, e);
    ::std::cerr << ::std::endl;
#endif
    if (checkSatisfiable) {
      auto satisfiability = constraintCheck(e, newEnv->getEnvironment());
      assert(satisfiability == checkSatisfiable && "Typechecked, but not satisfiable");
    }
  } catch (const ATypeChecker::TypeError &ex) {
    ::std::cerr << "Failed to typecheck " << ex.what() << ::std::endl;
    assert(false && "Failed to typecheck");
  } catch (const ::std::exception &ex) {
    ::std::cerr << "Unexpected error" << ex.what() << ::std::endl;
    assert(false && "Failed to typecheck");
  } catch (...) {
    ::std::cerr << "Bad error" << ::std::endl;
    assert(false && "Failed to typecheck");
  }
  return nullptr;
}

static void failTypeCheck(AExpression::Ptr ae, const AEnvironment::Ptr &env)
{
  try {
    ATypeChecker::typecheck(env, ae);
    assert(false && "Should not pass the type check");
  } catch (const ATypeChecker::TypeError &ex) {
    //
  }
  // ignore as this is expected
}

// a simple test to just test out the structures
void utest_basic_pass()
{
  auto env = AstEnvironment::of();

  auto bty = BoolType::putDefinition(*env);
  auto optTy = OptType::putDefinition(*env);

  auto isNoneF = env->lookupFSpec(OptType::isNone);
  auto noneF = env->lookupFSpec(OptType::none);

  auto e = AFunctionCall::of(isNoneF, { AFunctionCall::of(noneF, { }) });

  passTypeCheck(e, env, SATISFIABLE);
}

#if 0
// a simple test to just test out the structures
void utest_pass_satisfiable()
{
  auto env = AstEnvironment::of();

  IntegerType::putDefinition(*env);
  auto eq = registerEq(*env);

  auto left = AFunctionCall::of(env->lookupFSpec(IntegerType::integer), { AValue::of(ILiteral::of(0)),
      AValue::of(ILiteral::of(1)) });
  auto right = AFunctionCall::of(env->lookupFSpec(IntegerType::one), { });
  auto e = AFunctionCall::of(eq, { left, right });
  passTypeCheck(e, env, SATISFIABLE);
}

void utest_pass_unsatisfiable()
{
  auto env = AstEnvironment::of();

  IntegerType::putDefinition(*env);
  auto assign = registerAssign(*env);

  auto left = AFunctionCall::of(env->lookupFSpec(IntegerType::one), { });
  auto right = AFunctionCall::of(env->lookupFSpec(IntegerType::zero), { });
  auto e = AFunctionCall::of(assign, { left, right });
  passTypeCheck(e, env, UNSATISFIABLE);
}

// a simple test to just test out the structures
void utest_basic_fail()
{
  auto env = AstEnvironment::of();
  auto eq = registerEq(*env);

  auto boolT = BoolType::putDefinition(*env);
  auto bitT = BitType::putDefinition(*env);
  auto optT = OptType::putDefinition(*env);

  try {
    // type constructors for opt<bool> and opt<bit>
    auto optBoolTy = optT->getConstructor( { OptType::constructor, boolT->getConstructor() });
    auto optBitTy = optT->getConstructor( { OptType::constructor, bitT->getConstructor() });

    auto noneF = env->lookupFSpec(OptType::none);

    auto e = AFunctionCall::of(eq, { AFunctionCall::of(noneF, AType::of(optBoolTy), { }),
        AFunctionCall::of(noneF, AType::of(optBitTy), { }) });

    failTypeCheck(e, env);
  } catch (const ::std::exception &e) {
    ::std::cerr << e.what() << ::std::endl;
  }
}

// a simple test to just test out the structures
void utest_integer_succ()
{
  auto env = AstEnvironment::of();

  IntegerType::putDefinition(*env);
  auto assign = registerAssign(*env);

  // eq(succ(0),1)
  auto left = AFunctionCall::of(env->lookupFSpec(IntegerType::succ),
      { AFunctionCall::of(env->lookupFSpec(IntegerType::zero), { }) });

  auto right = AFunctionCall::of(env->lookupFSpec(IntegerType::one), { });

  auto e = AFunctionCall::of(assign, { left, right });
  passTypeCheck(e, env, SATISFIABLE);
}

void utest_optional()
{
  auto env = AstEnvironment::of();

  auto ity = IntegerType::putDefinition(*env);
  auto bty = BoolType::putDefinition(*env);
  auto optTy = OptType::putDefinition(*env);
  auto assign = registerAssign(*env);

  auto optInt = optTy->getConstructor( { VarExpr::of(TType::create()), ity->getConstructor( { }) });
  auto optBool = optTy->getConstructor(
      { VarExpr::of(TType::create()), bty->getConstructor( { }) });

  auto isNoneLeft = env->lookupFSpec(OptType::isNone);
  auto isNoneRight = env->lookupFSpec(OptType::isNone);

  auto left = AFunctionCall::of(isNoneLeft, { AValue::of(AType::of(optInt)) });
  auto right = AFunctionCall::of(isNoneRight, { AValue::of(AType::of(optBool)) });

  auto e = AFunctionCall::of(assign, { left, right });
  passTypeCheck(e, env, SATISFIABLE);
}

void utest_integer_maybe_zero()
{
  auto env = AstEnvironment::of();
  auto assign = registerAssign(*env);

  auto ity = IntegerType::putDefinition(*env);
  auto bty = BoolType::putDefinition(*env);

  auto maybeZeroF = env->lookupFSpec(IntegerType::maybeZero);
  auto newInt = env->lookupFSpec(IntegerType::integer);
  auto trueF = env->lookupFSpec(BoolType::t);
  auto falseF = env->lookupFSpec(BoolType::f);

  ::std::function<void(bool, ExprPtr, ExprPtr)> checker(
      [&](bool b, ExprPtr min,
          ExprPtr max) {
            auto e = AFunctionCall::of(assign, {
                  AFunctionCall::of(maybeZeroF, {AFunctionCall::of(newInt, {AValue::of(min),AValue::of(max)})}),
                  AFunctionCall::of(b ? trueF : falseF, {})
                });
            passTypeCheck(e, env, SATISFIABLE);
          });

  checker(false, ILiteral::of(-10), ILiteral::of(-1));
  checker(false, ILiteral::of(1), ILiteral::of(10));
  checker(true, ILiteral::of(0), ILiteral::of(10));
  checker(true, ILiteral::of(-10), ILiteral::of(0));
  checker(true, ILiteral::of(0), ILiteral::of(0));
  checker(true, ILiteral::of(-10), VarExpr::of(IType::create()));
  checker(true, VarExpr::of(IType::create()), ILiteral::of(10));
  checker(true, VarExpr::of(IType::create()), VarExpr::of(IType::create()));
  {
    auto v = VarExpr::of(IType::create());
    checker(true, v, v);
  }
  checker(false, VarExpr::of(IType::create()), ILiteral::of(-10));
  checker(false, ILiteral::of(10), VarExpr::of(IType::create()));
}
#endif
