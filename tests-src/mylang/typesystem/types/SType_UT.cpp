#include <mylang/typesystem/types/BType.h>
#include <mylang/typesystem/types/IType.h>
#include <mylang/typesystem/types/LType.h>
#include <mylang/typesystem/types/SType.h>
#include <mylang/typesystem/types/TType.h>
#include <mylang/typesystem/types/XType.h>
#include <mylang/typesystem/exprs/ConstructExpr.h>
#include <mylang/typesystem/exprs/ILiteral.h>
#include <mylang/typesystem/exprs/TLiteral.h>
#include <mylang/typesystem/exprs/TypeExpr.h>
#include <iostream>
#include <cassert>

using namespace mylang::typesystem::exprs;
using namespace mylang::typesystem::types;

void utest_struct()
{

  auto ty = ConstructExpr::of( { //
      TLiteral::of("array"), //
      TypeExpr::of("bool"), //
      ILiteral::of(0), //
      ILiteral::of(255) });
}

