#ifndef CLASS_MYLANG_BIGINT_H
#define CLASS_MYLANG_BIGINT_H
#include <iosfwd>
#include <string>
#include <gmp.h>
#include <cstdint>
#include <optional>

namespace mylang {

  class BigUInt;

  /** A multiprecision integer class */
  class BigInt
  {
    /** Copy constructor */
  public:
    BigInt(const BigInt &other);

    /** Copy operator */
  public:
    BigInt& operator=(const BigInt &other);

    /** Default constructor with the value 0 */
  public:
    BigInt();

    /** Constructor with a builtin integer */
  public:
    BigInt(long long int value);

    /** Constructor with a builtin integer */
  public:
    BigInt(int value);

    /** Constructor with a builtin integer */
  public:
    BigInt(long value);

    /** Constructor with a builtin integer */
  public:
    BigInt(unsigned long long int value);

    /** Constructor with a builtin integer */
  public:
    BigInt(unsigned long value);

    /** Constructor with a builtin integer */
  public:
    BigInt(unsigned int value);

    /**
     * Construct this integer from a big uint
     * @param value a big uint */
  public:
    BigInt(const BigUInt &value);

    /**
     * Destructor.
     */
  public:
    ~BigInt();

    /**
     * Create a big int from a signed integer
     * @param val a value
     * @return a big int
     */
  public:
    static BigInt signedValueOf(long long int value);

    /**
     * Create a big int from an unsigned integer
     * @param val a value
     * @return a big int
     */
  public:
    static BigInt unsignedValueOf(unsigned long long int value);

    /**
     * Parse a string as a signed integer.
     * @param str a string
     * @return a bigint
     */
  public:
    static BigInt parse(const ::std::string &str);

    /**
     * Parse a string as a signed integer using octal.
     * @param str a string
     * @return a bigint
     */
  public:
    static BigInt parseOctal(const ::std::string &str);

    /**
     * Parse a string as a signed integer using hex.
     * @param str a string
     * @return a bigint
     */
  public:
    static BigInt parseHex(const ::std::string &str);

    /**
     * Parse a string as a signed integer using binary notation.
     * @param str a string
     * @return a bigint
     */
  public:
    static BigInt parseBinary(const ::std::string &str);

    /**
     * Get the int64 for this value.
     * @return a value or nothing if the value does not fit
     */
  public:
    ::std::optional<::std::int64_t> toInt64() const;

    /**
     * Get the int32 for this value.
     * @return a value or nothing if the value does not fit
     */
  public:
    ::std::optional<::std::int32_t> toInt32() const;

    /**
     * Get the int16 for this value.
     * @return a value or nothing if the value does not fit
     */
  public:
    ::std::optional<::std::int16_t> toInt16() const;

    /**
     * Get the int8 for this value.
     * @return a value or nothing if the value does not fit
     */
  public:
    ::std::optional<::std::int8_t> toInt8() const;

    /**
     * Get the uint64 for this value.
     * @return a value or nothing if the value does not fit
     */
  public:
    ::std::optional<::std::uint64_t> toUInt64() const;

    /**
     * Get the uint32 for this value.
     * @return a value or nothing if the value does not fit
     */
  public:
    ::std::optional<::std::uint32_t> toUInt32() const;

    /**
     * Get the uint16 for this value.
     * @return a value or nothing if the value does not fit
     */
  public:
    ::std::optional<::std::uint16_t> toUInt16() const;

    /**
     * Get the uint8 for this value.
     * @return a value or nothing if the value does not fit
     */
  public:
    ::std::optional<::std::uint8_t> toUInt8() const;

    /**
     * Get the size_t for this value.
     * @return a value or nothing if the value does not fit
     */
  public:
    ::std::optional<::std::size_t> toSize_t() const;

    /**
     * Determine if the value can be represented as a size_t type
     * @return true if the number fits into a size_t
     */
  public:
    bool isSize_t() const;

    /**
     * Determine if the value can be represented as a signed 64bit number
     * @return true if the number does not require more than 64 bits
     */
  public:
    bool isSigned64() const;

    /**
     * Determine if the value can be represented as an unsigned 64bit number
     * @return true if the number does not require more than 64 bits
     */
  public:
    bool isUnsigned64() const;

    /**
     * Determine if the value can be represented as a signed 32bit number
     * @return true if the number does not require more than 32 bits
     */
  public:
    bool isSigned32() const;

    /**
     * Determine if the value can be represented as an unsigned 32bit number
     * @return true if the number does not require more than 32 bits
     */
  public:
    bool isUnsigned32() const;

    /**
     * Determine if the value can be represented as a signed 16bit number
     * @return true if the number does not require more than 16 bits
     */
  public:
    bool isSigned16() const;

    /**
     * Determine if the value can be represented as an unsigned 16bit number
     * @return true if the number does not require more than 16 bits
     */
  public:
    bool isUnsigned16() const;

    /**
     * Determine if the value can be represented as a signed 8bit number
     * @return true if the number does not require more than 8 bits
     */
  public:
    bool isSigned8() const;

    /**
     * Determine if the value can be represented as an unsigned 8bit number
     * @return true if the number does not require more than 8 bits
     */
  public:
    bool isUnsigned8() const;

    /**
     * Get a signed integer.
     * @return the signed value
     * @throws Exception if the value does not fit
     */
  public:
    long long int signedValue() const;

    /**
     * Get a unsigned integer.
     * @return the unsigned value
     */
  public:
    unsigned long long int unsignedValue() const;

    /**
     * Add two big ints.
     * @param y an integer
     * @return a new bigint
     */
  public:
    BigInt add(const BigInt &op2) const;

    /**
     * Add two big ints.
     * @param y an integer
     * @return a new bigint
     */
  public:
    BigInt subtract(const BigInt &op2) const;

    /**
     * Negate this integer.
     * @return a new bigint
     */
  public:
    BigInt negate() const;

    /**
     * Add two big ints.
     * @param y an integer
     * @return a new bigint
     */
  public:
    BigInt multiply(const BigInt &op2) const;

    /**
     * Add two big ints.
     * @param y an integer
     * @return a new bigint
     */
  public:
    BigInt divide(const BigInt &op2) const;

    /**
     * Add two big ints.
     * @param y an integer
     * @return a new bigint
     */
  public:
    BigInt remainder(const BigInt &op2) const;

    /**
     * Add two big ints.
     * @param y an integer
     * @return a new bigint
     */
  public:
    BigInt mod(const BigInt &op2) const;

    /**
     * Get the sign of this integer
     * @return -1, 0, or 1
     */
  public:
    int sgn() const;

    /**
     * Get the absolute value.
     * @return the absolute value
     */
  public:
    BigInt abs() const;

    /**
     * Min of this integer and that integer.
     * @param y an integer
     * @return a new bigint
     */
  public:
    BigInt min(const BigInt &op2) const;

    /**
     * Max of this integer and that integer.
     * @param y an integer
     * @return a new bigint
     */
  public:
    BigInt max(const BigInt &op2) const;

    /**
     * Raise this value to the specified power.
     * @param exp the unsigned exponent
     * @return this ^ exp
     */
  public:
    BigInt pow(::std::uint32_t exp) const;

    /**
     * Compare
     * @param y an integer
     * @return -1 if this integer is less, 0 if equal, 1 if greater than y
     */
  public:
    int compare(const BigInt &op2) const;

    /**
     * Write an int to a stream
     * @param out a stream
     * @return the outoput stream
     */
  public:
    ::std::ostream& write(::std::ostream &out) const;

    /**
     * Get the string representation.
     * @return a string
     */
  public:
    ::std::string toString() const;

    /** The operators */
  public:
    inline friend BigInt operator+(const BigInt &x, const BigInt &y)
    {
      return x.add(y);
    }

    inline friend BigInt operator-(const BigInt &x, const BigInt &y)
    {
      return x.subtract(y);
    }

    inline friend BigInt operator*(const BigInt &x, const BigInt &y)
    {
      return x.multiply(y);
    }

    inline friend BigInt operator/(const BigInt &x, const BigInt &y)
    {
      return x.divide(y);
    }

    inline friend BigInt operator%(const BigInt &x, const BigInt &y)
    {
      return x.mod(y);
    }

    inline friend BigInt operator-(const BigInt &x)
    {
      return x.negate();
    }

    inline friend bool operator<(const BigInt &x, const BigInt &y)
    {
      return x.compare(y) < 0;
    }

    inline friend bool operator<=(const BigInt &x, const BigInt &y)
    {
      return x.compare(y) <= 0;
    }

    inline friend bool operator>(const BigInt &x, const BigInt &y)
    {
      return x.compare(y) > 0;
    }

    inline friend bool operator>=(const BigInt &x, const BigInt &y)
    {
      return x.compare(y) >= 0;
    }

    inline friend bool operator==(const BigInt &x, const BigInt &y)
    {
      return x.compare(y) == 0;
    }

    inline friend bool operator!=(const BigInt &x, const BigInt &y)
    {
      return x.compare(y) != 0;
    }

    inline friend ::std::ostream& operator<<(::std::ostream &s, const BigInt &x)
    {
      return x.write(s);
    }

    /** The integer value */
  private:
    mpz_t value;
  };

}
#endif
