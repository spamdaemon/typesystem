#ifndef CLASS_MYLANG_SELF_H
#define CLASS_MYLANG_SELF_H

#include <memory>

namespace mylang {

  /**
   * The base type for all expressions used in the typesystem.
   */
  template<class T>
  class Self: public ::std::enable_shared_from_this<T>
  {

    /** Default constructor */
  protected:
    inline Self()
    {
    }

    /** Destructor */
  public:
    virtual ~Self()
    {
    }

    /**
     * The the self type.
     * @param return a shared pointer for this object
     */
  public:
    template<class U = T>
    inline ::std::shared_ptr<const U> self() const
    {
      auto ptr = ::std::enable_shared_from_this<T>::shared_from_this();
      return ::std::dynamic_pointer_cast<const U>(ptr);
    }
  };
}
#endif
