#include <mylang/BigUInt.h>
#include <mylang/BigInt.h>
#include <iostream>
#include <sstream>
#include <cmath>

namespace mylang {

  BigUInt::BigUInt()
  {
  }

  BigUInt::BigUInt(unsigned long long int xvalue)
      : value(BigInt::unsignedValueOf(xvalue))
  {
  }

  BigUInt::BigUInt(const BigInt &integer)
      : value(integer)
  {
    if (integer < BigInt(0)) {
      throw ::std::invalid_argument("not an unsigned integer " + integer.toString());
    }
  }

  BigUInt::~BigUInt()
  {
  }

  long long int BigUInt::signedValue() const
  {
    return value.signedValue();
  }

  unsigned long long int BigUInt::unsignedValue() const
  {
    return value.unsignedValue();
  }

  BigUInt BigUInt::signedValueOf(long long int value)
  {
    return BigUInt(BigInt::signedValueOf(value));
  }

  BigUInt BigUInt::unsignedValueOf(unsigned long long int value)
  {
    return BigUInt(BigInt::unsignedValueOf(value));
  }

  BigUInt BigUInt::parse(const ::std::string &str)
  {
    return BigInt::parse(str);
  }

  BigUInt BigUInt::negate() const
  {
    return value.negate();
  }

  BigUInt BigUInt::add(const BigUInt &op2) const
  {
    return value.add(op2.value);
  }

  BigUInt BigUInt::subtract(const BigUInt &op2) const
  {
    return value.subtract(op2.value);
  }

  BigUInt BigUInt::multiply(const BigUInt &op2) const
  {
    return value.multiply(op2.value);
  }

  BigUInt BigUInt::divide(const BigUInt &op2) const
  {
    return value.divide(op2.value);
  }

  BigUInt BigUInt::remainder(const BigUInt &op2) const
  {
    return value.remainder(op2);
  }

  BigUInt BigUInt::mod(const BigUInt &op2) const
  {
    return value.mod(op2.value);
  }

  BigUInt BigUInt::pow(::std::uint32_t exp) const
  {
    return value.pow(exp);
  }

  BigUInt BigUInt::min(const BigUInt &op2) const
  {
    return value.min(op2.value);
  }
  BigUInt BigUInt::max(const BigUInt &op2) const
  {
    return value.max(op2.value);
  }

  int BigUInt::compare(const BigUInt &op2) const
  {
    return value.compare(op2.value);
  }

  ::std::ostream& BigUInt::write(::std::ostream &out) const
  {
    return value.write(out);
  }

  ::std::string BigUInt::toString() const
  {
    return value.toString();
  }

}
