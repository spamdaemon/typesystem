#include <mylang/names/Name.h>
#include <mylang/names/FQN.h>
#include <algorithm>
#include <sstream>

namespace mylang {
  namespace names {

    namespace {
      static size_t nextGuid()
      {
        static size_t guid = 0;
        return guid++;
      }
    }

    Name::Name(const Ptr &p, const ::std::string &n, bool fqn)
        : guid(nextGuid()), luid(p ? p->nextLocalID() : guid), _nextLuid(0), _parent(p), _name(n),
            _fqn(fqn)
    {

      if (fqn) {
        if (p && !p->isFQN()) {
          throw ::std::invalid_argument("Parent is not a FQN");
        }
      }
      if (n.empty()) {
        throw ::std::invalid_argument("Name is empty");
      }
    }

    Name::~Name()
    {
    }

    size_t Name::nextLocalID() const
    {
      return _nextLuid++;
    }

    void Name::print(::std::ostream &out) const
    {
      out << uniqueFullName(".");
    }

    Name::Ptr Name::root() const
    {
      Name::Ptr THIS = shared_from_this();
      while (THIS->parent()) {
        THIS = THIS->parent();
      }
      return THIS;
    }

    ::std::vector<Name::Ptr> Name::segments() const
    {
      ::std::vector<Name::Ptr> res;
      Name::Ptr THIS = shared_from_this();
      while (THIS) {
        res.push_back(THIS);
        THIS = THIS->parent();
      }
      ::std::reverse(res.begin(), res.end());
      return res;
    }

    ::std::string Name::localName() const
    {
      return _name;
    }

    ::std::string Name::fullName(const ::std::string &sep) const
    {
      ::std::string res;
      Ptr p = parent();
      if (p) {
        res += p->fullName(sep);
        res += sep;
      }
      res += localName();
      return res;
    }

    ::std::string Name::uniqueLocalName() const
    {
      ::std::ostringstream out;
      out << _name;
      if (_name != "_") {
        out << '_';
      }

      out << ::std::hex << guid; // could also just use the 'this'
      return out.str();
    }

    ::std::string Name::uniqueFullName(const ::std::string &sep) const
    {
      ::std::string res;
      Ptr p = parent();
      if (p) {
        res += p->uniqueFullName(sep);
        res += sep;
      }
      res += uniqueLocalName();
      return res;
    }

    ::std::string Name::fullName() const
    {
      return fullName(".");
    }

    Name::Ptr Name::create(const FQN &fqn)
    {
      Ptr n;
      for (auto s : fqn.segments()) {
        n = createFQN(s.localName(), n);
      }
      return n;
    }

    ::std::optional<FQN> Name::fqn() const
    {
      auto p = parent();
      if (p) {
        auto res = p->fqn();
        if (res) {
          return FQN(res->createChild(localName()));
        }
      }

      if (isFQN() == false) {
        return ::std::nullopt;
      }

      return FQN(localName());
    }

    ::std::optional<FQN> Name::uniqueFqn() const
    {
      auto p = parent();
      if (p) {
        auto res = p->fqn();
        if (res) {
          return FQN(res->createChild(uniqueLocalName()));
        }
      }

      if (isFQN() == false) {
        return ::std::nullopt;
      }

      return FQN(uniqueLocalName());
    }

    Name::Ptr Name::create(const ::std::string &name, const Ptr &parent)
    {
      struct Impl: public Name
      {

        Impl(const Ptr &p, const ::std::string &n)
            : Name(p, n, false)
        {
        }

        ~Impl()
        {
        }

        Ptr createSibling() const
        {
          return create(localName(), parent());
        }

        Ptr createChild(const ::std::string &c) const
        {
          return create(c, shared_from_this());
        }
      };
      return ::std::make_shared<Impl>(parent, name.empty() ? "_" : name);
    }

    Name::Ptr Name::createFQN(const ::std::string &name, const Ptr &parent)
    {
      struct Impl: public Name
      {

        Impl(const Ptr &p, const ::std::string &n)
            : Name(p, n, true)
        {
        }

        ~Impl()
        {
        }

        Ptr createSibling() const
        {
          return createFQN(localName(), parent());
        }

        Ptr createChild(const ::std::string &c) const
        {
          return createFQN(c, shared_from_this());
        }
      };
      return ::std::make_shared<Impl>(parent, name.empty() ? "_" : name);
    }

    Name::Ptr Name::parseFQN(const ::std::string &n, char sep)
    {
      ::std::string::size_type i = 0;
      Ptr p;
      for (::std::string::size_type j; (j = n.find(sep, i)) != ::std::string::npos; i = j + 1) {
        p = createFQN(n.substr(i, j - i), p);
      }
      p = createFQN(n.substr(i, ::std::string::npos), p);
      return p;
    }

  }
}
