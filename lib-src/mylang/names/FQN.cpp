#include <mylang/names/FQN.h>
#include <algorithm>
#include <sstream>

namespace mylang {
  namespace names {

    FQN::Impl::Impl(Ptr p, ::std::string n)
        : _parent(::std::move(p)), _name(::std::move(n))
    {
      if (!isSegment(_name)) {
        throw ::std::invalid_argument("Not a valid segment");
      }
    }

    FQN::FQN(::std::string n, const ::std::optional<FQN> &p)
        : impl(::std::make_shared<Impl>(p ? p->impl : nullptr, ::std::move(n)))
    {
    }

    FQN::FQN(::std::string n, const FQN &p)
        : impl(::std::make_shared<Impl>(p.impl, ::std::move(n)))
    {
    }

    FQN::FQN(::std::string n)
        : impl(::std::make_shared<Impl>(nullptr, ::std::move(n)))
    {
    }

    FQN FQN::createChild(::std::string child) const
    {
      return FQN(::std::move(child), *this);
    }

    bool FQN::isSegment(const ::std::string &segment)
    {
      if (segment.empty()) {
        return false;
      }
      if (segment.find('.') != ::std::string::npos) {
        return false;
      }
      return true;
    }

    FQN FQN::create(::std::vector<::std::string> xsegments)
    {
      if (xsegments.empty()) {
        throw ::std::invalid_argument("Empty FQN");
      }
      FQN fqn(xsegments[0]);
      for (size_t i = 1; i < xsegments.size(); ++i) {
        fqn = fqn.createChild(xsegments[i]);
      }
      return fqn;
    }

    void FQN::print(::std::ostream &out) const
    {
      out << fullName(".");
    }

    ::std::optional<FQN> FQN::parent() const
    {
      auto p = impl->_parent;
      if (p) {
        return FQN(p);
      } else {
        return ::std::nullopt;
      }
    }

    bool FQN::isPrefixOf(const FQN &fqn) const
    {
      if (impl == fqn.impl) {
        return true;
      }
      auto theseSegments = localComponents();
      auto thoseSegments = fqn.localComponents();
      if (theseSegments.size() > thoseSegments.size()) {
        return false;
      }
      auto i = theseSegments.begin();
      auto j = thoseSegments.begin();

      while (i != theseSegments.end()) {
        if (*i != *j) {
          return false;
        }
        ++i, ++j;
      }
      return true;
    }

    FQN FQN::root() const
    {
      Impl::Ptr THIS = impl;
      while (THIS->_parent) {
        THIS = THIS->_parent;
      }
      return FQN(THIS);
    }

    size_t FQN::segmentCount() const
    {
      size_t n = 0;
      Impl::Ptr THIS = impl;
      while (THIS) {
        ++n;
        THIS = THIS->_parent;
      }
      return n;
    }

    ::std::vector<FQN> FQN::segments() const
    {
      ::std::vector<FQN> res;
      Impl::Ptr THIS = impl;
      while (THIS) {
        res.push_back(THIS);
        THIS = THIS->_parent;
      }
      ::std::reverse(res.begin(), res.end());
      return res;
    }

    ::std::vector<::std::string> FQN::localComponents() const
    {
      ::std::vector<::std::string> res;
      Impl::Ptr THIS = impl;
      while (THIS) {
        res.push_back(THIS->_name);
        THIS = THIS->_parent;
      }
      ::std::reverse(res.begin(), res.end());
      return res;
    }

    const ::std::string& FQN::localName() const
    {
      return impl->_name;
    }

    ::std::string FQN::fullName(const ::std::string &sep) const
    {
      ::std::string res;
      auto p = parent();
      if (p) {
        res += p->fullName(sep);
        res += sep;
      }
      res += localName();
      return res;
    }

    ::std::string FQN::fullName() const
    {
      ::std::string res;
      auto p = parent();
      if (p) {
        res += p->fullName();
        res += '.';
      }
      res += localName();
      return res;
    }

    ::std::optional<FQN> FQN::parseFQN(const ::std::string &n, char sep)
    {
      ::std::optional<FQN> p;
      ::std::string::size_type i = 0;
      try {
        for (::std::string::size_type j; (j = n.find(sep, i)) != ::std::string::npos; i = j + 1) {
          if (i == j) {
            return ::std::nullopt;
          }
          p = FQN(n.substr(i, j - i), p);
        }
        p = FQN(n.substr(i, ::std::string::npos), p);
      } catch (const ::std::invalid_argument&) {
        // ignore and return instead nothing
        return ::std::nullopt;
      }
      return p;
    }

    bool FQN::equals(const FQN &b) const
    {
      auto THIS = impl;
      auto THAT = b.impl;
      while (THIS && THAT) {
        if (THIS == THAT) {
          return true;
        }
        if (THIS->_name != THAT->_name) {
          return false;
        }
        THIS = THIS->_parent;
        THAT = THAT->_parent;
      }
      return THIS == THAT;
    }

    int FQN::compare(const FQN &b) const
    {
      auto sa = segments();
      auto sb = b.segments();
      for (size_t i = 0; i < sa.size() && i < sb.size(); ++i) {
        auto cmp = sa[i].impl->_name.compare(sb[i].impl->_name);
        if (cmp) {
          return cmp;
        }
      }
      if (sa.size() > sb.size()) {
        return 1;
      }
      if (sa.size() < sb.size()) {
        return -1;
      }
      return 0;
    }

    ::std::optional<FQN> FQN::findPrefix(const ::std::set<FQN> &prefixes) const
    {
      for (const auto &p : prefixes) {
        if (p.isPrefixOf(*this)) {
          return p;
        }
      }
      return ::std::nullopt;
    }

    ::std::set<FQN> FQN::normalizePrefixes(const ::std::set<FQN> &prefixes)
    {
      ::std::set<FQN> res;

      // only keep prefixes that are not themselves prefixed
      // e.g. A.B.C and A.B would only keep A.B
      for (auto i = prefixes.begin(); i != prefixes.end(); ++i) {
        bool isSubsumed = false;
        // check
        for (auto j = i; ++j != prefixes.end() && !isSubsumed;) {
          isSubsumed = j->isPrefixOf(*i);
        }
        if (!isSubsumed) {
          res.insert(*i);
        }
      }
      return res;
    }

  }
}
