#ifndef CLASS_MYLANG_NAMES_FQN_H
#define CLASS_MYLANG_NAMES_FQN_H
#include <iostream>
#include <memory>
#include <vector>
#include <string>
#include <optional>
#include <set>

namespace mylang {
  namespace names {

    /**
     * An FQN is hierarchical name.
     */
    class FQN
    {
      /**
       * An FQN is hierarchical name.
       */
    public:
      struct Impl
      {
        /** A shared pointer */
        typedef ::std::shared_ptr<const Impl> Ptr;

        /** Create a new FQN */
        Impl(Ptr p, ::std::string n);

        /** The parent */
        const Ptr _parent;

        /** The name */
        const ::std::string _name;
      };

    private:
      inline FQN(Impl::Ptr i)
          : impl(::std::move(i))
      {
      }

    public:
      FQN(::std::string n, const ::std::optional<FQN> &p);

    public:
      FQN(::std::string n, const FQN &p);

    public:
      explicit FQN(::std::string n);

      /** Destructor */
    public:
      ~FQN() = default;

      /**
       * Create a child FQN.
       * @param child a child name
       * @return a child name
       */
    public:
      FQN createChild(::std::string child) const;

      /**
       * Create an FQN from a list of name segments
       * @param segments the name segments
       * @return an FQN
       * @throws invalid_argument if segments is empty
       */
    public:
      static FQN create(::std::vector<::std::string> segments);

      /**
       * Determine if the specified string is a valid string for a segment.
       * A segment may not be empty!
       * @param segment a string for a segment
       * @return true if segment is valid, false otherwise
       */
    public:
      static bool isSegment(const ::std::string &segment);

      /**
       * Comparing FQNs
       */
    public:
      inline friend bool operator==(const FQN &a, const FQN &b)
      {
        return a.equals(b);
      }
      inline friend bool operator!=(const FQN &a, const FQN &b)
      {
        return a.equals(b) == false;
      }
      inline friend bool operator<(const FQN &a, const FQN &b)
      {
        return a.compare(b) < 0;
      }

      /** Compare */
    public:
      bool equals(const FQN &b) const;
      int compare(const FQN &b) const;

      /**
       * Write the fullname to the specified stream.
       * @param out an output stream
       * @param name a name pointer
       * @return out
       */
    public:
      friend inline ::std::ostream& operator<<(::std::ostream &out, const FQN &name)
      {
        name.print(out);
        return out;
      }

      /**
       * Write the fullname to the specified stream.
       * @param out an output stream
       * @param name a name pointer
       * @return out
       */
    public:
      void print(::std::ostream &out) const;

      /**
       * Count the number of segments that this FQN consists of.
       * @return the number of segments returned by `segments()`
       */
    public:
      size_t segmentCount() const;

      /**
       * Split this name into segments where the first segment is the top-most ancestor
       * and the last segment is this name
       * @return a vector of names that represent the FQN
       * @deprecated Use local components instead
       */
    public:
      ::std::vector<FQN> segments() const;

      /**
       * Split this name into segments of local component names.
       * @return a vector names representing the components of this fqn.
       */
    public:
      ::std::vector<::std::string> localComponents() const;

      /**
       * Get the last component of this FQN.
       * @return a string containing the local name.
       */
    public:
      const ::std::string& localName() const;

      /**
       * Convert this name into a text string using the specified
       * string as a separator.
       * @param sep a separator string
       * @return a string containing a separator character
       */
    public:
      ::std::string fullName(const ::std::string &sep) const;

      /**
       * Convert this name into a text string using using a . separator.
       * @return a string containing a separator character
       */
    public:
      ::std::string fullName() const;

      /**
       * Get the parent FQN
       * @return parent or nullptr if there is no parent
       */
    public:
      ::std::optional<FQN> parent() const;

      /**
       * Get the FQN that is the root component.
       * @return the first component of this FQN
       */
    public:
      FQN root() const;

      /**
       * Determine if this FQN is a prefix of the specified fqn or the same FQN
       * @param fqn another fqn
       * @return true if this fqn is an ancestor or this
       */
    public:
      bool isPrefixOf(const FQN &fqn) const;

      /**
       * Find an FQN that is a prefix of this FQN.
       * @param prefixes a set of prefixes
       * @return a prefix or nullopt if not found
       */
    public:
      ::std::optional<FQN> findPrefix(const ::std::set<FQN> &prefixes) const;

      /**
       * Create an FQN by parsing a delimited string.
       * @param n a name (may not be empty)
       * @param sep a separator
       * @return a new name
       */
    public:
      static ::std::optional<FQN> parseFQN(const ::std::string &n, char sep = '.');

      /**
       * A helper function to make a set of prefixes unique.
       * @param prefixes a set of prefixes
       * @return a new minimal set of prefixes
       */
    public:
      static ::std::set<FQN> normalizePrefixes(const ::std::set<FQN> &prefixes);

      /** The name */
    private:
      ::std::shared_ptr<const Impl> impl;
    };
  }
}
#endif
