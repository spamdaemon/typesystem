#ifndef CLASS_MYLANG_NAMES_NAME_H
#define CLASS_MYLANG_NAMES_NAME_H
#include <iostream>
#include <memory>
#include <vector>
#include <optional>

namespace mylang {
  namespace names {
    class FQN;

    /**
     * Every name is a unique tag for an object. Two names may
     * have the same fulltext representation, but represent different entities.
     * Names are compared by pointer equality
     */
    class Name: public std::enable_shared_from_this<Name>
    {
      /** A shared pointer */
    public:
      typedef ::std::shared_ptr<const Name> Ptr;

      /** Create a new name */
    private:
      Name(const Ptr &p, const ::std::string &n, bool fqn);

      /** Destructor */
    public:
      virtual ~Name() = 0;

      /**
       * Create a sibling name. The local name will be the same, but it will
       * have a different guid and local id.
       * @return a sibling name
       */
    public:
      virtual Ptr createSibling() const = 0;

      /**
       * Create a childname name.
       * @param child a child name
       * @return a child name
       */
    public:
      virtual Ptr createChild(const ::std::string &child) const = 0;

      /**
       * Determine if this name is an FQN.
       * @return true if this name is a FQN
       */
    public:
      inline bool isFQN() const
      {
        return _fqn;
      }

      /**
       * Write the fullname to the specified stream.
       * @param out an output stream
       * @param name a name pointer
       * @return out
       */
    public:
      friend inline ::std::ostream& operator<<(::std::ostream &out, const Ptr &name)
      {
        if (name) {
          name->print(out);
        }
        return out;
      }

      /**
       * Write the fullname to the specified stream.
       * @param out an output stream
       * @param name a name pointer
       * @return out
       */
    public:
      void print(::std::ostream &out) const;

      /**
       * Split this name into segments where the first segment is the top-most ancestor
       * and the last segment is this name
       * @return a vector names representing this names chain
       */
    public:
      virtual ::std::vector<Ptr> segments() const;

      /**
       * Get the local name for this name. The local name is the name part
       * without the ancestor. If this is an anonymous name, then it returns an empty string.
       * @param sep a separator string
       * @return a string containing the local name.
       */
    public:
      virtual ::std::string localName() const;

      /**
       * Convert this name into a text string using the specified
       * string as a separator.
       * @param sep a separator string
       * @return a string containing a separator character
       */
    public:
      virtual ::std::string fullName(const ::std::string &sep) const;

      /**
       * Convert this name into a text string using using a . separator.
       * @return a string containing a separator character
       */
    public:
      virtual ::std::string fullName() const;

      /**
       * Get the globally unique local name.
       * @return a unique name
       */
    public:
      virtual ::std::string uniqueLocalName() const;

      /**
       * Get the globally unique full name.
       * @param sep a separator string
       * @return a unique name
       */
    public:
      virtual ::std::string uniqueFullName(const ::std::string &sep) const;

      /**
       * Get the parent name
       * @return parent or null if there is no parent
       */
    public:
      inline Ptr parent() const
      {
        return _parent;
      }

      /**
       * Create a name from an FQN.
       * @param fqn an FQN
       * @return ptr a name
       */
    public:
      static Ptr create(const FQN &fqn);

      /**
       * Get the FQN that corresponds to this name.
       * If this name is not an FQN, then nothing is returned.
       * @return the FQN that consists of local names.
       */
    public:
      ::std::optional<FQN> fqn() const;

      /**
       * Get the unique FQN that corresponds to this name.
       * If this name is not an FQN, then nothing is returned.
       * @return the FQN that consists of the unique local names.
       */
    public:
      ::std::optional<FQN> uniqueFqn() const;

      /**
       * Get the parent that is root.
       * @return parent or null if there is no parent
       */
    public:
      Ptr root() const;

      /**
       * Create a new name that is distinct from any other name.
       * @param p a parent name (may be null)
       * @param n a name
       * @return a new name
       */
    public:
      static Ptr create(const ::std::string &n = ::std::string(), const Ptr &p = nullptr);

      /**
       * Create a full qualified name. If the parent is not a FQN, then
       * an error is thrown
       * @param n a name (may not be empty)
       * @param p a parent name (may be null)
       * @return a new name
       */
    public:
      static Ptr createFQN(const ::std::string &n, const Ptr &p = nullptr);

      /**
       * Create an FQN by parsing a delimited string.
       * @param n a name (may not be empty)
       * @param sep a separator
       * @return a new name
       */
    public:
      static Ptr parseFQN(const ::std::string &n, char sep = '.');

      /**
       * Get a new local id
       * @return the local id makes names unique with respect to the current name.
       */
    private:
      size_t nextLocalID() const;

      /** The unique id for this name; every instance of a Name has a its own guid.  */
    public:
      const size_t guid;

      /** The locally unique id */
    public:
      const size_t luid;

      /** The locally unique id */
    private:
      mutable size_t _nextLuid;

      /** The parent */
    private:
      const Ptr _parent;

      /** The name */
    private:
      const ::std::string _name;

      /** True if the name is an FQN */
    private:
      const bool _fqn;
    };
  }
}
#endif
