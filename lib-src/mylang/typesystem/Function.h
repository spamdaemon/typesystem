#ifndef CLASS_MYLANG_TYPESYSTEM_FUNCTION_H
#define CLASS_MYLANG_TYPESYSTEM_FUNCTION_H

#include <mylang/Self.h>
#include <mylang/typesystem/typesystem.h>
#include <mylang/typesystem/VarName.h>
#include <memory>
#include <optional>
#include <string>
#include <vector>
#include <set>

namespace mylang::typesystem {
  class Environment;

  /**
   * A generic function definition.
   *
   * The arguments to the function are constrained by the parameters and their types.
   * An example function definition might look like this:
   * <code>
   * eq:: a,b => a -> b -> bool
   * </code>
   * There are usually two kinds of functions, predicates and constructors.
   *
   * @see mylang::typesystem::Constructor
   */
  class Function: public Self<Function>
  {
    /**
     * The type parameters. Any type parameter used by the function appear in a parameter.
     * Type variables may not appear in the body of the function, unless they also appear in
     * the parameters.
     */
  public:
    using TypeParameters = ::std::vector<VarName>;

    /** The parameters of the function. These are variables that may use type parameters. */
  public:
    using Parameters = ::std::vector<VarPtr>;

    /**
     * The arguments to the function are expressions whose types must be unifiable with the
     * corresponding parameter.
     */
  public:
    using Arguments = ::std::vector<ExprPtr>;

    /**
     * Create a new function.
     * @param typeParams the parameters for the function
     * @param params the parameters for the function
     * @param body the body of the function
     * @throws ::std::invalid_argument if the body has free (type) variables
     */
  public:
    Function(TypeParameters typeParams, Parameters params, ExprPtr body);

    /** Destructor */
  public:
    ~Function();

    /**
     * Create a new function.
     * @param params the function parameters
     * @param body the body of the function
     * @return a function
     * @throws ::std::invalid_argument if the body has free variables
     */
  public:
    static ::std::shared_ptr<const Function> of(TypeParameters typeParams, Parameters params,
        ExprPtr body);

    /**
     * Substitute any variable references with the respective argument.
     * @param args the function arguments
     * @return an expression
     * @throws invalid_argument if the argument don't match the parameters
     */
  public:
    ExprPtr substitute(const Arguments &args) const;

    /**
     * Check the arguments against the parameters
     * @param args the function arguments
     * @return an error if the arguments do not match the parameters, nullopt otherwise
     */
  public:
    ::std::optional<::std::string> checkArguments(const Arguments &args) const;

    /**
     * Check the free variables in an expression against a set of parameters.
     * @param expr
     * @return an error message, or nullopt if there are no errors
     */
  public:
    ::std::optional<::std::string> checkFreeVariables(const ExprPtr &expr) const;

    /** The type variables used in the function */
  public:
    const TypeParameters typeParameters;

    /** The parameters */
  public:
    const Parameters parameters;

    /**
     * The function body.
     */
  public:
    const ExprPtr body;
  };
}
#endif
