#ifndef CLASS_MYLANG_TYPESYSTEM_TYPES_TTYPE_H
#define CLASS_MYLANG_TYPESYSTEM_TYPES_TTYPE_H

#include <mylang/typesystem/types/Type.h>

namespace mylang::typesystem::types {

  /**
   * A type representing text.
   */
  class TType: public Type
  {

    /** Default constructor */
  private:
    TType();

    /** Destructor */
  public:
    ~TType();

    /**
     * Get a new itype.
     * @return a new itype
     */
  public:
    static ::std::shared_ptr<const TType> create();

    void accept(TypeVisitor &v) const override final;
  };
}
#endif
