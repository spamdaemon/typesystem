#ifndef CLASS_MYLANG_TYPESYSTEM_TYPES_ITYPE_H
#define CLASS_MYLANG_TYPESYSTEM_TYPES_ITYPE_H

#include <mylang/typesystem/types/Type.h>

namespace mylang::typesystem::types {

  /**
   * A type representing an integer.
   */
  class IType: public Type
  {

    /** Default constructor */
  private:
    IType();

    /** Destructor */
  public:
    ~IType();

    /**
     * Get a new itype.
     * @return a new itype
     */
  public:
    static ::std::shared_ptr<const IType> create();
    void accept(TypeVisitor &v) const override final;

  };
}
#endif
