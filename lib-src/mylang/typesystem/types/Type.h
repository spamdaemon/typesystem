#ifndef CLASS_MYLANG_TYPESYSTEM_TYPES_TYPE_H
#define CLASS_MYLANG_TYPESYSTEM_TYPES_TYPE_H

#include <mylang/Self.h>
#include <mylang/typesystem/typesystem.h>

namespace mylang::typesystem::types {

  class TypeVisitor;

  /**
   * The base type for all expressions used in the typesystem.
   */
  class Type: public Self<Type>
  {

    /** Default constructor */
  protected:
    Type();

    /** Destructor */
  public:
    ~Type();

    /**
     * Determine if two types are unifiable.
     * @param other a type
     * @return true this type and other are unifiable
     */
  public:
    bool unifiable(const TypePtr &other) const;

    /**
     * Determine if two types are unifiable.
     * @param other a type
     * @return true this type and other are unifiable
     */
  public:
    template<class T>
    inline bool unifiable() const
    {
      return self<T>() != nullptr || isWildcard();
    }

    /**
     * Accept a visitor.
     * @param v a visitor
     */
  public:
    virtual void accept(TypeVisitor &v) const = 0;

    /** True if this is the special wildcard type */
  public:
    virtual bool isWildcard() const;
  };
}
#endif
