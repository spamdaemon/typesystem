#ifndef CLASS_MYLANG_TYPESYSTEM_TYPES_STYPE_H
#define CLASS_MYLANG_TYPESYSTEM_TYPES_STYPE_H

#include <mylang/typesystem/types/Type.h>
#include <vector>
#include <string>

namespace mylang::typesystem::types {

  /**
   * A type with named members.
   */
  class SType: public Type
  {

  public:
    using Members = ::std::vector<TypePtr>;

    /** Default constructor */
  private:
    SType(Members members);

    /** Destructor */
  public:
    ~SType();

    /**
     * Get a new structure type.
     * @param members the structure members
     * @return a structure type
     */
  public:
    static ::std::shared_ptr<const SType> create(Members members);

    void accept(TypeVisitor &v) const override final;

    /** The structure members */
  public:
    const Members members;
  };
}
#endif
