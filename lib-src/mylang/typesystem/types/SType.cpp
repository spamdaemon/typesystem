#include <mylang/typesystem/types/SType.h>
#include <mylang/typesystem/types/TypeVisitor.h>
#include <stdexcept>

namespace mylang::typesystem::types {

  SType::SType(Members m)
      : members(::std::move(m))
  {
  }

  SType::~SType()
  {
  }

  void SType::accept(TypeVisitor &v) const
  {
    v.visit(self<SType>());
  }

  ::std::shared_ptr<const SType> SType::create(Members m)
  {
    struct Impl: public SType
    {
      Impl(Members m)
          : SType(::std::move(m))
      {
      }

      ~Impl()
      {
      }

    };
    return ::std::make_shared<Impl>(::std::move(m));
  }

}
