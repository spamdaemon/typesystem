#ifndef CLASS_MYLANG_TYPESYSTEM_TYPES_BTYPE_H
#define CLASS_MYLANG_TYPESYSTEM_TYPES_BTYPE_H

#include <mylang/typesystem/types/Type.h>

namespace mylang::typesystem::types {

  /**
   * A type representing an boolean.
   */
  class BType: public Type
  {

    /** Default constructor */
  private:
    BType();

    /** Destructor */
  public:
    ~BType();

    /**
     * Get a new btype.
     * @return a new btype
     */
  public:
    static ::std::shared_ptr<const BType> create();

    void accept(TypeVisitor &v) const override final;

  };
}
#endif
