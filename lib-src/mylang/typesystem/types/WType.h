#ifndef CLASS_MYLANG_TYPESYSTEM_TYPES_WTYPE_H
#define CLASS_MYLANG_TYPESYSTEM_TYPES_WTYPE_H

#include <mylang/typesystem/types/Type.h>
#include <mylang/typesystem/VarName.h>

namespace mylang::typesystem::types {

  /**
   * A type variable.
   */
  class WType: public Type
  {
    /**
     * @param name the name of the type */
  public:
    WType(VarName name);

    /** Destructor */
  public:
    ~WType();

    /**
     * Create a type variable with the specified name
     * @param name the name
     * @return a type variable
     */
  public:
    static ::std::shared_ptr<const WType> of(VarName name);

    /**
     * Get a new type variable with a unique name.
     * @return a new type variable
     */
  public:
    static ::std::shared_ptr<const WType> create();

    void accept(TypeVisitor &v) const override final;

    /** This type is a wild-card type. */
  public:
    bool isWildcard() const override final;

    /** The name */
  public:
    const VarName name;
  };
}
#endif
