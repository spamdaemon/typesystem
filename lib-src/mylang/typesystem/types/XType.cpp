#include <mylang/typesystem/types/XType.h>
#include <mylang/typesystem/types/SType.h>
#include <mylang/typesystem/types/TypeVisitor.h>
#include <stdexcept>

namespace mylang::typesystem::types {

  XType::XType(TypePtr argTy)
      : arguments(::std::move(argTy))
  {
    if (!arguments->unifiable<types::SType>()) {
      throw ::std::invalid_argument("Expected a SType");
    }
  }

  XType::~XType()
  {
  }

  void XType::accept(TypeVisitor &v) const
  {
    v.visit(self<XType>());
  }

  ::std::shared_ptr<const XType> XType::create(TypePtr args)
  {
    struct Impl: public XType
    {
      Impl(TypePtr args)
          : XType(::std::move(args))
      {
      }
      ~Impl()
      {
      }

    };
    return ::std::make_shared<Impl>(::std::move(args));
  }

}
