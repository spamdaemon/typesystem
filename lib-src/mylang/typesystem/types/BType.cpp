#include <mylang/typesystem/types/BType.h>
#include <mylang/typesystem/types/TypeVisitor.h>

namespace mylang::typesystem::types {

  BType::BType()
  {
  }

  BType::~BType()
  {
  }

  void BType::accept(TypeVisitor &v) const
  {
    v.visit(self<BType>());
  }

  ::std::shared_ptr<const BType> BType::create()
  {
    struct Impl: public BType
    {
      ~Impl()
      {
      }

    };
    return ::std::make_shared<Impl>();
  }

}
