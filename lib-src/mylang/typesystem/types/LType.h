#ifndef CLASS_MYLANG_TYPESYSTEM_TYPES_LTYPE_H
#define CLASS_MYLANG_TYPESYSTEM_TYPES_LTYPE_H

#include <mylang/typesystem/types/Type.h>
#include <vector>

namespace mylang::typesystem::types {

  /**
   * A type representing a list.
   */
  class LType: public Type
  {

    /** Default constructor */
  private:
    LType(TypePtr element);

    /** Destructor */
  public:
    ~LType();

    /**
     * Get a new list type.
     * @param element the element type
     * @return a list type
     */
  public:
    static ::std::shared_ptr<const LType> create(TypePtr element);

    void accept(TypeVisitor &v) const override final;

    /** The members of this tuple */
  public:
    const TypePtr element;
  };
}
#endif
