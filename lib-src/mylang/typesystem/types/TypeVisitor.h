#ifndef CLASS_MYLANG_TYPESYSTEM_TYPES_TYPEVISITOR_H
#define CLASS_MYLANG_TYPESYSTEM_TYPES_TYPEVISITOR_H

#include <mylang/typesystem/types/types.h>

namespace mylang::typesystem::types {

  /**
   * The base type for all expressions used in the typesystem.
   */
  class TypeVisitor
  {
    /**
     * Create an expression with specified type.
     */
  protected:
    inline TypeVisitor() = default;

    /** Destructor */
  public:
    virtual ~TypeVisitor()
    {
    }
    ;

  public:
    virtual void visit(const ::std::shared_ptr<const BType> &e)=0;
    virtual void visit(const ::std::shared_ptr<const IType> &e)=0;
    virtual void visit(const ::std::shared_ptr<const LType> &e)=0;
    virtual void visit(const ::std::shared_ptr<const SType> &e)=0;
    virtual void visit(const ::std::shared_ptr<const TType> &e)=0;
    virtual void visit(const ::std::shared_ptr<const WType> &e)=0;
    virtual void visit(const ::std::shared_ptr<const XType> &e)=0;
  };
}
#endif
