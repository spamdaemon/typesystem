#ifndef FILE_MYLANG_TYPESYSTEM_TYPES_TYPES_H
#define FILE_MYLANG_TYPESYSTEM_TYPES_TYPES_H

#include <mylang/typesystem/types/BType.h>
#include <mylang/typesystem/types/IType.h>
#include <mylang/typesystem/types/LType.h>
#include <mylang/typesystem/types/SType.h>
#include <mylang/typesystem/types/TType.h>
#include <mylang/typesystem/types/WType.h>
#include <mylang/typesystem/types/XType.h>

#endif
