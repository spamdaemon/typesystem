#include <mylang/typesystem/types/TType.h>
#include <mylang/typesystem/types/TypeVisitor.h>

namespace mylang::typesystem::types {

  TType::TType()
  {
  }

  TType::~TType()
  {
  }

  void TType::accept(TypeVisitor &v) const
  {
    v.visit(self<TType>());
  }

  ::std::shared_ptr<const TType> TType::create()
  {
    struct Impl: public TType
    {
      ~Impl()
      {
      }

    };
    return ::std::make_shared<Impl>();
  }

}
