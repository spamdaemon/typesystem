#include <mylang/typesystem/types/WType.h>
#include <mylang/typesystem/types/TypeVisitor.h>

namespace mylang::typesystem::types {

  WType::WType(VarName n)
      : name(::std::move(n))
  {
  }

  WType::~WType()
  {
  }

  ::std::shared_ptr<const WType> WType::of(VarName name)
  {
    return ::std::make_shared<WType>(::std::move(name));
  }

  ::std::shared_ptr<const WType> WType::create()
  {
    return of(VarName());
  }

  bool WType::isWildcard() const
  {
    return true;
  }

  void WType::accept(TypeVisitor &v) const
  {
    v.visit(self<WType>());
  }

}
