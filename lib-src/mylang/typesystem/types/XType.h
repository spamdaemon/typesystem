#ifndef CLASS_MYLANG_TYPESYSTEM_TYPES_XTYPE_H
#define CLASS_MYLANG_TYPESYSTEM_TYPES_XTYPE_H

#include <mylang/typesystem/types/Type.h>

namespace mylang::typesystem::types {

  /**
   * A type representing a user-defined type.
   */
  class XType: public Type
  {
    /**
     * Constructor
     * @param argTy the type of the arguments
     * @throws ::std::invalid_argument if the argument type is not unifiable with a SType
     */
  private:
    XType(TypePtr argTy);

    /** Destructor */
  public:
    ~XType();

    /**
     * Get a new itype.
     * @param argTy the type of the arguments.
     * @return a new itype
     * @throws ::std::invalid_argument if the argument type is not unifiable with a SType
     */
  public:
    static ::std::shared_ptr<const XType> create(TypePtr argTy);

    void accept(TypeVisitor &v) const override final;

    /** The types of the arguments */
  public:
    const TypePtr arguments;
  };
}
#endif
