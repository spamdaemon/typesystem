#include <mylang/typesystem/types/LType.h>
#include <mylang/typesystem/types/TypeVisitor.h>

namespace mylang::typesystem::types {

  LType::LType(TypePtr e)
      : element(::std::move(e))
  {
  }

  LType::~LType()
  {
  }

  void LType::accept(TypeVisitor &v) const
  {
    v.visit(self<LType>());
  }

  ::std::shared_ptr<const LType> LType::create(TypePtr e)
  {
    struct Impl: public LType
    {
      Impl(TypePtr m)
          : LType(::std::move(m))
      {
      }

      ~Impl()
      {
      }

    };
    return ::std::make_shared<Impl>(::std::move(e));
  }
  ;
}
