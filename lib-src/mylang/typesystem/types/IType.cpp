#include <mylang/typesystem/types/IType.h>
#include <mylang/typesystem/types/TypeVisitor.h>

namespace mylang::typesystem::types {

  IType::IType()
  {
  }

  IType::~IType()
  {
  }

  void IType::accept(TypeVisitor &v) const
  {
    v.visit(self<IType>());
  }

  ::std::shared_ptr<const IType> IType::create()
  {
    struct Impl: public IType
    {
      ~Impl()
      {
      }

    };
    return ::std::make_shared<Impl>();
  }

}
