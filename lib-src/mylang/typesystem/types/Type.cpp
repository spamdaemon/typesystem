#include <mylang/typesystem/types/Type.h>
#include <mylang/typesystem/algorithms/Unify.h>

namespace mylang::typesystem::types {

  Type::Type()
  {
  }

  Type::~Type()
  {
  }

  bool Type::unifiable(const TypePtr &other) const
  {
    return algorithms::Unify::apply(self(), other, { }).second == algorithms::Unify::UNIFIED;
  }

  bool Type::isWildcard() const
  {
    return false;
  }

}
