#include <mylang/typesystem/Environment.h>

namespace mylang::typesystem {

  void Environment::updateBinding(VarName name, ExprPtr expr)
  {
    if (!expr) {
      throw ::std::invalid_argument("Nullptr");
    }
    bindings = bindings.insert(::std::move(name), expr);
  }

  void Environment::updateBinding(VarName name, TypePtr expr)
  {
    if (!expr) {
      throw ::std::invalid_argument("Nullptr");
    }
    typeBindings = typeBindings.insert(::std::move(name), expr);
  }

  void Environment::addFunction(::std::string name, FunctionPtr f)
  {
    if (!f) {
      throw ::std::invalid_argument("Nullptr");
    }
    functions = functions.insert(name, f);
  }

  void Environment::addType(::std::string name, TypeDefinitionPtr t)
  {
    if (!t) {
      throw ::std::invalid_argument("Nullptr");
    }
    types = types.insert(name, t);
  }

}
