#ifndef CLASS_MYLANG_TYPESYSTEMS_EVAL_EVAL_H
#define CLASS_MYLANG_TYPESYSTEMS_EVAL_EVAL_H

#include <mylang/typesystem/exprs/ExprTransform.h>
#include <mylang/typesystem/Environment.h>

namespace mylang::typesystem::eval {

  /**
   * Evaluate an expression tree within an environment.
   */
  struct Eval
  {

    /**
     * Evaluate the specified expression.
     *
     * Evaluating the specified node returns either the node,
     * or a new node where some or all parts of the given expression
     * have been evaluated and turned into literals.
     * If the expression fails to evaluate, then the error expression
     * is returned.
     *
     * @param e an expression
     * @param env an environment
     * @return a new expression
     */
    static Tx<ExprPtr> eval(ExprPtr e, const Environment &env);

    /**
     * Evaluate the specified variable bindings.
     *
     * Evaluating the specified node returns either the node,
     * or a new node where some or all parts of the given expression
     * have been evaluated and turned into literals.
     * If the expression fails to evaluate, then the error expression
     * is returned.
     *
     * @param e an expression
     * @param env an environment
     * @return a new expression
     */
    static Tx<ExprPtr> eval(ExprPtr e, const Environment::Bindings &env);
  };

}

#endif
