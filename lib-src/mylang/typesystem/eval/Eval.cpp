#include <mylang/typesystem/eval/Eval.h>
#include <mylang/typesystem/algorithms/PruneUnusedVariables.h>
#include <mylang/BigInt.h>
#include <mylang/Integer.h>
#include <mylang/Self.h>
#include <mylang/typesystem/Environment.h>
#include <mylang/typesystem/typesystem.h>
#include <mylang/typesystem/RBTree.h>
#include <mylang/typesystem/predicates/IsConstant.h>
#include <mylang/typesystem/VarName.h>
#include <mylang/typesystem/TypeDefinition.h>

#include <mylang/typesystem/exprs/exprs.h>
#include <mylang/typesystem/types/types.h>
#include <mylang/typesystem/exprs/ExprTransform.h>
#include <mylang/typesystem/exprs/TransformVisitor.h>

#include <cassert>
#include <iostream>

namespace mylang::typesystem::eval {
  using namespace exprs;
  using namespace algorithms;

  namespace {

    ExprPtr evalADD(const TypePtr &ty, const ::std::vector<ExprPtr> &operands)
    {
      auto left = operands.at(0)->self<ILiteral>();
      auto right = operands.at(1)->self<ILiteral>();
      if (left && left->value.sign() == 0) {
        return operands.at(1);
      }
      if (right && right->value.sign() == 0) {
        return operands.at(0);
      }

      if (left && right) {
        if (auto sum = left->value.add(right->value); sum.has_value()) {
          return ILiteral::of(*sum);
        } else {
          return ErrorExpr::of(ty);
        }
      }
      return nullptr;
    }

    ExprPtr evalALL(const TypePtr&, const ::std::vector<ExprPtr> &operands)
    {
      ::std::vector<ExprPtr> newOps;
      newOps.reserve(operands.size());
      for (auto op : operands) {
        if (auto e = op->self<BLiteral>(); e) {
          if (e->value == false) {
            return BLiteral::of(false);
          }
        } else {
          newOps.push_back(op);
        }
      }
      if (newOps.empty()) {
        return BLiteral::of(true);
      }
      if (newOps.size() == operands.size()) {
        return nullptr;
      } else {
        return BuiltinExpr::allOf(::std::move(newOps));
      }
    }

    ExprPtr evalANY(const TypePtr&, const ::std::vector<ExprPtr> &operands)
    {
      ::std::vector<ExprPtr> newOps;
      newOps.reserve(operands.size());
      for (auto op : operands) {
        if (auto e = op->self<BLiteral>(); e) {
          if (e->value) {
            return BLiteral::of(true);
          }
        } else {
          newOps.push_back(op);
        }
      }
      if (newOps.empty()) {
        return BLiteral::of(false);
      }
      if (newOps.size() == operands.size()) {
        return nullptr;
      } else {
        return BuiltinExpr::anyOf(::std::move(newOps));
      }
    }

    ExprPtr evalAT(const TypePtr &ty, const ::std::vector<ExprPtr> &operands)
    {
      auto i = operands.at(0)->self<ILiteral>();
      if (!i->value.value()) {
        // infinity does not work
        return ErrorExpr::of(ty);
      }
      auto x = i->value.value()->toSize_t();
      if (!x) {
        return nullptr;
      }

      if (auto list = operands.at(1)->self<ListLiteral>()) {
        if (*x < list->entries.size()) {
          return list->entries.at(*x);
        } else {
          return ErrorExpr::of(ty);
        }
      }
      if (auto cons = operands.at(1)->self<BuiltinExpr>(); cons && cons->op == BuiltinExpr::CONS) {
        if (*x == 0) {
          return cons->operands.at(0);
        } else {
          return BuiltinExpr::atIndexOf(ILiteral::of(*x - 1), cons->operands.at(1));
        }
      }
      return nullptr;
    }

    ExprPtr evalCONS(const TypePtr&, const ::std::vector<ExprPtr> &operands)
    {
      if (auto right = operands.at(1)->self<ListLiteral>(); right) {
        ::std::vector<ExprPtr> elems;
        elems.reserve(1 + right->entries.size());
        elems.push_back(operands.at(0));
        elems.insert(elems.end(), right->entries.begin(), right->entries.end());
        return ListLiteral::of(right->type->self<types::LType>(), ::std::move(elems));
      }
      return nullptr;
    }

    ExprPtr evalEQ(const TypePtr&, const ::std::vector<ExprPtr> &operands)
    {
      {
        auto left = operands.at(0)->self<VarExpr>();
        auto right = operands.at(1)->self<VarExpr>();

        if (left && right && left->name == right->name) {
          return BLiteral::of(true);
        }
      }

      {
        auto left = operands.at(0)->self<LiteralExpr>();
        auto right = operands.at(1)->self<LiteralExpr>();

        if (left && right) {
          if (auto ty = left->type->self<types::BType>(); ty) {
            return BLiteral::of(left->self<BLiteral>()->value == right->self<BLiteral>()->value);
          }
          if (auto ty = left->type->self<types::IType>(); ty) {
            return BLiteral::of(left->self<ILiteral>()->value == right->self<ILiteral>()->value);
          }
          if (auto ty = left->type->self<types::TType>(); ty) {
            return BLiteral::of(left->self<TLiteral>()->value == right->self<TLiteral>()->value);
          }
        }
      }

      {
        auto left = operands.at(0)->self<ListLiteral>();
        auto right = operands.at(1)->self<ListLiteral>();
        if (left && right) {
          if (left->entries.size() != right->entries.size()) {
            return BLiteral::of(false);
          }
          ExprPtr x = BLiteral::of(true);
          for (size_t i = 0; i < left->entries.size(); ++i) {
            x = BuiltinExpr::andOf(x, BuiltinExpr::eqOf(left->entries.at(i), right->entries.at(i)));
          }
          return x;
        }
      }

      {
        auto left = operands.at(0)->self<ConstructExpr>();
        auto right = operands.at(1)->self<ConstructExpr>();
        if (left && right) {
          // if the types don't match we have an invalid eq expression
          assert(left->arguments.size() == right->arguments.size());

          ExprPtr x = BLiteral::of(true);
          for (size_t i = 0; i < left->arguments.size(); ++i) {
            x = BuiltinExpr::andOf(x,
                BuiltinExpr::eqOf(left->arguments.at(i), right->arguments.at(i)));
          }
          return x;
        }
      }

      {
        auto left = operands.at(0)->self<TypeExpr>();
        auto right = operands.at(1)->self<TypeExpr>();
        if (left && right) {
          return BuiltinExpr::andOf(BuiltinExpr::eqOf(left->name, right->name),
              BuiltinExpr::eqOf(left->argument, right->argument));
        }
      }
      return nullptr;
    }

    ExprPtr evalLENGTH(const TypePtr&, const ::std::vector<ExprPtr> &operands)
    {
      if (auto list = operands.at(0)->self<ListLiteral>(); list) {
        return ILiteral::of(list->entries.size());
      }
      if (auto cons = operands.at(0)->self<BuiltinExpr>(); cons && cons->op == BuiltinExpr::CONS) {
        return BuiltinExpr::sumOf(ILiteral::of(1), BuiltinExpr::lengthOf(cons->operands.at(1)));
      }
      return nullptr;
    }

    ExprPtr evalLT(const TypePtr&, const ::std::vector<ExprPtr> &operands)
    {
      {
        auto left = operands.at(0)->self<LiteralExpr>();
        auto right = operands.at(1)->self<LiteralExpr>();

        if (left && right) {
          if (auto ty = left->type->self<types::IType>(); ty) {
            return BLiteral::of(left->self<ILiteral>()->value < right->self<ILiteral>()->value);
          }
          if (auto ty = left->type->self<types::TType>(); ty) {
            return BLiteral::of(left->self<TLiteral>()->value < right->self<TLiteral>()->value);
          }
        }
      }
      return nullptr;
    }

    ExprPtr evalLTE(const TypePtr &t, const ::std::vector<ExprPtr> &operands)
    {
      {
        auto left = operands.at(0)->self<LiteralExpr>();
        auto right = operands.at(1)->self<LiteralExpr>();

        if (left && right) {
          if (auto ty = left->type->self<types::IType>(); ty) {
            return BLiteral::of(left->self<ILiteral>()->value <= right->self<ILiteral>()->value);
          }
          if (auto ty = left->type->self<types::TType>(); ty) {
            return BLiteral::of(left->self<TLiteral>()->value <= right->self<TLiteral>()->value);
          }
        }
      }

      if (auto eq = evalEQ(t, operands); eq) {
        if (auto b = eq->self<BLiteral>(); b && b->value) {
          return b;
        }
      }

      if (auto lt = evalLT(t, operands); lt) {
        if (auto b = lt->self<BLiteral>(); b && b->value) {
          return b;
        }
      }

      return nullptr;
    }

    ExprPtr evalNEGATE(const TypePtr&, const ::std::vector<ExprPtr> &operands)
    {
      if (auto arg = operands.at(0)->self<ILiteral>(); arg) {
        return ILiteral::of(arg->value->negate());
      }
      if (auto b = operands.at(0)->self<BuiltinExpr>(); b && b->op == BuiltinExpr::NEGATE) {
        // negate(negate(x)) ==> x
        return b->operands.at(0);
      }
      return nullptr;
    }

    ExprPtr evalNOT(const TypePtr&, const ::std::vector<ExprPtr> &operands)
    {
      if (auto arg = operands.at(0)->self<BLiteral>(); arg) {
        return BLiteral::of(!arg->value);
      }
      if (auto b = operands.at(0)->self<BuiltinExpr>(); b && b->op == BuiltinExpr::NOT) {
        // not(not(x)) ==> x
        return b->operands.at(0);
      }
      return nullptr;
    }

    Tx<ExprPtr> evalNode(ExprPtr e, const Environment &env)
    {
      struct V: public ExprTransform
      {
        const Environment env;

        V(Environment xenv)
            : env(xenv)
        {
        }

        ~V()
        {
        }

        // the return values of this function are as follows;
        // {error, _ } -> error
        // {_, nullopt } -> not enough information
        // {_, not nullptr } -> valid type definition
        // {_, not nullptr } -> invalid type definition

        ::std::pair<Tx<ExprPtr>, ::std::optional<TypeDefinitionPtr>> lookupTypeDefinition(
            const ::std::shared_ptr<const TypeExpr> &e)
        {
          auto name = e->name->self<TLiteral>();
          auto args = e->argument->self<ConstructExpr>();
          Tx<ExprPtr> newExpr = noChange(e);
          if (name == nullptr || args == nullptr) {
            return {newExpr,std::nullopt};
          }
          auto t = env.types.find(name->value);
          if (t == nullptr) {
            // if the type doesn't exist, then we have an error
            return {newExpr,::std::nullopt};
          }

          ::std::optional<TypeDefinitionPtr> optDef;
          try {
            TypeDefinitionPtr def = t->second;
            // TODO: do more than just the check the arguments
            if (auto err = def->constructor->checkArguments(args->arguments); err) {
              optDef = nullptr;
            } else {
              optDef = def;
            }
          } catch (const ::std::exception &ex) {
            newExpr = newChange(ErrorExpr::of(e->type));
          }

          return {newExpr,optDef};
        }

        Tx<ExprPtr> visit(const ::std::shared_ptr<const CallExpr> &e)
        override
        {
          if (auto tmp = ExprTransform::visit(e); tmp.isNew) {
            return tmp;
          }

          return newChange(e->target->substitute(e->arguments));
        }

        Tx<ExprPtr> visit(const ::std::shared_ptr<const TypeExpr> &e) override
        {
          if (auto tmp = ExprTransform::visit(e); tmp.isNew) {
            return tmp;
          }
          auto name = e->name->self<TLiteral>();
          auto args = e->argument->self<ConstructExpr>();
          if (name == nullptr || args == nullptr) {
            return noChange(e);
          }

          // we just need to lookup the definition
          auto def = lookupTypeDefinition(e);
          if (def.first.actual->type->self<ErrorExpr>()) {
            return def.first;
          }
          if (def.second.has_value() && def.second.value() == nullptr) {
            // constraints on the type are not satisfied
            return newChange(ErrorExpr::of(e->type));
          }
          return def.first;
        }

        Tx<ExprPtr> visit(const ::std::shared_ptr<const VarExpr> &e) override
        {
          if (auto b = env.bindings.find(e->name); b) {
            return newChange(b->second);
          }
          return noChange(e);
        }

        Tx<ExprPtr> visit(const ::std::shared_ptr<const LetExpr> &e)
        override
        {
          auto value = transform(e->value);

          Environment newEnv(env);
          newEnv.updateBinding(e->variable, value.actual);
          V v(newEnv);
          auto result = v.transform(e->result);
          if (result.isNew || value.isNew) {
            return newChange(LetExpr::of(e->variable, value.actual, result.actual));
          }
          return noChange(e);
        }

        Tx<ExprPtr> visit(const ::std::shared_ptr<const ConditionalExpr> &e)
        override
        {
          auto c = transform(e->condition);
          if (c.isNew) {
            return newChange(ConditionalExpr::of(c.actual, e->iftrue, e->iffalse));
          }

          if (auto test = e->condition->self<BLiteral>(); test) {
            ExprPtr actual;
            if (test->value) {
              actual = transform(e->iftrue).actual;
            } else {
              actual = transform(e->iffalse).actual;
            }
            return newChange(::std::move(actual));
          }
          return noChange(e);
        }

        Tx<ExprPtr> visit(const ::std::shared_ptr<const BuiltinExpr> &e)
        override
        {
          if (auto tmp = ExprTransform::visit(e); tmp.isNew) {
            return tmp;
          }

          ExprPtr res = nullptr; // no change
          switch (e->op) {
          case BuiltinExpr::ADD:
            res = evalADD(e->type, e->operands);
            break;
          case BuiltinExpr::ALL:
            res = evalALL(e->type, e->operands);
            break;
          case BuiltinExpr::ANY:
            res = evalANY(e->type, e->operands);
            break;
          case BuiltinExpr::AT:
            res = evalAT(e->type, e->operands);
            break;
          case BuiltinExpr::CONS:
            res = evalCONS(e->type, e->operands);
            break;
          case BuiltinExpr::EQ:
            res = evalEQ(e->type, e->operands);
            break;
          case BuiltinExpr::LENGTH:
            res = evalLENGTH(e->type, e->operands);
            break;
          case BuiltinExpr::LT:
            res = evalLT(e->type, e->operands);
            break;
          case BuiltinExpr::LTE:
            res = evalLTE(e->type, e->operands);
            break;
          case BuiltinExpr::NEGATE:
            res = evalNEGATE(e->type, e->operands);
            break;
          case BuiltinExpr::NOT:
            res = evalNOT(e->type, e->operands);
            break;
          }
          if (res) {
            return newChange(res);
          } else {
            return noChange(e);
          }
        }
      }
      ;

      return V(env).transform(e);
    }

    Tx<ExprPtr> evalOnce(ExprPtr e, const Environment &env)
    {
      using namespace ::std::placeholders;
      Tx<ExprPtr> res(e, false);
// remove unused variables
      res <<= PruneUnusedVariables::prune;
// evaluate the expressions
      res <<= ::std::bind(evalNode, _1, env);
      return res;
    }

  }

  Tx<ExprPtr> Eval::eval(ExprPtr e, const Environment &env)
  {
    bool isNew = false;
    Tx<ExprPtr> res(e, true);
    while (res.isNew) {
      res = evalOnce(res.actual, env);
      isNew = isNew || res.isNew;
    }
    return Tx<ExprPtr>(res.actual, isNew);
  }

  Tx<ExprPtr> Eval::eval(ExprPtr e, const Environment::Bindings &binds)
  {
    Environment env;
    env.bindings = binds;
    return eval(e, env);
  }

}
