#ifndef CLASS_MYLANG_TYPESYSTEM_ENVIRONMENT_H
#define CLASS_MYLANG_TYPESYSTEM_ENVIRONMENT_H

#include <mylang/typesystem/typesystem.h>
#include <mylang/typesystem/RBTree.h>
#include <mylang/typesystem/VarName.h>
#include <string>

namespace mylang::typesystem {

  /**
   * The environment for evaluating and unifying type expressions. */
  class Environment
  {
    /** The variable bindings */
  public:
    using Bindings = RBTree<VarName,ExprPtr>;
    /** The variable bindings */
  public:
    using TypeBindings = RBTree<VarName,TypePtr>;

    /** A list of registered functions */
  public:
    using Functions = RBTree<::std::string,FunctionPtr>;

    /** The defined types */
  public:
    using Types = RBTree<::std::string,TypeDefinitionPtr>;

  public:
    Environment() = default;

  public:
    ~Environment() = default;

    /**
     * Update or insert a binding.
     * @param name the name
     * @param expr the expression
     */
  public:
    void updateBinding(VarName name, ExprPtr expr);

    /**
     * Update or insert a binding.
     * @param name the name
     * @param expr the expression
     */
  public:
    void updateBinding(VarName name, TypePtr expr);

    /**
     * Add a function into this environment.
     * @param name the name of a function
     * @param f the function
     */
  public:
    void addFunction(::std::string name, FunctionPtr f);

    /**
     * Add a new type into this environment.
     * @param name the name of a type
     * @param t the type
     */
  public:
    void addType(::std::string name, TypeDefinitionPtr t);

    /** The expected type */
  public:
    Bindings bindings;

    /** The expected type */
  public:
    TypeBindings typeBindings;

    /** The functions that are known in the environment */
  public:
    Functions functions;

    /** The types */
  public:
    Types types;
  };
}
#endif
