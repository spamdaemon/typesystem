#ifndef CLASS_MYLANG_TYPESYSTEM_AST_ATYPECHECKER_H
#define CLASS_MYLANG_TYPESYSTEM_AST_ATYPECHECKER_H

#include <stdexcept>

#ifndef CLASS_MYLANG_TYPESYSTEM_AST_AENVIRONMENT_H
#include <mylang/typesystem/ast/AEnvironment.h>
#endif

#ifndef CLASS_MYLANG_TYPESYSTEM_AST_AEXPRESSION_H
#include <mylang/typesystem/ast/AExpression.h>
#endif

namespace mylang::typesystem::ast {

  /**
   * The type checker checks an expression for correctness.
   */
  class ATypeChecker
  {
    /** A class that represents a type check error */
  public:
    struct TypeError: public ::std::runtime_error
    {
      TypeError(::std::string msg);

      ~TypeError();
    };

    ATypeChecker();
    ~ATypeChecker();

    /**
     * Check that an expression is type-correct in the specified environment.
     * @param env an environment
     * @param expr an expression
     * @return an updated environment that allows the expression to type-check
     * @throws TypeError if the expression does not type check
     */
  public:
    static AEnvironment::Ptr typecheck(const AEnvironment::Ptr &env, const AExpression::Ptr &e);
  };
}
#endif
