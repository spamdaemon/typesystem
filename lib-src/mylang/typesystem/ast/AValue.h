#ifndef CLASS_MYLANG_TYPESYSTEM_AST_AVALUE_H
#define CLASS_MYLANG_TYPESYSTEM_AST_AVALUE_H

#ifndef CLASS_MYLANG_TYPESYSTEM_AST_AEXPRESSION_H
#include <mylang/typesystem/ast/AExpression.h>
#endif

namespace mylang::typesystem::ast {

  /**
   * An expression that represents a value.
   */
  class AValue: public AExpression
  {
    /** A pointer to an expression */
  public:
    using Ptr = ::std::shared_ptr<const AValue>;

    /** The type of the expression */
  public:
    using ATypePtr = AType::Ptr;

    /**
     * An expression that calls a function with a list of arguments
     * @param the expected type of the value
     */
  public:
    AValue(ATypePtr expected);

    /** Destructor */
  public:
    ~AValue();

    /**
     * Create a new expression that expresses a variable or literal.
     * @param expected the expected type
     * @return an expression that returns the specified type
     */
  public:
    static Ptr of(ATypePtr expected);

    /**
     * Create a new expression that expresses a variable or literal.
     * @param expected the expected type
     * @return an expression that returns the specified type
     */
  public:
    static Ptr of(Pattern expected);

    /**
     * Create a new expression that expresses a variable or literal.
     * @param expected the expected type
     * @return an expression that returns the specified type
     */
  public:
    static Ptr of(ExprPtr expected);

    /**
     * Create a new expression that expresses a variable or literal.
     * @param expected the expected type
     * @return an expression that returns the specified type
     */
  public:
    static Ptr of(TypePtr expected);

    /**
     * Generate an equivalent expression.
     * @return the underlying expression
     */
  public:
    ExprPtr toExpr(const AEnvironment&) const override;
  };
}
#endif
