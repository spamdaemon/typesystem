#ifndef CLASS_MYLANG_TYPESYSTEM_AST_AFUNCTIONSPEC_H
#define CLASS_MYLANG_TYPESYSTEM_AST_AFUNCTIONSPEC_H

#ifndef CLASS_MYLANG_TYPESYSTEM_AST_ATYPE_H
#include <mylang/typesystem/ast/AType.h>
#endif

#ifndef CLASS_MYLANG_TYPESYSTEM_AST_ASIGNATURE_H
#include <mylang/typesystem/ast/ASignature.h>
#endif

#ifndef CLASS_MYLANG_TYPESYSTEM_AST_AFUNCTIONNAME_H
#include <mylang/typesystem/ast/AFunctionName.h>
#endif

namespace mylang::typesystem::ast {

  /**
   * A function specification.
   *
   * The specification consists of the name of the function
   * and its signature.
   */
  class AFunctionSpec
  {
    /** A pointer to an expression */
  public:
    using Ptr = ::std::shared_ptr<const AFunctionSpec>;

    /** The type of the expression */
  public:
    using ASignaturePtr = ASignature::Ptr;

    /**
     * Create a function.
     * @param name the name of the function
     * @param signature the function signature
     */
  public:
    AFunctionSpec(AFunctionName name, ASignature::Ptr signature);

    /** Destructor */
  public:
    ~AFunctionSpec();

    /**
     * Create a new function with a return type and positional parameters.
     *
     * @param name the name of a function
     * @param signature the function's positional parameters
     */
  public:
    static Ptr of(AFunctionName name, ASignature::Ptr signature);

    /**
     * Freshen this spec by freshening the signature.
     *
     * This function should be called when a fun
     */
  public:
    Ptr freshen() const;

    /** The function name */
  public:
    const AFunctionName name;

    /** The return type of this function */
  public:
    const ASignaturePtr signature;
  };
}
#endif
