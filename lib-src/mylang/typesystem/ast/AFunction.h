#ifndef CLASS_MYLANG_TYPESYSTEM_AST_AFUNCTION_H
#define CLASS_MYLANG_TYPESYSTEM_AST_AFUNCTION_H

#include <memory>
#include <vector>
#include <utility>

#ifndef CLASS_MYLANG_TYPESYSTEM_AST_ATYPE_H
#include <mylang/typesystem/ast/AType.h>
#endif

#ifndef CLASS_MYLANG_TYPESYSTEM_AST_ASIGNATURE_H
#include <mylang/typesystem/ast/ASignature.h>
#endif

#ifndef FILE_MYLANG_TYPESYSTEM_H
#include <mylang/typesystem/typesystem.h>
#endif

#ifndef CLASS_MYLANG_TYPESYSTEM_AST_AFUNCTIONNAME_H
#include <mylang/typesystem/ast/AFunctionName.h>
#endif

namespace mylang::typesystem::ast {

  /**
   * A general expression of some type.
   */
  class AFunction
  {
    /** A pointer to an expression */
  public:
    using Ptr = ::std::shared_ptr<const AFunction>;

    /** The type of the expression */
  public:
    using ATypePtr = AType::Ptr;

    /** The type of the expression */
  public:
    using ASignaturePtr = ASignature::Ptr;

    /** The arguments arguments to the expression (if any) */
  public:
    using Parameters = ::std::vector<ATypePtr>;

    /** The arguments arguments to the expression (if any) */
  public:
    using Arguments = ::std::vector<VarPtr>;

    /**
     * Create a function specification
     * @param name the name of the spec
     * @param sig the signature
     * @param retVar the variable for the return type
     * @param argVar the variable for the arguments
     * @param bodyConstraint a constraint expression for the body and is allowed to use the retVar and argVar
     */
  public:
    AFunction(ASignaturePtr sig, VarPtr retVar, Arguments argVar, ExprPtr b);

    /** Destructor */
  public:
    ~AFunction();

    /**
     * Create a new function with a return type and positional parameters.
     *
     * @param retTy the function's return type
     * @param params the function's positional parameters
     * @param constraintFN a function that produces a constraint expression
     */
  public:
    static Ptr of(ATypePtr retTy, Parameters params,
        ::std::function<ExprPtr(VarPtr, const Arguments&)> constraintFN = nullptr);

    /**
     * Create a new function with a return type and positional parameters.
     *
     * @param retTy the function's return type
     * @param param the function single parameter
     * @param constraintFN a function that produces a constraint expression
     */
  public:
    static Ptr ofUnary(ATypePtr retTy, ATypePtr param,
        ::std::function<ExprPtr(VarPtr, VarPtr)> constraintFN = nullptr);

    /**
     * Create a new function with a return type and positional parameters.
     *
     * @param retTy the function's return type
     * @param param0 the function's first positional parameters
     * @param param1 the function's second positional parameters
     * @param constraintFN a function that produces a constraint expression
     */
  public:
    static Ptr ofBinary(ATypePtr retTy, ATypePtr param0, ATypePtr param1,
        ::std::function<ExprPtr(VarPtr, VarPtr, VarPtr)> constraintFN = nullptr);

    /**
     * Create a new function where are local variables are fresh.
     */
  public:
    Ptr freshen(Environment env) const;

    /** The return type of this function */
  public:
    const ASignaturePtr signature;

    /** A variable that represents the return type of the function in the constraint expression */
  public:
    const VarPtr result;

    /** The variables for each positional parameter in the signature */
  public:
    const Arguments arguments;

    /**
     * The constraint expression for the body. The body may contain free variables, which should
     * be treated as local variables.
     */
  public:
    const ExprPtr body;

  };
}
#endif
