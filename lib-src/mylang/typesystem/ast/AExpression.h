#ifndef CLASS_MYLANG_TYPESYSTEM_AST_AEXPRESSION_H
#define CLASS_MYLANG_TYPESYSTEM_AST_AEXPRESSION_H

#ifndef CLASS_MYLANG_TYPESYSTEM_AST_ATYPE_H
#include <mylang/typesystem/ast/AType.h>
#endif

#ifndef CLASS_MYLANG_TYPESYSTEM_AST_AENVIRONMENT_H
#include <mylang/typesystem/ast/AEnvironment.h>
#endif

#ifndef CLASS_MYLANG_SELF_H
#include <mylang/Self.h>
#endif

namespace mylang::typesystem::ast {

  /**
   * An expression that can be used to model the structure
   * of an actual AST.
   *
   * All expressions yield values of some type. To model the
   * structure of an AST, each expression is modeled after a
   * function call. If the expression does not model a function call,
   * i.e., function is nullptr, then the expression models a literal, variable,
   * or something of that nature.
   *
   * Typically, the expression models the behavior of a single statement
   * in terms of types.
   *
   * @see mylang::typesystem::ast::AFunction
   * @see mylang::typesystem::ast::AType
   */
  class AExpression: public Self<AExpression>
  {
    /** A pointer to an expression */
  public:
    using Ptr = ::std::shared_ptr<const AExpression>;

    /** The type of the expression */
  public:
    using ATypePtr = AType::Ptr;

    /**
     * An expression that calls a function with a list of arguments
     * @param expected the expected type
     */
  protected:
    AExpression(ATypePtr expected);

    /** Destructor */
  public:
    virtual ~AExpression();

    /**
     * Generate an equivalent expression.
     * @param env an environment
     * @return the underlying expression
     */
  public:
    virtual ExprPtr toExpr(const AEnvironment &env) const = 0;

    /** The type of this expression */
  public:
    const ATypePtr type;
  };
}
#endif
