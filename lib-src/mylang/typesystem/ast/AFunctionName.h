#ifndef CLASS_MYLANG_TYPESYSTEM_AST_AFUNCTIONNAME_H
#define CLASS_MYLANG_TYPESYSTEM_AST_AFUNCTIONNAME_H

#include <memory>
#include <vector>

#ifndef CLASS_MYLANG_TYPESYSTEM_VARNAME_H
#include <mylang/typesystem/VarName.h>
#endif

namespace mylang::typesystem::ast {

  using AFunctionName = ::mylang::typesystem::VarName;
}
#endif
