#ifndef CLASS_MYLANG_TYPESYSTEM_AST_AFUNCTIONCALL_H
#define CLASS_MYLANG_TYPESYSTEM_AST_AFUNCTIONCALL_H

#include <memory>
#include <vector>

#ifndef CLASS_MYLANG_TYPESYSTEM_AST_AENVIRONMENT_H
#include <mylang/typesystem/ast/AEnvironment.h>
#endif

#ifndef CLASS_MYLANG_TYPESYSTEM_AST_AEXPRESSION_H
#include <mylang/typesystem/ast/AExpression.h>
#endif

#ifndef CLASS_MYLANG_TYPESYSTEM_AST_ATYPE_H
#include <mylang/typesystem/ast/AType.h>
#endif

#ifndef CLASS_MYLANG_TYPESYSTEM_AST_AFUNCTIONSPEC_H
#include <mylang/typesystem/ast/AFunctionSpec.h>
#endif

namespace mylang::typesystem::ast {

  /**
   * The function call is used to a call function by name with some arguments.
   */
  class AFunctionCall: public AExpression
  {
    /** A pointer to an expression */
  public:
    using Ptr = ::std::shared_ptr<const AFunctionCall>;

    /** The type of the expression */
  public:
    using ATypePtr = AType::Ptr;

    /** The arguments arguments to the expression (if any) */
  public:
    using Arguments = ::std::vector<AExpression::Ptr>;

    /**
     * Create a new function call.
     *
     * @param f the spec of the function to invoke.
     * @param retTy the constraints on the return type of the function
     * @param args the arguments to the function
     */
  public:
    AFunctionCall(AFunctionSpec::Ptr f, ATypePtr retTy, Arguments args);

    /** Destructor */
  public:
    ~AFunctionCall();

    /**
     * Create a new function call with an explicit constraint
     * on the result.
     *
     * @param f the spec of the function to invoke.
     * @param retTy the constraints on the return type of the function
     * @param args the arguments to the function
     */
  public:
    static Ptr of(AFunctionSpec::Ptr f, ATypePtr retTy, Arguments args);

    /**
     * Create a new function call.
     *
     * The return type constraint will be taken from the
     * return type of the function spec.
     *
     * @param f the spec of the function to invoke.
     * @param args the arguments to the function
     */
  public:
    static Ptr of(AFunctionSpec::Ptr f, Arguments args);

    /**
     * Create an expression that will invoke the target function.
     *
     * @param env an environment that will be used to look up a definition of the function.
     * @return the underlying expression
     * @throws ::std::runtime_error if the function and the spec are incompatible.
     */
  public:
    ExprPtr toExpr(const AEnvironment &env) const override;

    /** The function to be invoked */
  public:
    const AFunctionSpec::Ptr target;

    /** The arguments to the function. Only useful if function is set */
  public:
    const Arguments arguments;
  };
}
#endif
