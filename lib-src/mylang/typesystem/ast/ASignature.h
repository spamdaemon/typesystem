#ifndef CLASS_MYLANG_TYPESYSTEM_AST_ASIGNATURE_H
#define CLASS_MYLANG_TYPESYSTEM_AST_ASIGNATURE_H

#include <memory>
#include <vector>

#ifndef CLASS_MYLANG_TYPESYSTEM_AST_ATYPE_H
#include <mylang/typesystem/ast/AType.h>
#endif

#ifndef CLASS_MYLANG_TYPESYSTEM_VARNAME_H
#include <mylang/typesystem/VarName.h>
#endif

#ifndef CLASS_MYLANG_TYPESYSTEM_ALGORITHMS_UNIFY_H
#include <mylang/typesystem/algorithms/Unify.h>
#endif

namespace mylang::typesystem::ast {

  /**
   * A general expression of some type.
   */
  class ASignature
  {
    /** A pointer to an expression */
  public:
    using Ptr = ::std::shared_ptr<const ASignature>;

    /** The type of the expression */
  public:
    using ATypePtr = AType::Ptr;

    /** The arguments arguments to the expression (if any) */
  public:
    using Parameters = ::std::vector<ATypePtr>;

    /** The arguments arguments to the expression (if any) */
  public:
    using TypeParameters = ::std::vector<VarName>;

    /**
     * Create a function signature.
     * @param retTy the return type
     * @param params the parameter types
     */
  public:
    ASignature(ATypePtr retTy, TypeParameters typeParams, Parameters params);

    /** Destructor */
  public:
    ~ASignature();

    /**
     * Create a new function with a return type and positional parameters.
     *
     * @param retTy the function's return type
     * @param params the function's positional parameters
     * @param constraintFN a function that produces a constraint expression
     */
  public:
    static Ptr of(ATypePtr retTy, TypeParameters typeParams, Parameters params);

    /**
     * Create a new function with a return type and positional parameters.
     *
     * @param retTy the function's return type
     * @param params the function's positional parameters
     * @param constraintFN a function that produces a constraint expression
     */
  public:
    static Ptr of(ATypePtr retTy, Parameters params);

    /**
     * Test if this signature can be unified with the given one.
     * @param that another signature
     * @return true if the unification is possible, false otherwise
     */
  public:
    bool unifiable(const ASignature &that) const;

    /**
     * Unify two type signatures as much as possible.
     *
     * This function updates an existing unification result with
     * new information.
     *
     * @param sigA a signature
     * @param sigB a signature
     * @param result an existing unification which is updated
     */
  public:
    static void unify(const ASignature &sigA, const ASignature &sigB,
        algorithms::Unify::Result &result);

    /**
     * Freshen the signature
     */
  public:
    ::std::pair<Ptr, Environment> freshen(const Environment &env) const;

    /** The return type of this function */
  public:
    const ATypePtr returnType;

    /** The parameter types */
  public:
    const TypeParameters typeParameters;

    /** The parameter types */
  public:
    const Parameters parameters;
  };
}
#endif
