#include <mylang/typesystem/ast/AProperty.h>
#include <mylang/typesystem/exprs/BLiteral.h>
#include <mylang/typesystem/exprs/VarExpr.h>
#include <mylang/typesystem/types/BType.h>
#include <stdexcept>

namespace mylang::typesystem::ast {

  using namespace mylang::typesystem::exprs;

  AProperty::AProperty(AFunctionSpec::Ptr spec)
      : function(::std::move(spec))
  {
  }

  AProperty::~AProperty()
  {
  }

  AProperty::Ptr AProperty::of(AFunctionName xname, ::std::vector<ATypePtr> params,
      ::std::function<ExprPtr(const Arguments&)> c)
  {
//    auto fn = AFunctionSpec::of(::std::move(xname), AType::of(types::BType::create()),
//        ::std::move(params), [&](VarPtr, const Arguments &args) {
//          return c ? c(args) : BLiteral::of(true);
//        }
//        );
//    return ::std::make_shared<AProperty>(::std::move(fn));
//
    return nullptr;
  }

  AProperty::Ptr AProperty::ofUnary(AFunctionName xname, ATypePtr param,
      ::std::function<ExprPtr(VarPtr)> c)
  {
    return of(::std::move(xname), { param }, [&](const Arguments &args) {
      return c ? c(args.at(0)) : BLiteral::of(true);
    });
  }

  AProperty::Ptr AProperty::ofBinary(AFunctionName xname, ATypePtr param0, ATypePtr param1,
      ::std::function<ExprPtr(VarPtr, VarPtr)> c)
  {
    return of(::std::move(xname), { param0, param1 }, [&](const Arguments &args) {
      return c ? c(args.at(0),args.at(1)) : BLiteral::of(true);
    });
  }
}
