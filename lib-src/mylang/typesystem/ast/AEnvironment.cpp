#include <mylang/typesystem/ast/AEnvironment.h>

namespace mylang::typesystem::ast {

  AEnvironment::~AEnvironment()
  {
  }

  AFunctionSpec::Ptr AEnvironment::lookupFSpec(const AFunctionName &name) const
  {
    auto f = lookupF(name);
    return AFunctionSpec::of(name, f->signature);
  }

}
