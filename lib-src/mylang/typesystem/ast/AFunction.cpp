#include <mylang/typesystem/ast/AFunction.h>
#include <mylang/typesystem/exprs/BLiteral.h>
#include <mylang/typesystem/exprs/BuiltinExpr.h>
#include <mylang/typesystem/exprs/VarExpr.h>
#include <mylang/typesystem/algorithms/Freshen.h>
#include <stdexcept>

namespace mylang::typesystem::ast {

  using namespace mylang::typesystem::exprs;

  namespace {
    inline static VarPtr freshenVar(VarPtr v, Environment &vars)
    {
      auto [res, env] = v->freshen(vars);
      vars = ::std::move(env);
      return res;
    }

    inline static ExprPtr buildSignatureConstraint(const AFunction::ASignaturePtr &signature,
        const VarPtr &result, const AFunction::Arguments &arguments, ExprPtr c)
    {
      if (arguments.size() != signature->parameters.size()) {
        throw ::std::invalid_argument("Signature/arguments mismatch");
      }
      if (result != signature->returnType->pattern.expression) {
        if (result->type->unifiable(signature->returnType->pattern->type) == false) {
          throw ::std::invalid_argument("Return argument and parameter mismatch");
        }
      }

      BuiltinExpr::Operands constraints;
      constraints.push_back(BuiltinExpr::eqOf(result, signature->returnType->pattern.expression));

      for (size_t i = 0; i < arguments.size(); ++i) {
        const auto &arg = arguments.at(i);
        if (arg->type->unifiable(signature->parameters.at(i)->pattern->type) == false) {
          throw ::std::invalid_argument("Argument/parameter mismatch");
        }
        if (arg != signature->parameters.at(i)->pattern.expression) {
          constraints.push_back(
              BuiltinExpr::eqOf(arg, signature->parameters.at(i)->pattern.expression));
        }
      }
      if (c) {
        constraints.push_back(c);
      }
      auto b = BuiltinExpr::allOf(constraints);

      return b;
    }
  }

  AFunction::AFunction(ASignaturePtr sig, VarPtr retVar, Arguments argVar, ExprPtr b)
      : signature(::std::move(sig)), result(::std::move(retVar)), arguments(::std::move(argVar)),
          body(::std::move(b))
  {
    for (auto arg : arguments) {
      assert(arg != nullptr);
    }
  }

  AFunction::~AFunction()
  {
  }

  AFunction::Ptr AFunction::of(ATypePtr retTy, ::std::vector<ATypePtr> params,
      ::std::function<ExprPtr(VarPtr, const Arguments&)> c)
  {
    VarPtr ret;
    Arguments argVars;
    ret = VarExpr::of(retTy->pattern->type);
    argVars.reserve(params.size());
    for (const auto &p : params) {
      argVars.push_back(VarExpr::of(p->pattern->type));
    }
    auto cexpr = c ? c(ret, argVars) : BLiteral::of(true);
    auto sig = ASignature::of(::std::move(retTy), ::std::move(params));
    auto body = buildSignatureConstraint(sig, ret, argVars, cexpr);

    return ::std::make_shared<AFunction>(::std::move(sig), ::std::move(ret), ::std::move(argVars),
        ::std::move(body));
  }

  AFunction::Ptr AFunction::ofUnary(ATypePtr retTy, ATypePtr param,
      ::std::function<ExprPtr(VarPtr, VarPtr)> constraintFN)
  {
    if (constraintFN) {
      return of(::std::move(retTy), { ::std::move(param) }, [&](VarPtr r, const Arguments &args) {
        return constraintFN(r,args.at(0));
      });
    } else {
      return of(::std::move(retTy), { ::std::move(param) }, nullptr);
    }
  }

  AFunction::Ptr AFunction::ofBinary(ATypePtr retTy, ATypePtr param0, ATypePtr param1,
      ::std::function<ExprPtr(VarPtr, VarPtr, VarPtr)> constraintFN)
  {
    if (constraintFN) {
      return of(::std::move(retTy), { ::std::move(param0), ::std::move(param1) },
          [&](VarPtr r, const Arguments &args) {
            return constraintFN(r,args.at(0),args.at(1));
          });
    } else {
      return of(::std::move(retTy), { ::std::move(param0), ::std::move(param1) }, nullptr);
    }
  }
  AFunction::Ptr AFunction::freshen(Environment vars) const
  {
    // freshen the signature first as that will also freshen the type parameters
    auto sig = signature->freshen(vars);

    auto ret = freshenVar(result, vars);
    Arguments args;
    args.reserve(arguments.size());
    for (auto arg : arguments) {
      args.push_back(freshenVar(arg, vars));
    }

    auto b = algorithms::Freshen::apply(body, true, sig.second).actual;

    return ::std::make_shared<AFunction>(::std::move(sig.first), ::std::move(ret),
        ::std::move(args), ::std::move(b.first));
  }

}
