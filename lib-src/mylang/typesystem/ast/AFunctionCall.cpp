#include <mylang/typesystem/ast/AFunctionCall.h>
#include <mylang/typesystem/exprs/VarExpr.h>
#include <mylang/typesystem/exprs/CallExpr.h>
#include <mylang/typesystem/exprs/ConditionalExpr.h>
#include <mylang/typesystem/exprs/LetExpr.h>
#include <mylang/typesystem/types/XType.h>
#include <mylang/typesystem/types/WType.h>
#include <mylang/typesystem/types/SType.h>
#include <mylang/typesystem/algorithms/Unify.h>
#include <stdexcept>
#include <algorithm>
#include <iostream>

namespace mylang::typesystem::ast {

  AFunctionCall::AFunctionCall(AFunctionSpec::Ptr f, ATypePtr retTy, Arguments args)
      : AExpression(::std::move(retTy)), target(::std::move(f->freshen())),
          arguments(::std::move(args))
  {
    for (const auto &arg : arguments) {
      if (arg == nullptr) {
        throw ::std::invalid_argument("argument is nullptr");
      }
    }
    if (arguments.size() != target->signature->parameters.size()) {
      throw ::std::invalid_argument("Bad function arguments");
    }
    if (!target->signature->returnType->pattern->type->unifiable(type->pattern->type)) {
      throw ::std::invalid_argument("Function return type mismatch");
    }
    for (size_t i = 0; i < target->signature->parameters.size(); ++i) {
      if (!arguments.at(i)->type->pattern->type->unifiable(
          target->signature->parameters.at(i)->pattern->type)) {
        throw ::std::invalid_argument("Function argument mismatch");
      }
    }
  }

  AFunctionCall::~AFunctionCall()
  {
  }

  AFunctionCall::Ptr AFunctionCall::of(AFunctionSpec::Ptr f, ATypePtr retTy, Arguments args)
  {
    return ::std::make_shared<AFunctionCall>(::std::move(f), ::std::move(retTy), ::std::move(args));
  }

  AFunctionCall::Ptr AFunctionCall::of(AFunctionSpec::Ptr f, Arguments args)
  {
    return ::std::make_shared<AFunctionCall>(::std::move(f),
        f->signature->returnType->freshen( { }).first, ::std::move(args));
  }

  ExprPtr AFunctionCall::toExpr(const AEnvironment &env) const
  {
    auto fn = env.lookupF(target->name);
    if (fn->signature->typeParameters.size() != target->signature->typeParameters.size()) {
      throw ::std::runtime_error("Function and signature type parameter mismatch");
    }
    if (fn->signature->parameters.size() != target->signature->parameters.size()) {
      throw ::std::runtime_error("Function and signature parameter mismatch");
    }

    Environment xenv(env.getEnvironment());

    // update the bindings to map the function's type parameters to the
    // corresponding ones of the signature.
    for (size_t i = 0; i < target->signature->typeParameters.size(); ++i) {
      xenv.updateBinding(fn->signature->typeParameters.at(i),
          types::WType::of(target->signature->typeParameters.at(i)));
    }
    // freshen the function given the new environment with the remapped type
    // names.
    auto function = fn->freshen(xenv);

    Function::TypeParameters typeParams;

    Function::Parameters params;
    params.reserve(function->arguments.size() + 1);
    params.push_back(function->result);
    params.insert(params.end(), function->arguments.begin(), function->arguments.end());
    auto gn = Function::of(::std::move(typeParams), ::std::move(params), function->body);

    auto res = type->pattern.expression;
    exprs::CallExpr::Arguments args;
    args.reserve(arguments.size());
    args.push_back(res);
    for (auto arg : arguments) {
      args.push_back(arg->toExpr(env));
    }
    auto call = exprs::CallExpr::of(gn, ::std::move(args));
    return exprs::ConditionalExpr::guardOf(call, res);
  }

}
