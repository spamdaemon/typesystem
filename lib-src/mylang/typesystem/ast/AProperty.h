#ifndef CLASS_MYLANG_TYPESYSTEM_AST_APROPERTY_H
#define CLASS_MYLANG_TYPESYSTEM_AST_APROPERTY_H

#include <memory>
#include <vector>
#include <utility>

#ifndef CLASS_MYLANG_TYPESYSTEM_AST_ATYPE_H
#include <mylang/typesystem/ast/AType.h>
#endif

#ifndef CLASS_MYLANG_TYPESYSTEM_AST_AFUNCTIONSPEC_H
#include <mylang/typesystem/ast/AFunctionSpec.h>
#endif

#ifndef FILE_MYLANG_TYPESYSTEM_H
#include <mylang/typesystem/typesystem.h>
#endif

#ifndef CLASS_MYLANG_TYPESYSTEM_AST_AFUNCTIONNAME_H
#include <mylang/typesystem/ast/AFunctionName.h>
#endif

namespace mylang::typesystem::ast {

  /**
   * A general expression of some type.
   */
  class AProperty
  {
    /** A pointer to an expression */
  public:
    using Ptr = ::std::shared_ptr<const AProperty>;

    /** The type of the expression */
  public:
    using ATypePtr = AType::Ptr;

    /** The type of the expression */
  public:
    using ASignaturePtr = ASignature::Ptr;

    /** The arguments arguments to the expression (if any) */
  public:
    using Parameters = ::std::vector<ATypePtr>;

    /** The arguments arguments to the expression (if any) */
  public:
    using Arguments = ::std::vector<VarPtr>;

    /**
     * Create a function specification
     * @param spec a function spec
     */
  public:
    AProperty(AFunctionSpec::Ptr spec);

    /** Destructor */
  public:
    ~AProperty();

    /**
     * Create a new function with a return type and positional parameters.
     *
     * @param params the function's positional parameters
     * @param constraintFN a function that produces a constraint expression
     */
  public:
    static Ptr of(AFunctionName name, Parameters params,
        ::std::function<ExprPtr(const Arguments&)> constraintFN = nullptr);

    /**
     * Create a new function with a return type and positional parameters.
     *
     * @param param the function single parameter
     * @param constraintFN a function that produces a constraint expression
     */
  public:
    static Ptr ofUnary(AFunctionName name, ATypePtr param,
        ::std::function<ExprPtr(VarPtr)> constraintFN = nullptr);

    /**
     * Create a new function with a return type and positional parameters.
     *
     * @param param0 the function's first positional parameters
     * @param param1 the function's second positional parameters
     * @param constraintFN a function that produces a constraint expression
     */
  public:
    static Ptr ofBinary(AFunctionName name, ATypePtr param0, ATypePtr param1,
        ::std::function<ExprPtr(VarPtr, VarPtr)> constraintFN = nullptr);

    /** The function spec that is the property */
  public:
    const AFunctionSpec::Ptr function;
  };
}
#endif
