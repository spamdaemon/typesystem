#ifndef CLASS_MYLANG_TYPESYSTEM_AST_ATYPE_H
#define CLASS_MYLANG_TYPESYSTEM_AST_ATYPE_H

#include <memory>
#include <vector>

#ifndef CLASS_MYLANG_TYPESYSTEM_H
#include <mylang/typesystem/typesystem.h>
#endif

#ifndef CLASS_MYLANG_PATTERN_H
#include <mylang/typesystem/Pattern.h>
#endif

#ifndef CLASS_MYLANG_TYPESYSTEM_ENVIRONMENT_H
#include <mylang/typesystem/Environment.h>
#endif

#ifndef CLASS_MYLANG_TYPESYSTEM_ALGORITHMS_UNIFY_H
#include <mylang/typesystem/algorithms/Unify.h>
#endif

namespace mylang::typesystem::ast {

  /**
   * The Type class is a wrapper for a type expression.
   */
  class AType
  {
    /** The pointer to a type */
  public:
    using Ptr = ::std::shared_ptr<const AType>;

    /**
     * Create a type using the specified type expression
     * @param e an expression
     * @throws ::std::invalid_argument if the expression does not produce a type
     */
  public:
    explicit AType(Pattern e);

    /** Destructor */
  public:
    ~AType();

    /**
     * Create a type expression from a pattern.
     * @param pattern a pattern
     * @return a type
     */
  public:
    static Ptr of(Pattern pattern);

    /**
     * Create a new type instance using a type expression.
     *
     * @param ty a type expression
     * @return a type
     */
  public:
    static Ptr of(TypeDefinitionPtr ty);

    /**
     * Create a new type instance using a type expression which has to be a pattern.
     *
     * @param ty a type expression
     * @return a type
     * @throws ::std::invalid_argument if the expression is not a pattern
     */
  public:
    static Ptr ofPattern(ExprPtr ty);

    /**
     * Create a new type instance using a type.
     *
     * @param ty a type
     * @return a type
     */
  public:
    static Ptr of(TypePtr ty);

    /**
     * Freshen the pattern.
     */
  public:
    ::std::pair<Ptr, Environment> freshen(const Environment &env) const;

    /**
     * Test if this type is unfiable with the given one.
     * @param that another type
     * @return true if the unification is possible, false otherwise
     */
  public:
    bool unifiable(const AType &that) const;

    /**
     * Unify two types as much as possible.
     *
     * This function updates an existing unification result with
     * new information.
     *
     * @param typeA a signature
     * @param typeB a signature
     * @param result an existing unification which is updated
     */
  public:
    static void unify(const AType &typeA, const AType &typeB, algorithms::Unify::Result &result);

    /** The expression that evaluates to a type */
  public:
    const Pattern pattern;
  };
}
#endif
