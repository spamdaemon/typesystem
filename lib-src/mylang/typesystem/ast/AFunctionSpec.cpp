#include <mylang/typesystem/ast/AFunctionSpec.h>

namespace mylang::typesystem::ast {

  AFunctionSpec::AFunctionSpec(AFunctionName xname, ASignature::Ptr sig)
      : name(::std::move(xname)), signature(::std::move(sig))
  {
  }

  AFunctionSpec::~AFunctionSpec()
  {
  }

  AFunctionSpec::Ptr AFunctionSpec::of(AFunctionName xname, ASignature::Ptr sig)
  {
    return ::std::make_shared<AFunctionSpec>(::std::move(xname), ::std::move(sig));
  }

  AFunctionSpec::Ptr AFunctionSpec::freshen() const
  {
    return of(name, signature->freshen( { }).first);
  }

}
