#include <mylang/typesystem/ast/ATypeChecker.h>
#include <mylang/typesystem/ast/AFunctionCall.h>
#include <mylang/typesystem/ast/AValue.h>
#include <mylang/typesystem/algorithms/Unify.h>
#include <mylang/typesystem/visitors/Printer.h>
#include <mylang/typesystem/exprs/Expr.h>
#include <stdexcept>
#include <iostream>

namespace mylang::typesystem::ast {
  using ::mylang::typesystem::algorithms::Unify;

  namespace {

    static void printBindings(::std::ostream &out, const Unify::Bindings &env)
    {
      out << "-----------------------------" << ::std::endl;
      {
        out << "Binds (" << env.vars.size() << ")" << ::std::endl;
        for (const auto &nb : env.vars.elements()) {
          out << "  " << nb->first.toString() << " = ";
          visitors::Printer::print(out, nb->second);
        }
        out << ::std::endl;
      }
      {
        out << "TypeBinds (" << env.typeVars.size() << ")";
        for (const auto &nb : env.typeVars.elements()) {
          out << "  " << nb->first.toString() << " = " << typeid(*(nb->second)).name()
              << ::std::endl;
        }
        out << ::std::endl;
      }
    }

    static AType::Ptr doTypeCheck(const AExpression::Ptr &e, Unify::Result &result);

    static AType::Ptr doTypeCheckFunction(const AFunctionCall::Ptr &fn, Unify::Result &result)
    {
      auto sig = fn->target->signature;

      // unify the return types first
      AType::unify(*sig->returnType, *fn->type, result);
      if (result.second == Unify::FAILED) {
        throw ::std::runtime_error(
            "Unification failed for return type of function " + fn->target->name.toString());
      }
      // unify each argument with the parameter
      for (size_t i = 0; i < sig->parameters.size(); ++i) {
        const auto &arg = fn->arguments.at(i);
        const auto &param = sig->parameters.at(i);
        // recursively do a typecheck
        auto argTy = doTypeCheck(arg, result);
        AType::unify(*param, *argTy, result);

        if (result.second == Unify::FAILED) {
          throw ::std::runtime_error(
              "Unification failed for function parameter " + ::std::to_string(i) + " of function "
                  + fn->target->name.toString());
        }
      }
      return fn->type;
    }

    static AType::Ptr doTypeCheckValue(const AValue::Ptr &e, Unify::Result&)
    {
      return e->type;
    }

    static AType::Ptr doTypeCheck(const AExpression::Ptr &e, Unify::Result &result)
    {
      // check the function call arguments
      if (auto fn = e->self<AFunctionCall>(); fn) {
        return doTypeCheckFunction(fn, result);
      }

      return doTypeCheckValue(e->self<AValue>(), result);
    }
  }

  ATypeChecker::TypeError::TypeError(::std::string msg)
      : ::std::runtime_error(::std::move(msg))
  {
  }

  ATypeChecker::TypeError::~TypeError()
  {
  }

  ATypeChecker::ATypeChecker()
  {
  }

  ATypeChecker::~ATypeChecker()
  {
  }

  AEnvironment::Ptr ATypeChecker::typecheck(const AEnvironment::Ptr &aenv,
      const AExpression::Ptr &e)
  {
    Environment env = aenv->getEnvironment();
    Unify::Result res { { env.bindings, env.typeBindings }, Unify::BINDINGS };
    try {
      while (res.second == Unify::BINDINGS) {
        res.second = Unify::UNIFIED;
        doTypeCheck(e, res);
      }
    } catch (const ::std::exception &ex) {
      throw TypeError(ex.what());
    }

    if (res.second == Unify::FAILED) {
      throw TypeError("Unification failed");
    }
    env.bindings = res.first.vars;
    env.typeBindings = res.first.typeVars;
    return aenv->replaceEnvironment(env);
  }

}
