#include <mylang/typesystem/ast/AExpression.h>

namespace mylang::typesystem::ast {

  AExpression::AExpression(ATypePtr expected)
      : type(::std::move(expected))
  {
  }

  AExpression::~AExpression()
  {
  }

}
