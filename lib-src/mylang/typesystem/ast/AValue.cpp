#include <mylang/typesystem/ast/AValue.h>
#include <mylang/typesystem/exprs/VarExpr.h>
#include <mylang/typesystem/Pattern.h>
#include <stdexcept>
#include <algorithm>
#include <iostream>

namespace mylang::typesystem::ast {

  AValue::AValue(ATypePtr expected)
      : AExpression(::std::move(expected))
  {
  }

  AValue::~AValue()
  {
  }

  AValue::Ptr AValue::of(TypePtr expected)
  {
    return of(exprs::VarExpr::of(expected));
  }

  AValue::Ptr AValue::of(ExprPtr expected)
  {
    return of(Pattern(expected));
  }

  AValue::Ptr AValue::of(Pattern expected)
  {
    return of(AType::of(::std::move(expected)));
  }

  AValue::Ptr AValue::of(ATypePtr expected)
  {
    return ::std::make_shared<AValue>(::std::move(expected));
  }

  ExprPtr AValue::toExpr(const AEnvironment&) const
  {
    return type->pattern.expression;
  }

}
