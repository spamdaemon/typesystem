#include <mylang/typesystem/ast/AType.h>
#include <mylang/typesystem/ast/AEnvironment.h>
#include <mylang/typesystem/TypeDefinition.h>
#include <mylang/typesystem/exprs/VarExpr.h>
#include <mylang/typesystem/algorithms/Freshen.h>
#include <mylang/typesystem/eval/Eval.h>
#include <iostream>
namespace mylang::typesystem::ast {

  AType::AType(Pattern t)
      : pattern(::std::move(t))
  {
  }

  AType::~AType()
  {
  }

  AType::Ptr AType::of(Pattern p)
  {
    return ::std::make_shared<AType>(::std::move(p));
  }

  AType::Ptr AType::ofPattern(ExprPtr t)
  {
    return of(Pattern(t));
  }

  AType::Ptr AType::of(TypeDefinitionPtr def)
  {
    return of(def->getConstructorPattern());
  }

  AType::Ptr AType::of(TypePtr ty)
  {
    return ofPattern(exprs::VarExpr::of(ty));
  }

  ::std::pair<AType::Ptr, Environment> AType::freshen(const Environment &env) const
  {
    auto res = algorithms::Freshen::apply(pattern.expression, true, env);
    if (res.isNew) {
      return {of(Pattern(res.actual.first)),res.actual.second};
    } else {
      return {of(pattern),env};
    }
  }

  bool AType::unifiable(const AType &that) const
  {
    algorithms::Unify::Result tmp;
    tmp.second = algorithms::Unify::BINDINGS;
    unify(*this, that, tmp);
    return tmp.second != algorithms::Unify::FAILED;
  }

  void AType::unify(const AType &left, const AType &right, algorithms::Unify::Result &result)
  {
    algorithms::Unify::Result res = algorithms::Unify::apply(left.pattern, right.pattern,
        result.first);
    algorithms::Unify::updateResult(result, res);
  }

}
