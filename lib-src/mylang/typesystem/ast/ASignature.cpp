#include <mylang/typesystem/ast/ASignature.h>
#include <mylang/typesystem/FreeVariables.h>
#include <mylang/typesystem/algorithms/Unify.h>
#include <mylang/typesystem/types/WType.h>
#include <stdexcept>
#include <iostream>

namespace mylang::typesystem::ast {

  ASignature::ASignature(ATypePtr retTy, TypeParameters typeParams, Parameters params)
      : returnType(::std::move(retTy)), typeParameters(typeParams), parameters(::std::move(params))
  {
    if (!returnType) {
      throw ::std::invalid_argument("nullptr");
    }
    for (const auto &t : parameters) {
      if (!t) {
        throw ::std::invalid_argument("nullptr");
      }
    }
  }

  ASignature::~ASignature()
  {
  }

  ASignature::Ptr ASignature::of(ATypePtr retTy, TypeParameters typeParams, Parameters params)
  {
    return ::std::make_shared<ASignature>(::std::move(retTy), ::std::move(typeParams),
        ::std::move(params));
  }

  ASignature::Ptr ASignature::of(ATypePtr retTy, Parameters params)
  {
    FreeVariables freeVars;
    for (const auto &p : params) {
      freeVars.add(p->pattern.freeVariables);
    }
    ::std::vector<VarName> freeTypes(freeVars.freeTypes.begin(), freeVars.freeTypes.end());
    return of(::std::move(retTy), ::std::move(freeTypes), ::std::move(params));
  }

  ::std::pair<ASignature::Ptr, Environment> ASignature::freshen(const Environment &env) const
  {
    // freshen the variable first and create new type names for them
    Environment typeEnv(env);
    for (const auto &v : typeParameters) {
      typeEnv.typeBindings.insert(v, types::WType::create());
    }

    auto [retTy, vars] = returnType->freshen(typeEnv);

    Parameters params;
    params.reserve(parameters.size());
    for (const auto &p : parameters) {
      auto pp = p->freshen(vars);
      params.push_back(pp.first);
      vars = ::std::move(pp.second);
    }
    return {of(retTy, ::std::move(params)),vars};
  }

  bool ASignature::unifiable(const ASignature &that) const
  {
    algorithms::Unify::Result tmp;
    tmp.second = algorithms::Unify::BINDINGS;
    unify(*this, that, tmp);
    return tmp.second != algorithms::Unify::FAILED;
  }

  void ASignature::unify(const ASignature &sigA, const ASignature &sigB,
      algorithms::Unify::Result &result)
  {
    if (result.second == algorithms::Unify::FAILED) {
      return;
    }
    if (sigA.typeParameters.size() != sigB.typeParameters.size()) {
      result.second = algorithms::Unify::FAILED;
      return;
    }
    if (sigA.parameters.size() != sigB.parameters.size()) {
      result.second = algorithms::Unify::FAILED;
      return;
    }

    // unify the return types first
    AType::unify(*sigA.returnType, *sigB.returnType, result);
    // unify each argument with the parameter
    for (size_t i = 0; result.second != algorithms::Unify::FAILED && i < sigA.parameters.size();
        ++i) {
      const auto &pA = sigA.parameters.at(i);
      const auto &pB = sigB.parameters.at(i);
      AType::unify(*pA, *pB, result);
    }
  }

}
