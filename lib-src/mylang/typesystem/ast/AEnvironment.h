#ifndef CLASS_MYLANG_TYPESYSTEM_AST_AENVIRONMENT_H
#define CLASS_MYLANG_TYPESYSTEM_AST_AENVIRONMENT_H

#include <mylang/typesystem/ast/AFunctionName.h>
#include <mylang/typesystem/ast/AFunction.h>
#include <mylang/typesystem/ast/AFunctionSpec.h>
#include <mylang/typesystem/ast/AProperty.h>
#include <mylang/typesystem/Environment.h>
#include <mylang/typesystem/Pattern.h>

#include <mylang/Self.h>
#include <vector>

namespace mylang::typesystem::ast {

  /**
   * An environment that contains functions and expressions.
   */
  class AEnvironment: public Self<AEnvironment>
  {
  public:
    using Ptr = ::std::shared_ptr<AEnvironment>;

    /** Default constructor */
  public:
    AEnvironment() = default;

    /** Destructor */
  public:
    virtual ~AEnvironment() = 0;

    /**
     * Lookup a function in the environment.
     *
     * @param name the name of a function
     * @return a function pointer
     * @throws std::out_of_range if there is no such function
     */
  public:
    virtual AFunction::Ptr lookupF(const AFunctionName &name) const = 0;

    /**
     * Lookup a function in the environment and get a spec for it.
     *
     * @param name the name of a function
     * @return a function spec pointer
     * @throws std::out_of_range if there is no such function
     */
  public:
    AFunctionSpec::Ptr lookupFSpec(const AFunctionName &name) const;

    /**
     * Lookup a property in the environment.
     *
     * @param name the name a of a property
     * @param selector a selector that works uses names of type constructors to select the property function
     */
  public:
    virtual AProperty::Ptr lookupP(const AFunctionName &name,
        const ::std::vector<Pattern> &names) const = 0;

    /**
     * Get the current type environment suitable for evaluation
     * @return an environment
     */
  public:
    virtual Environment getEnvironment() const = 0;

    /**
     * Replace the environment with a new one.
     * @param newEnv a new environment
     * @return a new environment
     */
  public:
    virtual Ptr replaceEnvironment(Environment env) const = 0;
  };
}
#endif
