#include <mylang/typesystem/TypeMismatch.h>

namespace mylang::typesystem {

  TypeMismatch::TypeMismatch(TypePtr e, TypePtr a)
      : ::std::runtime_error("Type mismatch"), expected(::std::move(e)), actual(::std::move(a))
  {
  }

  TypeMismatch::~TypeMismatch()
  {
  }

}
