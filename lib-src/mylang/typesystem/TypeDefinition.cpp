#include <mylang/typesystem/TypeDefinition.h>
#include <mylang/typesystem/exprs/TypeExpr.h>
#include <mylang/typesystem/exprs/ExprTransform.h>
#include <mylang/typesystem/types/BType.h>
#include <mylang/typesystem/Environment.h>
#include <mylang/typesystem/FreeVariables.h>
#include <mylang/typesystem/eval/Eval.h>
#include <mylang/typesystem/typesystem.h>
#include <mylang/typesystem/Constraints.h>
#include <mylang/typesystem/exprs/VarExpr.h>
#include <mylang/typesystem/FreeVariables.h>
#include <mylang/typesystem/VarName.h>
#include <stdexcept>
#include <algorithm>
#include <memory>
#include <optional>
#include <map>
#include <string>

namespace mylang::typesystem {
  using namespace exprs;

  TypeDefinition::TypeDefinition(::std::string defName, ConstructorPtr f, ExprPtr c)
      : constructor(::std::move(f)), name(::std::move(defName)), constraints(::std::move(c))
  {
    if (constraints->type->unifiable(types::BType::create()) == false) {
      throw ::std::invalid_argument("Constraint must be a boolean constraint");
    }
    if (auto err = constructor->checkFreeVariables(constraints); err) {
      throw ::std::invalid_argument(err.value());
    }
  }

  TypeDefinition::~TypeDefinition()
  {
  }

  ::std::shared_ptr<const TypeDefinition> TypeDefinition::of(::std::string defName,
      ConstructorPtr c, ExprPtr constraintExpr)
  {
    if (constraintExpr == nullptr) {
      constraintExpr = BLiteral::of(true);
    }
    return ::std::make_shared<TypeDefinition>(::std::move(defName), ::std::move(c),
        ::std::move(constraintExpr));
  }

  ::std::shared_ptr<const TypeDefinition> TypeDefinition::of(::std::string defName,
      ::std::string typeName, Function::TypeParameters t, Function::Parameters p,
      ExprPtr constraintExpr)
  {
    return of(::std::move(defName), Constructor::of(typeName, t, p), ::std::move(constraintExpr));
  }

  ::std::shared_ptr<const TypeDefinition> TypeDefinition::of(::std::string xname,
      Function::Parameters p, ExprPtr constraintExpr)
  {
    return of(xname, xname, p, constraintExpr);
  }

  ::std::shared_ptr<const TypeDefinition> TypeDefinition::of(::std::string defName,
      ::std::string typeName, Function::Parameters p, ExprPtr constraintExpr)
  {
    return of(::std::move(defName), Constructor::of(typeName, p), ::std::move(constraintExpr));
  }

  ::std::shared_ptr<const TypeDefinition> TypeDefinition::of(::std::string xname,
      Function::TypeParameters t, Function::Parameters p, ExprPtr constraintExpr)
  {
    return of(xname, xname, t, p, constraintExpr);
  }

  ::std::shared_ptr<const TypeDefinition> TypeDefinition::refine(::std::string xname,
      ExprPtr constraintExpr) const
  {
    return ::std::make_shared<TypeDefinition>(::std::move(xname), constructor,
        BuiltinExpr::andOf(constraints, constraintExpr));
  }

  ExprPtr TypeDefinition::callConstructor(Function::Arguments args) const
  {
    // fill in additional parameters with variables
    args.reserve(constructor->parameters.size());
    for (size_t i = args.size(); i < constructor->parameters.size(); ++i) {
      args.push_back(VarExpr::of(constructor->parameters.at(i)->type));
    }

    if (auto err = constructor->checkArguments(args); err) {
      throw ::std::invalid_argument(err.value());
    }

    return constructor->getBody()->replaceArguments(::std::move(args));
  }

  Pattern TypeDefinition::getConstructorPattern(::std::vector<Pattern> args) const
  {
    args.reserve(constructor->parameters.size());
    for (size_t i = args.size(); i < constructor->parameters.size(); ++i) {
      args.push_back(Pattern(VarExpr::of(constructor->parameters.at(i)->type)));
    }
    return Pattern(constructor->getCallPattern(::std::move(args)));
  }

}
