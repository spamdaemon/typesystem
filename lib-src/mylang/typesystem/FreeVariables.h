#ifndef CLASS_MYLANG_TYPESYSTEM_FREEVARIABLES_H
#define CLASS_MYLANG_TYPESYSTEM_FREEVARIABLES_H

#include <mylang/typesystem/typesystem.h>
#include <mylang/typesystem/VarName.h>
#include <map>
#include <set>
#include <vector>

namespace mylang::typesystem {

  /**
   * The free variables in expression or type.
   **/
  class FreeVariables
  {

  public:
    FreeVariables() = default;

    /** Default move constructor */
  public:
    FreeVariables(FreeVariables &&that) = default;

    /** Default move constructor */
  public:
    FreeVariables(const FreeVariables &that) = default;

    /**
     * Get the free variables in an expression.
     * @param e an expression
     */
  public:
    FreeVariables(const ExprPtr &e);

    /**
     * Get the free variables in an expression.
     * @param e a list of expressions
     */
  public:
    FreeVariables(const ::std::vector<ExprPtr> &e);

    /**
     * Get the free variables in a set of expressions
     * @param i an iterator
     * @param e an end iterator
     */
  public:
    template<typename bIter, typename eIter>
    FreeVariables(bIter i, eIter e)
    {
      for (; i != e; ++i) {
        add(FreeVariables(*i));
      }
    }

  public:
    ~FreeVariables() = default;

    /**
     * Add a free variables.
     * @param e an var expr
     */
  public:
    void unbound(const VarPtr &e);

    /**
     * Add a wildcard type.
     * @param t a wildcard type
     */
  public:
    void unbound(const WTypePtr &t);

    /**
     * Add a set of free variables into this set
     * @param v free variables
     * @return true if any variables were added
     */
  public:
    bool add(const FreeVariables &v);

    /** The unbound variables and their types */
  public:
    ::std::map<VarName, TypePtr> freeVariables;

    /** The type variables */
  public:
    ::std::set<VarName> freeTypes;
  };
}
#endif
