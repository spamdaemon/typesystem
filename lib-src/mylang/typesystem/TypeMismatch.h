#ifndef CLASS_MYLANG_TYPESYSTEM_TYPEMISMATCH_H
#define CLASS_MYLANG_TYPESYSTEM_TYPEMISMATCH_H

#include <stdexcept>
#include <mylang/typesystem/typesystem.h>

namespace mylang::typesystem {

  /**
   * The base type for all expressions used in the typesystem.
   */
  class TypeMismatch: public ::std::runtime_error
  {
    /** Constructor */
  public:
    TypeMismatch(TypePtr expected, TypePtr actual);

    /** Destructor */
  public:
    ~TypeMismatch();

    /** The expected type */
  public:
    const TypePtr expected;

    /** The actual type */
  public:
    const TypePtr actual;
  };
}
#endif
