#ifndef CLASS_MYLANG_TYPESYSTEM_VARNAME_H
#define CLASS_MYLANG_TYPESYSTEM_VARNAME_H
#include <memory>
#include <string>
#include <ostream>

namespace mylang {
  namespace typesystem {

    /**
     * A class to represent a variable name.
     */
    class VarName
    {
      /** A dummy class */
    public:
      struct Impl
      {
        inline Impl(::std::string p)
            : prefix(::std::move(p))
        {
        }
        const ::std::string prefix;
      };

      /**
       * Create a fresh variable name.
       */
    public:
      VarName();

      /**
       * Create a fresh variable name.
       * @param str a prefix string
       */
    public:
      VarName(::std::string str);

      /**
       * Create a fresh variable name.
       * @param cstr a prefix string
       */
    public:
      VarName(const char *cstr);

      /** Destructor */
    public:
      ~VarName();

      /**
       * Create a fresh variable name.
       * @param str a prefix string
       */
    public:
      inline static VarName of(::std::string str)
      {
        return VarName(::std::move(str));
      }

      /**
       * Create a fresh variable name.
       * @param cstr a prefix string
       */
    public:
      inline static VarName of(const char *cstr)
      {
        return VarName(cstr);
      }

      /**
       * Compare name for equality.
       * @param other another name
       * @return true if the two names are the same
       */
    public:
      inline bool operator==(const VarName &other) const
      {
        return name == other.name;
      }

      /**
       * Compare name for equality.
       * @param other another name
       * @return true if the two names are the same
       */
    public:
      inline bool operator!=(const VarName &other) const
      {
        return name != other.name;
      }

      /**
       * Compare name for equality.
       * @param other another name
       * @return true if the two names are the same
       */
    public:
      inline bool operator<(const VarName &other) const
      {
        return name < other.name;
      }

      /**
       * Compare name for equality.
       * @param other another name
       * @return true if the two names are the same
       */
    public:
      inline friend ::std::ostream& operator<<(::std::ostream &out, const VarName &v)
      {
        return out << v.toString();
      }

      /** Freshen this name */
    public:
      VarName freshen() const;

      /**
       * Create a string for this name.
       * @return a string name
       */
    public:
      ::std::string toString() const;

      /** The name of the variable; since we're incorporating
       * the pointer into the name, we're getting a unique name */
    private:
      ::std::shared_ptr<Impl> name;
    };
  }
}
#endif
