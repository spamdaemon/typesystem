#ifndef CLASS_MYLANG_TYPESYSTEM_PREDICATES_ISCONSTANT_H
#define CLASS_MYLANG_TYPESYSTEM_PREDICATES_ISCONSTANT_H

#include <mylang/typesystem/typesystem.h>

namespace mylang::typesystem::predicates {

  /**
   * Predicate to determine if the specified expression is a constant expression.
   *
   * A constant expression is either a literal, or some kind of constructor
   * that whose arguments satisfy this predicate.
   */
  struct IsConstant
  {
    static bool test(ExprPtr e);
  };

}

#endif
