#ifndef CLASS_MYLANG_TYPESYSTEM_PREDICATES_HASFREEVARIABLES_H
#define CLASS_MYLANG_TYPESYSTEM_PREDICATES_HASFREEVARIABLES_H

#include <mylang/typesystem/typesystem.h>
#include <mylang/typesystem/Environment.h>

namespace mylang::typesystem::predicates {

  /**
   * This predicate determines if an expression contains variables that are
   * not in the specified bindings.
   */
  struct HasFreeVariables
  {
    static bool test(ExprPtr e, const Environment::Bindings &bindings);
  };

}

#endif
