#include <mylang/typesystem/predicates/IsPattern.h>
#include <mylang/typesystem/exprs/TransformVisitor.h>
#include <mylang/typesystem/exprs/BLiteral.h>
#include <mylang/typesystem/exprs/ConstructExpr.h>
#include <mylang/typesystem/exprs/ErrorExpr.h>
#include <mylang/typesystem/exprs/ILiteral.h>
#include <mylang/typesystem/exprs/ListLiteral.h>
#include <mylang/typesystem/exprs/TLiteral.h>
#include <mylang/typesystem/exprs/TypeExpr.h>
#include <mylang/typesystem/typesystem.h>
#include <memory>

namespace mylang::typesystem::predicates {
  using namespace exprs;

  bool IsPattern::test(ExprPtr e)
  {
    struct V: public TransformVisitor<bool>
    {
      V()
      {
      }

      ~V()
      {
      }

      bool visitDefault(const ExprPtr&) override
      {
        return false;
      }

      bool visit(const ::std::shared_ptr<const VarExpr>&) override
      {
        return true;
      }

      bool visit(const ::std::shared_ptr<const BLiteral>&) override
      {
        return true;
      }
      bool visit(const ::std::shared_ptr<const ILiteral>&) override
      {
        return true;
      }
      bool visit(const ::std::shared_ptr<const TLiteral>&) override
      {
        return true;
      }
      bool visit(const ::std::shared_ptr<const ConstructExpr> &e) override
      {
        for (const auto &arg : e->arguments) {
          if (!transform(arg)) {
            return false;
          }
        }
        return true;
      }

      bool visit(const ::std::shared_ptr<const LetExpr> &e) override
      {
        return transform(e->result) && transform(e->value);
      }

      bool visit(const ::std::shared_ptr<const ListLiteral> &e) override
      {
        for (const auto &elem : e->entries) {
          if (!transform(elem)) {
            return false;
          }
        }
        return true;
      }

      bool visit(const ::std::shared_ptr<const TypeExpr> &e) override
      {
        return transform(e->name) && transform(e->argument);
      }

    };

    return V().transform(e);
  }

}
