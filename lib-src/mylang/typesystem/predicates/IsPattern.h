#ifndef CLASS_MYLANG_TYPESYSTEM_PREDICATES_ISPATTERN_H
#define CLASS_MYLANG_TYPESYSTEM_PREDICATES_ISPATTERN_H

#include <mylang/typesystem/typesystem.h>

namespace mylang::typesystem::predicates {

  /**
   * Predicate to determine if the specified expression constitutes a pattern.
   */
  struct IsPattern
  {
    static bool test(ExprPtr e);
  };

}

#endif
