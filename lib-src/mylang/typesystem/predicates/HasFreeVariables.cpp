#include <mylang/typesystem/predicates/HasFreeVariables.h>
#include <mylang/typesystem/exprs/ExprTransform.h>
#include <mylang/typesystem/exprs/exprs.h>
#include <mylang/typesystem/Environment.h>
#include <mylang/typesystem/typesystem.h>
#include <memory>

namespace mylang::typesystem::predicates {
  using namespace exprs;

  bool HasFreeVariables::test(ExprPtr e, const Environment::Bindings &bindings)
  {
    struct V: public ExprTransform
    {
      V(const Environment::Bindings &xbindings)
          : bindings(xbindings), hasFreeVars(false)
      {
      }

      ~V()
      {
      }

      Tx<ExprPtr> visit(const ::std::shared_ptr<const LetExpr> &e) override
      {
        if (!hasFreeVars) {
          transform(e->value);
          if (!hasFreeVars) {
            V v(bindings.insert(e->variable, e->value));
            v.transform(e->result);
            hasFreeVars = v.hasFreeVars;
          }
        }
        return visitDefault(e);
      }

      Tx<ExprPtr> visit(const ::std::shared_ptr<const VarExpr> &e) override
      {
        hasFreeVars = hasFreeVars || bindings.find(e->name) == nullptr;
        return visitDefault(e);
      }
      const Environment::Bindings &bindings;
      bool hasFreeVars;
    };

    V v(bindings);
    v.transform(e);
    return v.hasFreeVars;
  }

}
