#ifndef CLASS_MYLANG_TYPESYSTEM_EXPRS_CONDITIONALEXPR_H
#define CLASS_MYLANG_TYPESYSTEM_EXPRS_CONDITIONALEXPR_H

#include <mylang/typesystem/exprs/Expr.h>
#include <mylang/typesystem/typesystem.h>

namespace mylang::typesystem::exprs {

  /**
   * An expression that references a type.
   */
  class ConditionalExpr: public Expr
  {

    /**
     * Constructor
     * @param c the condition
     * @param t the expression to evaluate if the condition is true
     * @param f the expression to evaluate if the condition is false
     *
     * @throws IllegalArgument c is a not a boolean expression
     */
  public:
    ConditionalExpr(ExprPtr c, ExprPtr t, ExprPtr f);

    /** Destructor */
  public:
    ~ConditionalExpr();

    /**
     * Create a condition expression.
     *
     * @param c the condition
     * @param t the expression to evaluate if the condition is true
     * @param f the expression to evaluate if the condition is false
     * @return a condition expression
     * @throws IllegalArgument c is a not a boolean expression
     */
  public:
    static ::std::shared_ptr<const ConditionalExpr> of(ExprPtr c, ExprPtr t, ExprPtr f);

    /**
     * Create a guarding condition expression.
     *
     * If the condition is false, then an error is returned
     *
     * @param c the condition
     * @param t the expression to evaluate if the condition is true
     * @return a condition expression
     * @throws IllegalArgument c is a not a boolean expression
     */
  public:
    static ::std::shared_ptr<const ConditionalExpr> guardOf(ExprPtr c, ExprPtr t);

  public:
    void accept(ExprVisitor &v) const override;

    /** The condition */
  public:
    const ExprPtr condition;

    /** The expression that produces the value if the condition is true */
  public:
    const ExprPtr iftrue;

    /** The expression that produces the value if the condition is false */
  public:
    const ExprPtr iffalse;
  };
}
#endif
