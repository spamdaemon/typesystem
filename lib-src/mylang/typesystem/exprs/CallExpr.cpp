#include <mylang/typesystem/exprs/CallExpr.h>
#include <mylang/typesystem/exprs/ExprVisitor.h>
#include <mylang/typesystem/TypeMismatch.h>
#include <stdexcept>
#include <mylang/Self.h>
#include <mylang/typesystem/typesystem.h>
#include <algorithm>
#include <memory>

namespace mylang::typesystem::exprs {

  CallExpr::CallExpr(FunctionPtr f, Arguments args)
      : Expr(f->body->type), target(::std::move(f)), arguments(::std::move(args))
  {
  }

  CallExpr::~CallExpr()
  {
  }

  ::std::shared_ptr<const CallExpr> CallExpr::of(FunctionPtr f, Arguments args)
  {
    return ::std::make_shared<CallExpr>(::std::move(f), ::std::move(args));
  }

  void CallExpr::accept(ExprVisitor &v) const
  {
    v.visit(self<CallExpr>());
  }

}
