#ifndef CLASS_MYLANG_TYPESYSTEM_EXPRS_TYPEEXPR_H
#define CLASS_MYLANG_TYPESYSTEM_EXPRS_TYPEEXPR_H

#include <mylang/typesystem/exprs/Expr.h>
#include <mylang/typesystem/typesystem.h>
#include <string>
#include <memory>
#include <vector>

namespace mylang::typesystem::exprs {

  /**
   * An expression that constructs a specified type.
   */
  class TypeExpr: public Expr
  {
    /** The parameters for the constraints */
  public:
    using Parameters = ::std::vector<VarPtr>;

    /** The arguments to the constructor */
  public:
    using Arguments = ::std::vector<ExprPtr>;

    /**
     * Create a new type instance
     *
     * @param name the name of the type
     * @param args the arguments to the type constructor
     * @param c constraints
     */
  public:
    TypeExpr(ExprPtr name, ExprPtr args);

    /** Destructor */
  public:
    ~TypeExpr();

    /**
     * Create a type construction expression.
     *
     * If no constraints are specified, then constraint for the expression
     * will be set to the boolean literal true.
     *
     * @param name the name of the constructor function
     * @param args the arguments to the constructor
     * @return an expression that constructs an instance of a type
     * @throws ::std::invalid_argument if the type of the name is not text
     * @throws ::std::invalid_argument if the type of the args is not a structure
     */
  public:
    static TypeExprPtr of(ExprPtr name, ExprPtr args);

    /**
     * Create a type construction expression.
     *
     * If no constraints are specified, then constraint for the expression
     * will be set to the boolean literal true.
     *
     * @param name the name of the constructor function
     * @param args the arguments to the constructor
     * @return an expression that constructs an instance of a type
     */
  public:
    static TypeExprPtr of(::std::string name, ExprPtr args);

    /**
     * Create a type construction expression.
     *
     * If no constraints are specified, then constraint for the expression
     * will be set to the boolean literal true.
     *
     * @param name the name of the constructor function
     * @param args the arguments to the constructor
     * @return an expression that constructs an instance of a type
     * @throws ::std::invalid_argument if the type of the name is not text
     */
  public:
    static TypeExprPtr of(ExprPtr name);

    /**
     * Create a type construction expression.
     *
     * If no constraints are specified, then constraint for the expression
     * will be set to the boolean literal true.
     *
     * @param name the name of the constructor function
     * @return an expression that constructs an instance of a type
     */
  public:
    static TypeExprPtr of(::std::string name);

    /**
     * Create a type construction expression.
     *
     * If no constraints are specified, then constraint for the expression
     * will be set to the boolean literal true.
     *
     * @param name the name of the constructor function
     * @param args the arguments to the constructor
     * @return an expression that constructs an instance of a type
     */
  public:
    static TypeExprPtr ofArgs(::std::string name, Arguments args);

    /**
     * Change the arguments of this type.
     * @param args the new arguments
     * @return a new expression with new arguments
     */
  public:
    TypeExprPtr replaceArguments(Arguments args) const;

  public:
    void accept(ExprVisitor &v) const override;

    /** The name of the constructor function, which implicitly defines the type  */
  public:
    const ExprPtr name;

    /** The type argument */
  public:
    const ExprPtr argument;
  };
}
#endif
