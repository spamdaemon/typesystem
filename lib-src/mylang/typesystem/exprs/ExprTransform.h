#ifndef CLASS_MYLANG_TYPESYSTEM_EXPRS_EXPRTRANSFORM_H
#define CLASS_MYLANG_TYPESYSTEM_EXPRS_EXPRTRANSFORM_H

#include <mylang/typesystem/exprs/TransformVisitor.h>
#include <mylang/typesystem/Tx.h>
#include <vector>
#include <map>
#include <mylang/typesystem/exprs/exprs.h>

namespace mylang::typesystem::exprs {

  /**
   * A function that can transform an expression and its sub-expressions.
   */
  class ExprTransform: public TransformVisitor<Tx<ExprPtr>>
  {
    /**
     * Create a new change
     * @param expr an expr
     */
  public:
    static inline Tx<ExprPtr> newChange(ExprPtr e)
    {
      return Tx<ExprPtr>(e, true);
    }

  public:
    inline Tx<ExprPtr> newChangeAndRecurse(ExprPtr e)
    {
      return Tx<ExprPtr>(transform(e).actual, true);
    }

    static inline Tx<ExprPtr> noChange(ExprPtr e)
    {
      return Tx<ExprPtr>(e, false);
    }

    /**
     * Create a recursive transform.
     */
  protected:
    inline ExprTransform()
        : recursive(true)
    {
    }

    /**
     * Create a transform.
     * @param recurse true if the transform should be recursive
     */
  protected:
    inline ExprTransform(bool recurse)
        : recursive(recurse)
    {
    }

    /** Destructor */
  public:
    virtual ~ExprTransform();

    /**
     * Transform a list of expressions.
     * @param list a list of expression
     * @return a list of expression
     */
  protected:
    Tx<::std::vector<ExprPtr>> transformList(const ::std::vector<ExprPtr> &list);

    template<class K>
    Tx<::std::map<K, ExprPtr>> transformMap(const ::std::map<K, ExprPtr> &map)
    {
      ::std::map<K, ExprPtr> out;
      bool isNew = false;
      for (const auto &arg : map) {
        auto r = transform(arg.second);
        isNew = isNew || r.isNew;
        out.emplace(arg.first, ::std::move(r.actual));
      }
      return Tx<::std::map<K, ExprPtr>>(::std::move(out), isNew);
    }

    template<class K>
    Tx<::std::map<K, VarPtr>> transformMap(const ::std::map<K, VarPtr> &map)
    {
      ::std::map<K, VarPtr> out;
      bool isNew = false;
      for (const auto &arg : map) {
        auto r = transform(arg.second);
        isNew = isNew || r.isNew;
        VarPtr v = r.actual->template self<VarExpr>();
        out.emplace(arg.first, ::std::move(v));
      }
      return Tx<::std::map<K, VarPtr>>(::std::move(out), isNew);
    }

    virtual Tx<FunctionPtr> transformFunction(const FunctionPtr &f);

  public:
    Tx<ExprPtr> visitDefault(const ExprPtr &e) override;

    Tx<ExprPtr> visit(const ::std::shared_ptr<const BuiltinExpr> &e) override;
    Tx<ExprPtr> visit(const ::std::shared_ptr<const ConditionalExpr> &e) override;
    Tx<ExprPtr> visit(const ::std::shared_ptr<const CallExpr> &e) override;
    Tx<ExprPtr> visit(const ::std::shared_ptr<const ConstructExpr> &e) override;
    Tx<ExprPtr> visit(const ::std::shared_ptr<const LetExpr> &e) override;
    Tx<ExprPtr> visit(const ::std::shared_ptr<const ListLiteral> &e) override;
    Tx<ExprPtr> visit(const ::std::shared_ptr<const TypeExpr> &e) override;

    /** True if the transform should be recursive */
  public:
    const bool recursive;
  };
}
#endif
