#ifndef CLASS_MYLANG_TYPESYSTEM_EXPRS_LITERALEXPR_H
#define CLASS_MYLANG_TYPESYSTEM_EXPRS_LITERALEXPR_H

#include <mylang/typesystem/exprs/Expr.h>
#include <mylang/typesystem/types/Type.h>

namespace mylang::typesystem::exprs {
  class ExprVisitor;

  /**
   * An abstract class that represent literals.
   *
   * Literals that are composites must consist entirely of other literals
   */
  class LiteralExpr: public Expr
  {
    /** A pointer to aliteral */
  public:
    using Ptr = ::std::shared_ptr<const LiteralExpr>;

    /**
     * Create an expression with specified type.
     */
  protected:
    LiteralExpr(TypePtr type);

    /** Destructor */
  public:
    ~LiteralExpr();
  };
}
#endif
