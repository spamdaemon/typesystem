#include <mylang/typesystem/exprs/BLiteral.h>
#include <mylang/typesystem/exprs/ExprVisitor.h>
#include <mylang/typesystem/types/BType.h>

namespace mylang::typesystem::exprs {
  using namespace mylang::typesystem::types;

  BLiteral::BLiteral(bool v)
      : LiteralExpr(BType::create()), value(::std::move(v))
  {

  }

  BLiteral::~BLiteral()
  {
  }

  ExprPtr BLiteral::of(bool v)
  {
    return ::std::make_shared<BLiteral>(::std::move(v));
  }

  void BLiteral::accept(ExprVisitor &v) const
  {
    v.visit(self<BLiteral>());
  }

}
