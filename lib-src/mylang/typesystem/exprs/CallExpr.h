#ifndef CLASS_MYLANG_TYPESYSTEM_EXPRS_CONSTRAINEDEXPR_H
#define CLASS_MYLANG_TYPESYSTEM_EXPRS_CONSTRAINEDEXPR_H

#include <mylang/typesystem/exprs/Expr.h>
#include <mylang/typesystem/Function.h>
#include <string>

namespace mylang::typesystem::exprs {

  /**
   * An expression that invokes a function in the environment.
   */
  class CallExpr: public Expr
  {
    /** The arguments */
  public:
    using Arguments = Function::Arguments;

    /**
     * Constructor
     *
     * @param f the name of the function
     * @param args the arguments
     */
  public:
    CallExpr(FunctionPtr f, Arguments args);

    /** Destructor */
  public:
    ~CallExpr();

    /**
     * Create a new function call expression
     * @param ty the type of the result
     * @param f the name of the function to call
     * @param args the function arguments
     */
  public:
    static ::std::shared_ptr<const CallExpr> of(FunctionPtr ptr, Arguments args);

  public:
    void accept(ExprVisitor &v) const override;

    /** The function to call */
  public:
    const FunctionPtr target;

    /** The arguments */
  public:
    const Arguments arguments;
    ;
  };
}
#endif
