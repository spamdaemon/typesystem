#include <mylang/typesystem/exprs/ConditionalExpr.h>
#include <mylang/typesystem/exprs/ErrorExpr.h>
#include <mylang/typesystem/exprs/ExprVisitor.h>
#include <mylang/typesystem/types/BType.h>
#include <mylang/typesystem/types/XType.h>
#include <mylang/typesystem/TypeMismatch.h>
#include <stdexcept>
#include <mylang/Self.h>
#include <mylang/typesystem/typesystem.h>
#include <algorithm>
#include <memory>

namespace mylang::typesystem::exprs {

  ConditionalExpr::ConditionalExpr(ExprPtr c, ExprPtr t, ExprPtr f)
      : Expr(t->type), condition(::std::move(c)), iftrue(::std::move(t)), iffalse(::std::move(f))
  {
    if (condition->type->self<types::BType>() == nullptr) {
      throw ::std::invalid_argument("Bad condition");
    }
    if (iftrue->type->unifiable(iffalse->type) == false) {
      throw TypeMismatch(iftrue->type, iffalse->type);
    }
  }

  ConditionalExpr::~ConditionalExpr()
  {
  }

  ::std::shared_ptr<const ConditionalExpr> ConditionalExpr::of(ExprPtr c, ExprPtr t, ExprPtr f)
  {
    return ::std::make_shared<ConditionalExpr>(::std::move(c), ::std::move(t), ::std::move(f));
  }

  ::std::shared_ptr<const ConditionalExpr> ConditionalExpr::guardOf(ExprPtr c, ExprPtr t)
  {
    return of(c, t, ErrorExpr::of(t->type));
  }

  void ConditionalExpr::accept(ExprVisitor &v) const
  {
    v.visit(self<ConditionalExpr>());
  }

}
