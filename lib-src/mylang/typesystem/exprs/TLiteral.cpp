#include <mylang/typesystem/exprs/TLiteral.h>
#include <mylang/typesystem/exprs/ExprVisitor.h>
//#include <mylang/typesystem/types/BType.h>
#include <mylang/Self.h>
#include <mylang/typesystem/types/TType.h>
#include <mylang/typesystem/typesystem.h>
#include <algorithm>
#include <memory>
#include <string>

namespace mylang::typesystem::exprs {
  using namespace mylang::typesystem::types;

  TLiteral::TLiteral(::std::string v)
      : LiteralExpr(TType::create()), value(::std::move(v))
  {

  }

  TLiteral::~TLiteral()
  {
  }

  ExprPtr TLiteral::of(::std::string v)
  {
    return ::std::make_shared<TLiteral>(::std::move(v));
  }

  void TLiteral::accept(ExprVisitor &v) const
  {
    v.visit(self<TLiteral>());
  }

}
