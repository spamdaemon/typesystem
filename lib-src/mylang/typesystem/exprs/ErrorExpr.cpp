#include <mylang/typesystem/exprs/ErrorExpr.h>
#include <mylang/typesystem/exprs/ExprVisitor.h>

namespace mylang::typesystem::exprs {

  ErrorExpr::ErrorExpr(TypePtr t)
      : Expr(::std::move(t))
  {

  }

  ErrorExpr::~ErrorExpr()
  {
  }

  ExprPtr ErrorExpr::of(TypePtr t)
  {
    return ::std::make_shared<ErrorExpr>(::std::move(t));
  }

  void ErrorExpr::accept(ExprVisitor &v) const
  {
    v.visit(self<ErrorExpr>());
  }

}
