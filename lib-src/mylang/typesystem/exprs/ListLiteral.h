#ifndef CLASS_MYLANG_TYPESYSTEM_EXPRS_LISTLITERAL_H
#define CLASS_MYLANG_TYPESYSTEM_EXPRS_LISTLITERAL_H

#include <mylang/typesystem/exprs/LiteralExpr.h>
#include <mylang/typesystem/typesystem.h>
#include <mylang/typesystem/types/LType.h>
#include <memory>
#include <vector>

namespace mylang::typesystem::exprs {

  /**
   * An expression that references a type.
   */
  class ListLiteral: public LiteralExpr
  {
    /**
     * Create an empty list
     * @param list a list type
     * @param the list entries
     */
  public:
    ListLiteral(::std::shared_ptr<const types::LType> listTy, ::std::vector<ExprPtr> entries);

    /** Destructor */
  public:
    ~ListLiteral();

    /**
     * Create an empty list
     * @param ty the type of list
     * @return an expression that constructs an empty list
     */
  public:
    static ExprPtr of(::std::shared_ptr<const types::LType> listTy);

    /**
     * Create an empty list
     * @param ty the type of list
     * @param entries the entries in the list
     * @return an expression that constructs an empty list
     */
  public:
    static ExprPtr of(::std::shared_ptr<const types::LType> listTy, ::std::vector<ExprPtr> entries);

  public:
    void accept(ExprVisitor &v) const override;

    /** The list value */
  public:
    const ::std::vector<ExprPtr> entries;
  };
}
#endif
