#ifndef CLASS_MYLANG_TYPESYSTEM_EXPRS_BUILTINEXPR_H
#define CLASS_MYLANG_TYPESYSTEM_EXPRS_BUILTINEXPR_H

#include <mylang/typesystem/exprs/Expr.h>
#include <mylang/typesystem/typesystem.h>
#include <vector>

namespace mylang::typesystem::exprs {

  /**
   * An expression expression as an operator and arguments.
   */
  class BuiltinExpr: public Expr
  {
    /** The arguments */
  public:
    using Operands = ::std::vector<ExprPtr>;

    /** The operators */
  public:
    enum Operator
    {
      ADD, ALL, ANY, AT, CONS, EQ, LENGTH, LT, LTE, NEGATE, NOT
    };

  public:
    static const char* toString(Operator p);

    /**
     * Create an expression with specified type.
     * @param type the expected output type
     * @param op the operator
     * @param operands
     * @throws ::std::invalid_argument if the expression is not valid
     */
  public:
    BuiltinExpr(TypePtr type, Operator op, Operands operands);

    /** Destructor */
  public:
    ~BuiltinExpr();

    /**
     * Create an expression.
     * @param type the result type of this expression
     * @param op the operator
     * @param args the arguments
     * @return a builtin expression
     */
  public:
    static ::std::shared_ptr<const BuiltinExpr> of(TypePtr type, Operator op, Operands args);

    /**
     * Create an expression to add two value.
     * @param arg1 an argument
     * @param arg2 an argument
     * @return an expression that sums the two arguments
     */
  public:
    static ExprPtr sumOf(ExprPtr arg1, ExprPtr arg2);

    /**
     * Create an expression to negates a value.
     * @param arg0 an argument
     * @return an expression that negates a value
     */
  public:
    static ExprPtr negationOf(ExprPtr arg);

    /**
     * Create an expression to negates a boolean value.
     * @param arg0 an argument
     * @return an expression that negates a value
     */
  public:
    static ExprPtr notOf(ExprPtr arg);

    /**
     * Create an expression to ANDs two boolean values.
     * @param arg1 an argument
     * @param arg2 an argument
     * @return an expression that returns a boolean
     */
  public:
    static ExprPtr andOf(ExprPtr arg1, ExprPtr arg2);

    /**
     * Create an expression to ORs to boolean values.
     * @param arg1 an argument
     * @param arg2 an argument
     * @return an expression that returns a boolean
     */
  public:
    static ExprPtr orOf(ExprPtr arg1, ExprPtr arg2);

    /**
     * Create an expression to ANDs multiple boolean values.
     *
     * This expression returns true if all arguments  evaluate
     * to true. If no arguments are provided, then this expression returns true.
     *
     * @param args the arguments.
     * @return an expression that returns a boolean
     */
  public:
    static ExprPtr allOf(Operands args);

    /**
     * Create an expression to ORs multiple boolean values.
     *
     * This expression returns true if at least one argument evaluates
     * to true. If no arguments are provided, then this expression returns false.
     *
     * @param args the arguments.
     * @return an expression that returns a boolean
     */
  public:
    static ExprPtr anyOf(Operands args);

    /**
     * Create an expression to compares two values for equality.
     * @param arg1 an argument
     * @param arg2 an argument
     * @return an expression that returns a boolean
     */
  public:
    static ExprPtr eqOf(ExprPtr arg1, ExprPtr arg2);

    /**
     * Create an expression that determines if a value is smaller than
     * some other value.
     * @param arg1 an argument
     * @param arg2 an argument
     * @return an expression that returns a boolean
     */
  public:
    static ExprPtr ltOf(ExprPtr arg1, ExprPtr arg2);

    /**
     * Create an expression that determines if a value is smaller than
     * some other value or equal to it.
     * @param arg1 an argument
     * @param arg2 an argument
     * @return an expression that returns a boolean
     */
  public:
    static ExprPtr lteOf(ExprPtr arg1, ExprPtr arg2);

    /**
     * Create an expression that determines if a value is bigger than
     * some other value.
     * @param arg1 an argument
     * @param arg2 an argument
     * @return an expression that returns a boolean
     */
  public:
    static ExprPtr gtOf(ExprPtr arg1, ExprPtr arg2);

    /**
     * Create an expression that determines if a value is bigger than
     * some other value or equal to it.
     * @param arg1 an argument
     * @param arg2 an argument
     * @return an expression that returns a boolean
     */
  public:
    static ExprPtr gteOf(ExprPtr arg1, ExprPtr arg2);

    /**
     * Create an expression to gets the length of something.
     * @param arg an argument
     * @return an expression that returns an integer
     */
  public:
    static ExprPtr lengthOf(ExprPtr arg);

    /**
     * Create an expression to gets the length of something.
     * @param index an expression of type integer
     * @param obj an object
     * @return an expression that returns the value at index in the object
     */
  public:
    static ExprPtr atIndexOf(ExprPtr index, ExprPtr obj);

    /**
     * Create an expression that prepends an element to a list.
     * @param element an element
     * @param list an list
     * @return an expression for a new list
     */
  public:
    static ExprPtr consOf(ExprPtr element, ExprPtr list);

    /**
     * Create a copy of this builtin expr, but with new argument.
     * @param args the new arguments
     * @return an expression with the same operator and type, but different arguments
     */
  public:
    ExprPtr replaceOperands(Operands args) const;

    /**
     * Simplify this expression.
     *
     * @return an equivalent expression that is possibly simpler.
     */
  public:
    ExprPtr simplify() const;

  public:
    void accept(ExprVisitor &v) const override;

    /** The operator */
  public:
    const Operator op;

    /** The right hand side */
  public:
    const Operands operands;
  };
}
#endif
