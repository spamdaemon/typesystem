#ifndef CLASS_MYLANG_TYPESYSTEM_EXPRS_TLITERAL_H
#define CLASS_MYLANG_TYPESYSTEM_EXPRS_TLITERAL_H

#include <mylang/typesystem/exprs/LiteralExpr.h>
#include <mylang/typesystem/typesystem.h>
#include <string>
#include <map>

namespace mylang::typesystem::exprs {

  /**
   * An expression that references a type.
   */
  class TLiteral: public LiteralExpr
  {
    /**
     * Create a new literal.
     * @param value the value
     */
  public:
    TLiteral(::std::string value);

    /** Destructor */
  public:
    ~TLiteral();

    /**
     * Create a new literal
     * @param value the value
     * @return an expression that constructs an instance of a type
     */
  public:
    static ExprPtr of(::std::string value);

  public:
    void accept(ExprVisitor &v) const override;

    /** The literal value  */
  public:
    const ::std::string value;
  };
}
#endif
