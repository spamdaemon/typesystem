#include <mylang/typesystem/exprs/VarExpr.h>
#include <mylang/typesystem/Environment.h>
#include <mylang/typesystem/exprs/ExprVisitor.h>
#include <mylang/Self.h>
#include <mylang/typesystem/typesystem.h>
#include <mylang/typesystem/types/WType.h>
#include <mylang/typesystem/algorithms/Freshen.h>
#include <algorithm>
#include <memory>

namespace mylang::typesystem::exprs {

  VarExpr::VarExpr(TypePtr t, VarName n)
      : Expr(::std::move(t)), name(::std::move(n))
  {
  }

  VarExpr::~VarExpr()
  {
  }

  VarPtr VarExpr::of(TypePtr t, VarName n)
  {
    return ::std::make_shared<VarExpr>(::std::move(t), ::std::move(n));
  }
  VarPtr VarExpr::of(TypePtr t)
  {
    return of(t, VarName());
  }
  VarPtr VarExpr::wildcard()
  {
    return of(types::WType::create());
  }
  void VarExpr::accept(ExprVisitor &v) const
  {
    v.visit(self<VarExpr>());
  }

  ::std::pair<VarPtr, Environment> VarExpr::freshen(const Environment &env) const
  {
    auto res = algorithms::Freshen::apply(self(), true, env);
    return {res.actual.first->self<VarExpr>(), res.actual.second};
  }

}
