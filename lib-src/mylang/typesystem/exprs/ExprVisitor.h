#ifndef CLASS_MYLANG_TYPESYSTEM_EXPRS_EXPRVISITOR_H
#define CLASS_MYLANG_TYPESYSTEM_EXPRS_EXPRVISITOR_H

#include <mylang/typesystem/exprs/BuiltinExpr.h>
#include <mylang/typesystem/exprs/BLiteral.h>
#include <mylang/typesystem/exprs/ConditionalExpr.h>
#include <mylang/typesystem/exprs/CallExpr.h>
#include <mylang/typesystem/exprs/ConstructExpr.h>
#include <mylang/typesystem/exprs/ErrorExpr.h>
#include <mylang/typesystem/exprs/ListLiteral.h>
#include <mylang/typesystem/exprs/ILiteral.h>
#include <mylang/typesystem/exprs/TLiteral.h>
#include <mylang/typesystem/exprs/TypeExpr.h>
#include <mylang/typesystem/exprs/VarExpr.h>
#include <mylang/typesystem/exprs/LetExpr.h>
#include <mylang/typesystem/exprs/TypeExpr.h>

namespace mylang::typesystem::exprs {

  /**
   * The base type for all expressions used in the typesystem.
   */
  class ExprVisitor
  {
    /**
     * Create an expression with specified type.
     */
  protected:
    inline ExprVisitor() = default;

    /** Destructor */
  public:
    virtual ~ExprVisitor()
    {
    }
    ;

  public:
    virtual void visit(const ::std::shared_ptr<const BLiteral> &e)=0;
    virtual void visit(const ::std::shared_ptr<const ILiteral> &e)=0;
    virtual void visit(const ::std::shared_ptr<const TLiteral> &e)=0;
    virtual void visit(const ::std::shared_ptr<const BuiltinExpr> &e)=0;
    virtual void visit(const ::std::shared_ptr<const ConditionalExpr> &e)=0;
    virtual void visit(const ::std::shared_ptr<const CallExpr> &e)=0;
    virtual void visit(const ::std::shared_ptr<const ConstructExpr> &e)=0;
    virtual void visit(const ::std::shared_ptr<const LetExpr> &e)=0;
    virtual void visit(const ::std::shared_ptr<const ListLiteral> &e)=0;
    virtual void visit(const ::std::shared_ptr<const ErrorExpr> &e) = 0;
    virtual void visit(const ::std::shared_ptr<const TypeExpr> &e) = 0;
    virtual void visit(const ::std::shared_ptr<const VarExpr> &e) = 0;
  };
}
#endif
