#ifndef CLASS_MYLANG_TYPESYSTEM_EXPRS_CONSTRUCTEXPR_H
#define CLASS_MYLANG_TYPESYSTEM_EXPRS_CONSTRUCTEXPR_H

#include <mylang/typesystem/exprs/Expr.h>
#include <mylang/typesystem/typesystem.h>
#include <string>
#include <vector>

namespace mylang::typesystem::exprs {

  /**
   * An expression that references a type.
   */
  class ConstructExpr: public Expr
  {

    /** The arguments to the constructor */
  public:
    using Arguments = ::std::vector<ExprPtr>;

    /**
     * An expression that references a variable.
     * @param type the type of the expression
     * @param args the arguments to the type constructor
     */
  public:
    ConstructExpr(Arguments args);

    /** Destructor */
  public:
    ~ConstructExpr();

    /**
     * Create a constructor for an SType.
     *
     * The type of the expression is inferred from the arguments.
     *
     * @param type the type of expression
     * @param args the arguments to the constructor
     * @return an expression that constructs an instance of a type
     */
  public:
    static ::std::shared_ptr<const ConstructExpr> of(Arguments args = Arguments());

  public:
    void accept(ExprVisitor &v) const override;

    /** The arguments */
  public:
    const Arguments arguments;
  };
}
#endif
