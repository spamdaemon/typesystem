#ifndef CLASS_MYLANG_TYPESYSTEM_EXPRS_TRANSFORMVISITOR_H
#define CLASS_MYLANG_TYPESYSTEM_EXPRS_TRANSFORMVISITOR_H

#include <mylang/typesystem/exprs/ExprVisitor.h>

namespace mylang::typesystem::exprs {

  /**
   * The base type for all expressions used in the typesystem.
   */
  template<class T>
  class TransformVisitor
  {
    /**
     * Create an expression with specified type.
     */
  protected:
    inline TransformVisitor() = default;

    /** Destructor */
  public:
    virtual ~TransformVisitor()
    {
    }

    /**
     * Transform an expression
     * @param e an expression
     * @return the value
     */
  public:
    virtual T transform(ExprPtr e)
    {
      struct V: public ExprVisitor
      {
        V(TransformVisitor &t)
            : tx(t)
        {
        }
        ~V()
        {
        }
        TransformVisitor &tx;
        ::std::optional<T> result;

        void visit(const ::std::shared_ptr<const ErrorExpr> &e) override
        {
          result = tx.visit(e);
        }

        void visit(const ::std::shared_ptr<const BLiteral> &e) override
        {
          result = tx.visit(e);
        }
        void visit(const ::std::shared_ptr<const ILiteral> &e) override
        {
          result = tx.visit(e);
        }
        void visit(const ::std::shared_ptr<const TLiteral> &e) override
        {
          result = tx.visit(e);
        }

        void visit(const ::std::shared_ptr<const BuiltinExpr> &e) override
        {
          result = tx.visit(e);
        }

        void visit(const ::std::shared_ptr<const ConditionalExpr> &e) override
        {
          result = tx.visit(e);
        }

        void visit(const ::std::shared_ptr<const CallExpr> &e) override
        {
          result = tx.visit(e);
        }

        void visit(const ::std::shared_ptr<const ConstructExpr> &e) override
        {
          result = tx.visit(e);
        }

        void visit(const ::std::shared_ptr<const LetExpr> &e) override
        {
          result = tx.visit(e);
        }

        void visit(const ::std::shared_ptr<const ListLiteral> &e) override
        {
          result = tx.visit(e);
        }

        void visit(const ::std::shared_ptr<const TypeExpr> &e) override
        {
          result = tx.visit(e);
        }

        void visit(const ::std::shared_ptr<const VarExpr> &e) override
        {
          result = tx.visit(e);
        }

      };
      V v(*this);
      e->accept(v);
      return ::std::move(*v.result);
    }

  public:
    virtual T visitDefault(const ExprPtr &e) = 0;

    virtual T visit(const ::std::shared_ptr<const ErrorExpr> &e)
    {
      return visitDefault(e);
    }
    virtual T visit(const ::std::shared_ptr<const BLiteral> &e)
    {
      return visitDefault(e);
    }
    virtual T visit(const ::std::shared_ptr<const ILiteral> &e)
    {
      return visitDefault(e);
    }
    virtual T visit(const ::std::shared_ptr<const TLiteral> &e)
    {
      return visitDefault(e);
    }
    virtual T visit(const ::std::shared_ptr<const BuiltinExpr> &e)
    {
      return visitDefault(e);
    }
    virtual T visit(const ::std::shared_ptr<const ConditionalExpr> &e)
    {
      return visitDefault(e);
    }
    virtual T visit(const ::std::shared_ptr<const CallExpr> &e)
    {
      return visitDefault(e);
    }
    virtual T visit(const ::std::shared_ptr<const ConstructExpr> &e)
    {
      return visitDefault(e);
    }
    virtual T visit(const ::std::shared_ptr<const LetExpr> &e)
    {
      return visitDefault(e);
    }
    virtual T visit(const ::std::shared_ptr<const ListLiteral> &e)
    {
      return visitDefault(e);
    }
    virtual T visit(const ::std::shared_ptr<const TypeExpr> &e)
    {
      return visitDefault(e);
    }
    virtual T visit(const ::std::shared_ptr<const VarExpr> &e)
    {
      return visitDefault(e);
    }
  };
}
#endif
