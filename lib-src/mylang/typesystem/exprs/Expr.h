#ifndef CLASS_MYLANG_TYPESYSTEM_EXPRS_EXPR_H
#define CLASS_MYLANG_TYPESYSTEM_EXPRS_EXPR_H

#include <mylang/Self.h>
#include <mylang/typesystem/typesystem.h>
#include <mylang/typesystem/types/Type.h>

namespace mylang::typesystem::exprs {
  class ExprVisitor;

  /**
   * The base type for all expressions used in the typesystem.
   */
  class Expr: public Self<Expr>
  {
    /**
     * Create an expression with specified type.
     */
  protected:
    Expr(TypePtr type);

    /** Destructor */
  public:
    ~Expr();

    /**
     * Accept a visitor
     * @param v a visitor
     */
  public:
    virtual void accept(ExprVisitor &v) const = 0;

    /** The type of the expression */
  public:
    const TypePtr type;
  };
}
#endif
