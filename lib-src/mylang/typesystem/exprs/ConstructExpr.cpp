#include <mylang/typesystem/exprs/ConstructExpr.h>
#include <mylang/typesystem/exprs/ExprVisitor.h>
#include <mylang/typesystem/types/SType.h>
#include <mylang/Self.h>
#include <algorithm>
#include <memory>

namespace mylang::typesystem::exprs {

  static TypePtr mkSType(const ConstructExpr::Arguments &args)
  {
    types::SType::Members members;
    members.reserve(args.size());
    for (const auto &arg : args) {
      members.push_back(arg->type);
    }
    return types::SType::create(::std::move(members));
  }

  ConstructExpr::ConstructExpr(Arguments args)
      : Expr(mkSType(args)), arguments(::std::move(args))
  {
  }

  ConstructExpr::~ConstructExpr()
  {
  }

  ::std::shared_ptr<const ConstructExpr> ConstructExpr::of(Arguments args)
  {
    return ::std::make_shared<ConstructExpr>(::std::move(args));
  }

  void ConstructExpr::accept(ExprVisitor &v) const
  {
    v.visit(self<ConstructExpr>());
  }

}
