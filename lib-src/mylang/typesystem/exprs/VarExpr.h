#ifndef CLASS_MYLANG_TYPESYSTEM_EXPRS_VAREXPR_H
#define CLASS_MYLANG_TYPESYSTEM_EXPRS_VAREXPR_H

#include <mylang/typesystem/exprs/Expr.h>
#include <mylang/typesystem/Environment.h>
#include <mylang/typesystem/VarName.h>
#include <mylang/typesystem/RBTree.h>
#include <mylang/typesystem/typesystem.h>
#include <utility>

namespace mylang::typesystem::exprs {

  /**
   * The base type for all expressions used in the typesystem.
   */
  class VarExpr: public Expr
  {
    /**
     * An expression that references a variable.
     * @param type the type of the expressions
     * @param var the variable name
     */
  public:
    VarExpr(TypePtr type, VarName var);

    /** Destructor */
  public:
    ~VarExpr();

    /**
     * Create VarExpr
     * @param type the type of the variable
     * @param name the name of a variable to reference
     * @return an expression that references the specified variable
     */
  public:
    static VarPtr of(TypePtr type, VarName name);

    /**
     * Create VarExpr using a fresh variable name.
     * @param type the type of the variable
     * @return an expression that references a variable
     */
  public:
    static VarPtr of(TypePtr typen);

    /**
     * Create wildcard variable.
     * @return an expression refernces a wildcard
     */
  public:
    static VarPtr wildcard();

  public:
    void accept(ExprVisitor &v) const override;

    /**
     * Create a new fresh variable.
     * @param vars an existing mapping of variables
     * @return a new variable or a variable from vars
     */
  public:
    ::std::pair<VarPtr, Environment> freshen(const Environment &vars) const;

    /** The left hand side */
  public:
    const VarName name;
  };
}
#endif
