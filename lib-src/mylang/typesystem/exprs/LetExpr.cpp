#include <mylang/typesystem/exprs/LetExpr.h>
#include <mylang/typesystem/exprs/VarExpr.h>
#include <mylang/typesystem/exprs/ExprVisitor.h>
#include <stdexcept>
#include <algorithm>
#include <memory>
#include <mylang/Self.h>
#include <mylang/typesystem/typesystem.h>
#include <mylang/typesystem/VarName.h>
#include <functional>
#include <vector>

namespace mylang::typesystem::exprs {

  namespace {
    static VarPtr mkVar(ExprPtr value)
    {
      if (auto var = value->self<VarExpr>(); var) {
        // no need to introduce a temporary for another variable
        return var;
      } else {
        VarName n("tmp");
        return VarExpr::of(value->type, n);
      }
    }
  }

  LetExpr::LetExpr(VarName xvariable, ExprPtr xvalue, ExprPtr xresult)
      : Expr(xresult->type), variable(::std::move(xvariable)), value(::std::move(xvalue)),
          result(::std::move(xresult))
  {
  }

  LetExpr::~LetExpr()
  {
  }

  ExprPtr LetExpr::of(VarName xvariable, ExprPtr xvalue, ExprPtr xresult)
  {
    return ::std::make_shared<LetExpr>(::std::move(xvariable), ::std::move(xvalue),
        ::std::move(xresult));
  }

  ExprPtr LetExpr::of(ExprPtr value, ::std::function<ExprPtr(VarPtr)> result)
  {
    auto v = mkVar(value);
    if (v == value) {
      // we don't need a temporary, since the value is already a variable
      return result(v);
    } else {
      return ::std::make_shared<LetExpr>(v->name, ::std::move(value), result(v));
    }
  }

  ExprPtr LetExpr::of(ExprPtr left, ExprPtr right, ::std::function<ExprPtr(VarPtr, VarPtr)> result)
  {
    return of(left, [=](VarPtr lhs) { //
          return of(right,[=](VarPtr rhs) { //
                return result(lhs,rhs);//
              }); //
        });
  }

  ExprPtr LetExpr::of(const ::std::vector<ExprPtr> &values,
      ::std::function<ExprPtr(::std::vector<VarPtr>)> result)
  {
    ::std::vector<VarPtr> vars;
    vars.reserve(values.size());
    for (const auto &p : values) {
      vars.push_back(mkVar(p));
    }
    // compute the result first
    auto res = result(vars);
    // then create let expressions (first variable will be most deeply nested let)
    for (size_t i = 0; i < values.size(); ++i) {
      res = ::std::make_shared<LetExpr>(vars.at(i)->name, values.at(i), res);
    }
    return res;
  }

  void LetExpr::accept(ExprVisitor &v) const
  {
    v.visit(self<LetExpr>());
  }

}
