#ifndef CLASS_MYLANG_TYPESYSTEM_EXPRS_ILITERAL_H
#define CLASS_MYLANG_TYPESYSTEM_EXPRS_ILITERAL_H

#include <mylang/Integer.h>
#include <mylang/typesystem/exprs/LiteralExpr.h>
#include <mylang/typesystem/typesystem.h>
#include <string>
#include <map>

namespace mylang::typesystem::exprs {

  /**
   * An expression that references a type.
   */
  class ILiteral: public LiteralExpr
  {
    /**
     * Create a new literal.
     * @param value the value
     */
  public:
    ILiteral(Integer value);

    /** Destructor */
  public:
    ~ILiteral();

    /**
     * Create a new literal
     * @param value the value
     * @return an expression that constructs an instance of a type
     */
  public:
    static ExprPtr of(Integer value);

    /**
     * Create a new literal
     * @param value the value
     * @return an expression that constructs an instance of a type
     */
  public:
    static ExprPtr of(int value);

    /**
     * Create a new literal
     * @param value the value
     * @return an expression that constructs an instance of a type
     */
  public:
    static ExprPtr of(size_t value);

  public:
    void accept(ExprVisitor &v) const override;

    /** The name of the constructor function, which implicitly defines the type of  */
  public:
    const Integer value;
  };
}
#endif
