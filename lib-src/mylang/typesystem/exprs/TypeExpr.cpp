#include <mylang/typesystem/exprs/TypeExpr.h>
#include <mylang/typesystem/exprs/TLiteral.h>
#include <mylang/typesystem/exprs/ConstructExpr.h>
#include <mylang/typesystem/exprs/ExprVisitor.h>
#include <mylang/typesystem/types/XType.h>
#include <mylang/typesystem/types/TType.h>
#include <mylang/typesystem/types/SType.h>
#include <stdexcept>

namespace mylang::typesystem::exprs {
  TypeExpr::TypeExpr(ExprPtr xname, ExprPtr arg)
      : Expr(types::XType::create(arg->type)), name(::std::move(xname)), argument(::std::move(arg))
  {
    if (!name->type->unifiable<types::TType>()) {
      throw ::std::invalid_argument("Expected a string expression");
    }

    if (!argument->type->unifiable<types::SType>()) {
      throw ::std::runtime_error("Type argument must be a tuple type");
    }
  }

  TypeExpr::~TypeExpr()
  {
  }

  TypeExprPtr TypeExpr::replaceArguments(Arguments args) const
  {
    return of(name, ConstructExpr::of(::std::move(args)));
  }

  TypeExprPtr TypeExpr::of(ExprPtr xname, ExprPtr args)
  {
    return ::std::make_shared<TypeExpr>(::std::move(xname), ::std::move(args));
  }

  TypeExprPtr TypeExpr::of(::std::string xname, ExprPtr args)
  {
    return TypeExpr::of(TLiteral::of(::std::move(xname)), ::std::move(args));
  }

  TypeExprPtr TypeExpr::of(ExprPtr xname)
  {
    return of(::std::move(xname), ConstructExpr::of());
  }

  TypeExprPtr TypeExpr::of(::std::string xname)
  {
    return of(TLiteral::of(::std::move(xname)), ConstructExpr::of());
  }

  TypeExprPtr TypeExpr::ofArgs(::std::string xname, Arguments args)
  {
    return of(TLiteral::of(::std::move(xname)), ConstructExpr::of(::std::move(args)));
  }

  void TypeExpr::accept(ExprVisitor &v) const
  {
    v.visit(self<TypeExpr>());
  }

}
