#ifndef CLASS_MYLANG_TYPESYSTEM_EXPRS_ERROREXPR_H
#define CLASS_MYLANG_TYPESYSTEM_EXPRS_ERROREXPR_H

#include <mylang/typesystem/exprs/Expr.h>

namespace mylang::typesystem::exprs {

  /**
   * An expression that references a type.
   */
  class ErrorExpr: public Expr
  {
    /**
     * Create a new error.
     * @param type the type of expression
     */
  public:
    ErrorExpr(TypePtr type);

    /** Destructor */
  public:
    ~ErrorExpr();

    /**
     * Create a new error expression.
     */
  public:
    static ExprPtr of(TypePtr type);

  public:
    void accept(ExprVisitor &v) const override;
  };
}
#endif
