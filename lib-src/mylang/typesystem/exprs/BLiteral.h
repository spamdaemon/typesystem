#ifndef CLASS_MYLANG_TYPESYSTEM_EXPRS_BLITERAL_H
#define CLASS_MYLANG_TYPESYSTEM_EXPRS_BLITERAL_H

#include <mylang/typesystem/exprs/LiteralExpr.h>
#include <mylang/typesystem/typesystem.h>
#include <string>
#include <map>

namespace mylang::typesystem::exprs {

  /**
   * An expression that references a type.
   */
  class BLiteral: public LiteralExpr
  {
    /**
     * Create a new literal.
     * @param value the value
     */
  public:
    BLiteral(bool value);

    /** Destructor */
  public:
    ~BLiteral();

    /**
     * Create a new literal
     * @param value the value
     * @return an expression that constructs an instance of a type
     */
  public:
    static ExprPtr of(bool value);

  public:
    void accept(ExprVisitor &v) const override;

    /** The name of the constructor function, which implicitly defines the type of  */
  public:
    const bool value;
  };
}
#endif
