#include <mylang/typesystem/exprs/ExprTransform.h>
#include <iostream>

namespace mylang::typesystem::exprs {

  ExprTransform::~ExprTransform()
  {
  }

  Tx<::std::vector<ExprPtr>> ExprTransform::transformList(const ::std::vector<ExprPtr> &list)
  {
    ::std::vector<ExprPtr> args;
    args.reserve(list.size());
    bool isNew = false;
    for (const auto &arg : list) {
      auto r = transform(arg);
      isNew = isNew || r.isNew;
      args.push_back(r.actual);
    }
    return Tx<::std::vector<ExprPtr>>(::std::move(args), isNew);
  }

  Tx<FunctionPtr> ExprTransform::transformFunction(const FunctionPtr &f)
  {
    Function::Parameters args;
    args.reserve(f->parameters.size());
    bool isNew = false;
    for (const auto &arg : f->parameters) {
      auto r = transform(arg);
      isNew = isNew || r.isNew;
      args.push_back(r.actual->self<VarExpr>());
    }
    auto body = transform(f->body);
    if (isNew || body.isNew) {
      return Tx<FunctionPtr>::txNew(
          Function::of(f->typeParameters, ::std::move(args), ::std::move(body.actual)));
    } else {
      return Tx<FunctionPtr>::txOld(f);
    }
  }

  Tx<ExprPtr> ExprTransform::visitDefault(const ExprPtr &e)
  {
    return noChange(e);
  }

  Tx<ExprPtr> ExprTransform::visit(const ::std::shared_ptr<const BuiltinExpr> &e)
  {
    if (recursive) {
      auto args = transformList(e->operands);
      for (auto ex : args.actual) {
        if (ex->self<ErrorExpr>()) {
          return newChange(ErrorExpr::of(e->type));
        }
      }
      if (args.isNew) {
        return newChange(e->replaceOperands(::std::move(args.actual)));
      }
    }
    return noChange(e);
  }

  Tx<ExprPtr> ExprTransform::visit(const ::std::shared_ptr<const ConditionalExpr> &e)
  {
    if (recursive) {
      auto c = transform(e->condition);
      if (c.actual->self<ErrorExpr>()) {
        return newChange(ErrorExpr::of(e->type));
      }
      auto t = transform(e->iftrue);
      auto f = transform(e->iffalse);

      if (t.actual->self<ErrorExpr>() && f.actual->self<ErrorExpr>()) {
        return newChange(t.actual);
      }

      if (c.isNew || t.isNew || f.isNew) {
        return newChange(ConditionalExpr::of(c.actual, t.actual, f.actual));
      }
    }
    return noChange(e);
  }

  Tx<ExprPtr> ExprTransform::visit(const ::std::shared_ptr<const CallExpr> &e)
  {
    if (recursive) {
      auto f = transformFunction(e->target);
      auto args = transformList(e->arguments);
      for (const auto &arg : args.actual) {
        if (arg->self<ErrorExpr>()) {
          return newChange(ErrorExpr::of(e->type));
        }
      }

      if (args.isNew || f.isNew) {
        return newChange(CallExpr::of(f.actual, ::std::move(args.actual)));
      }
    }
    return noChange(e);
  }

  Tx<ExprPtr> ExprTransform::visit(const ::std::shared_ptr<const ConstructExpr> &e)
  {
    if (recursive) {
      auto args = transformList(e->arguments);
      for (auto ex : args.actual) {
        if (ex->self<ErrorExpr>()) {
          return newChange(ErrorExpr::of(e->type));
        }
      }

      if (args.isNew) {
        return newChange(ConstructExpr::of(::std::move(args.actual)));
      }
    }
    return noChange(e);
  }

  Tx<ExprPtr> ExprTransform::visit(const ::std::shared_ptr<const LetExpr> &e)
  {
    if (recursive) {
      auto value = transform(e->value);
      auto result = transform(e->result);
      if (value.isNew || result.isNew) {
        return newChange(
            LetExpr::of(e->variable, ::std::move(value.actual), ::std::move(result.actual)));
      }
    }
    return noChange(e);
  }

  Tx<ExprPtr> ExprTransform::visit(const ::std::shared_ptr<const ListLiteral> &e)
  {
    if (recursive) {
      auto args = transformList(e->entries);
      for (auto ex : args.actual) {
        if (ex->self<ErrorExpr>()) {
          return newChange(ErrorExpr::of(e->type));
        }
      }
      if (args.isNew) {
        return newChange(ListLiteral::of(e->type->self<types::LType>(), ::std::move(args.actual)));
      }
    }
    return noChange(e);
  }

  Tx<ExprPtr> ExprTransform::visit(const ::std::shared_ptr<const TypeExpr> &e)
  {
    if (recursive) {
      auto name = transform(e->name);
      auto arg = transform(e->argument);
      if (name.actual->self<ErrorExpr>() || arg.actual->self<ErrorExpr>()) {
        return newChange(ErrorExpr::of(e->type));
      }
      if (name.isNew || arg.isNew) {
        return newChange(TypeExpr::of(name.actual, ::std::move(arg.actual)));
      }
    }
    return noChange(e);
  }
}
