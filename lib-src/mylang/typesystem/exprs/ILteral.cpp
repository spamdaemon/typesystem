#include <mylang/typesystem/exprs/ILiteral.h>
#include <mylang/typesystem/exprs/ExprVisitor.h>
#include <mylang/typesystem/types/IType.h>

namespace mylang::typesystem::exprs {
  using namespace mylang::typesystem::types;

  ILiteral::ILiteral(Integer v)
      : LiteralExpr(IType::create()), value(::std::move(v))
  {

  }

  ILiteral::~ILiteral()
  {
  }

  ExprPtr ILiteral::of(Integer v)
  {
    return ::std::make_shared<ILiteral>(::std::move(v));
  }

  ExprPtr ILiteral::of(int v)
  {
    return of(Integer(v));
  }

  ExprPtr ILiteral::of(size_t v)
  {
    return of(Integer(v));
  }

  void ILiteral::accept(ExprVisitor &v) const
  {
    v.visit(self<ILiteral>());
  }

}
