#include <mylang/typesystem/exprs/Expr.h>

namespace mylang::typesystem::exprs {

  Expr::Expr(TypePtr t)
      : type(::std::move(t))
  {
  }

  Expr::~Expr()
  {
  }

}
