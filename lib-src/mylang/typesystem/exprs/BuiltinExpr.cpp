#include <mylang/typesystem/exprs/BuiltinExpr.h>
#include <mylang/typesystem/types/BType.h>
#include <mylang/typesystem/types/IType.h>
#include <mylang/typesystem/types/LType.h>
#include <mylang/typesystem/TypeMismatch.h>
#include <mylang/typesystem/exprs/ExprVisitor.h>
#include <stdexcept>
#include <iostream>
#include <cassert>

namespace mylang::typesystem::exprs {

  namespace {
    using namespace mylang::typesystem::types;

    static void expectBoolType(ExprPtr e)
    {
      if (e->type->unifiable<BType>()) {
        return;
      }
      throw ::std::invalid_argument("Expected a boolean type");
    }

    static void expectIntType(ExprPtr e)
    {
      if (e->type->unifiable<IType>()) {
        return;
      }
      throw ::std::invalid_argument("Expected an integer type");
    }

    static void expectListType(ExprPtr e)
    {
      if (e->type->unifiable<LType>()) {
        return;
      }
      throw ::std::invalid_argument("Expected a list type");
    }
  }

  const char* BuiltinExpr::toString(Operator op)
  {
    switch (op) {
    case ADD:
      return "add";
    case ALL:
      return "all";
    case ANY:
      return "any";
    case AT:
      return "at";
    case CONS:
      return "cons";
    case EQ:
      return "eq";
    case LENGTH:
      return "len";
    case LT:
      return "lt";
    case LTE:
      return "lte";
    case NEGATE:
      return "neg";
    case NOT:
      return "not";
    }
    ;
    return nullptr;
  }

  BuiltinExpr::BuiltinExpr(TypePtr t, Operator o, Operands args)
      : Expr(::std::move(t)), op(o), operands(::std::move(args))
  {
    for (const auto &e : operands) {
      if (!e) {
        throw ::std::invalid_argument("Null operand");
      }
    }
    assert(o != CONS || operands.size() == 2);
  }

  BuiltinExpr::~BuiltinExpr()
  {
  }

  ::std::shared_ptr<const BuiltinExpr> BuiltinExpr::of(TypePtr t, Operator o, Operands args)
  {
    return ::std::make_shared<BuiltinExpr>(::std::move(t), o, ::std::move(args));
  }

  ExprPtr BuiltinExpr::negationOf(ExprPtr a)
  {
    expectIntType(a);
    return of(a->type, NEGATE, { a });
  }

  ExprPtr BuiltinExpr::sumOf(ExprPtr a, ExprPtr b)
  {
    expectIntType(a);
    expectIntType(b);
    return of(a->type, ADD, { a, b });
  }

  ExprPtr BuiltinExpr::lengthOf(ExprPtr obj)
  {
    expectListType(obj);
    return of(types::IType::create(), LENGTH, { obj });
  }

  ExprPtr BuiltinExpr::atIndexOf(ExprPtr i, ExprPtr obj)
  {
    expectIntType(i);
    expectListType(obj);
    if (auto ty = obj->type->self<types::LType>(); ty) {
      return of(ty->element, AT, { i, obj });
    }
    throw ::std::invalid_argument("Bad object type");
  }

  ExprPtr BuiltinExpr::consOf(ExprPtr e, ExprPtr list)
  {
    if (auto ty = list->type->self<types::LType>(); ty) {
      if (ty->element->unifiable(e->type)) {
        return of(list->type, CONS, { e, list });

      }
      throw ::std::invalid_argument("Bad element type");
    }
    // we know this will fail!
    expectListType(list);
    return nullptr; // keep compiler happy
  }

  ExprPtr BuiltinExpr::notOf(ExprPtr a)
  {
    expectBoolType(a);
    return of(a->type, NOT, { a });
  }

  ExprPtr BuiltinExpr::allOf(Operands args)
  {
    for (auto arg : args) {
      expectBoolType(arg);
    }
    return of(types::BType::create(), ALL, ::std::move(args));
  }

  ExprPtr BuiltinExpr::anyOf(Operands args)
  {
    for (auto arg : args) {
      expectBoolType(arg);
    }
    return of(types::BType::create(), ANY, args);
  }

  ExprPtr BuiltinExpr::andOf(ExprPtr a, ExprPtr b)
  {
    return allOf( { a, b });
  }

  ExprPtr BuiltinExpr::orOf(ExprPtr a, ExprPtr b)
  {
    return anyOf( { a, b });
  }

  ExprPtr BuiltinExpr::eqOf(ExprPtr a, ExprPtr b)
  {
    return of(types::BType::create(), EQ, { a, b });
  }

  ExprPtr BuiltinExpr::ltOf(ExprPtr a, ExprPtr b)
  {
    return of(types::BType::create(), LT, { a, b });
  }

  ExprPtr BuiltinExpr::lteOf(ExprPtr a, ExprPtr b)
  {
    return of(types::BType::create(), LTE, { a, b });
  }

  ExprPtr BuiltinExpr::gtOf(ExprPtr a, ExprPtr b)
  {
    return ltOf(b, a);
  }

  ExprPtr BuiltinExpr::gteOf(ExprPtr a, ExprPtr b)
  {
    return lteOf(b, a);
  }

  void BuiltinExpr::accept(ExprVisitor &v) const
  {
    v.visit(self<BuiltinExpr>());
  }

  ExprPtr BuiltinExpr::replaceOperands(Operands args) const
  {
    return of(type, op, ::std::move(args));
  }

}
