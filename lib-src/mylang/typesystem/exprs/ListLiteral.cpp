#include <mylang/typesystem/exprs/ListLiteral.h>
#include <mylang/typesystem/exprs/ExprVisitor.h>
#include <mylang/typesystem/types/LType.h>
#include <mylang/typesystem/typesystem.h>

namespace mylang::typesystem::exprs {
  using namespace mylang::typesystem::types;

  ListLiteral::ListLiteral(::std::shared_ptr<const LType> listTy, ::std::vector<ExprPtr> e)
      : LiteralExpr(::std::move(listTy)), entries(::std::move(e))
  {

  }

  ListLiteral::~ListLiteral()
  {
  }

  ExprPtr ListLiteral::of(::std::shared_ptr<const LType> listTy)
  {
    return of(::std::move(listTy), { });
  }

  ExprPtr ListLiteral::of(::std::shared_ptr<const LType> listTy, ::std::vector<ExprPtr> e)
  {
    return ::std::make_shared<ListLiteral>(::std::move(listTy), ::std::move(e));
  }

  void ListLiteral::accept(ExprVisitor &v) const
  {
    v.visit(self<ListLiteral>());
  }

}
