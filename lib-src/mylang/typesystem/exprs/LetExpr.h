#ifndef CLASS_MYLANG_TYPESYSTEM_EXPRS_LETEXPR_H
#define CLASS_MYLANG_TYPESYSTEM_EXPRS_LETEXPR_H

#include <mylang/typesystem/exprs/Expr.h>
#include <mylang/typesystem/VarName.h>
#include <mylang/typesystem/typesystem.h>
#include <functional>
#include <vector>

namespace mylang::typesystem::exprs {

  /**
   * Create a let expression.
   */
  class LetExpr: public Expr
  {
    /**
     * Create a let expression
     */
  public:
    LetExpr(VarName variable, ExprPtr value, ExprPtr result);

    /** Destructor */
  public:
    ~LetExpr();

    /**
     * Get the member of a structure.
     * @param variable the name of the variable
     * @param value the value to be assigned to the variable
     * @param result expression
     * @return a let expression
     */
  public:
    static ExprPtr of(VarName variable, ExprPtr value, ExprPtr result);

    /**
     * Create a let expression using a lambda function.
     * @param value the value
     * @param result the result function
     * @return an expr
     */
  public:
    static ExprPtr of(ExprPtr value, ::std::function<ExprPtr(VarPtr)> result);

    /**
     * Create a let expression using a lambda function.
     * @param value the value
     * @param result the result function
     * @return an expr
     */
  public:
    static ExprPtr of(const ::std::vector<ExprPtr> &values,
        ::std::function<ExprPtr(::std::vector<VarPtr>)> result);

    /**
     * Create a let expression using a lambda function.
     * @param left the left value
     * @param right the right value
     * @param result the result function
     * @return an expr
     */
  public:
    static ExprPtr of(ExprPtr left, ExprPtr right, ::std::function<ExprPtr(VarPtr, VarPtr)> result);

  public:
    void accept(ExprVisitor &v) const override;

    /** The variable bound by this let expression */
  public:
    const VarName variable;

    /** The expression bound to the variable */
  public:
    const ExprPtr value;

    /** The result of this expression */
  public:
    const ExprPtr result;
  };
}
#endif
