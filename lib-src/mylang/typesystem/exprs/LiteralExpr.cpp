#include <mylang/typesystem/exprs/LiteralExpr.h>

namespace mylang::typesystem::exprs {

  LiteralExpr::LiteralExpr(TypePtr t)
      : Expr(::std::move(t))
  {
  }

  LiteralExpr::~LiteralExpr()
  {
  }

}
