#include <mylang/typesystem/FreeVariables.h>
#include <mylang/typesystem/exprs/VarExpr.h>
#include <mylang/typesystem/types/WType.h>
#include <mylang/typesystem/algorithms/FreeVariableFinder.h>

namespace mylang::typesystem {

  FreeVariables::FreeVariables(const ExprPtr &e)
      : FreeVariables(algorithms::FreeVariableFinder::find(e, { }))
  {
  }

  FreeVariables::FreeVariables(const ::std::vector<ExprPtr> &exprs)
  {
    for (const auto &e : exprs) {
      add(algorithms::FreeVariableFinder::find(e, { }));
    }
  }

  void FreeVariables::unbound(const VarPtr &v)
  {
    freeVariables.emplace(v->name, v->type);
  }

  void FreeVariables::unbound(const WTypePtr &t)
  {
    freeTypes.insert(t->name);
  }

  bool FreeVariables::add(const FreeVariables &v)
  {
    size_t vars = freeVariables.size();
    size_t types = freeTypes.size();
    if (&v != this) {
      freeVariables.insert(v.freeVariables.begin(), v.freeVariables.end());
      freeTypes.insert(v.freeTypes.begin(), v.freeTypes.end());
    }
    return vars != freeVariables.size() || types != freeTypes.size();
  }

}
