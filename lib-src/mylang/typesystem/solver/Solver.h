#ifndef MYLANG_TYPESYSTEM_SOLVER_SOLVER_H
#define MYLANG_TYPESYSTEM_SOLVER_SOLVER_H

#ifndef MYLANG_TYPESYSTEM_TYPESYSTEM_H
#include <mylang/typesystem/typesystem.h>
#endif

#ifndef MYLANG_TYPESYSTEM_ENVIRONMENT_H
#include <mylang/typesystem/Environment.h>
#endif

#include <optional>

namespace mylang::typesystem::solver {

  class Solver
  {
    Solver();
    ~Solver();

    /**
     * Determine if the specified expression is satisfiable.
     * @param expr an expression
     * @param env an environment
     * @return new bindings
     */
  public:
    static ::std::optional<Environment::Bindings> eval(ExprPtr expr, const Environment &env);
  };
}
#endif
