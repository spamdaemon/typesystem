#ifndef CLASS_MYLANG_TYPESYSTEM_TYPEDEFINITION_H
#define CLASS_MYLANG_TYPESYSTEM_TYPEDEFINITION_H

#include <mylang/Self.h>
#include <mylang/typesystem/Constructor.h>
#include <mylang/typesystem/Pattern.h>
#include <mylang/typesystem/typesystem.h>
#include <mylang/typesystem/Tx.h>
#include <string>
#include <map>
#include <optional>
#include <memory>

namespace mylang::typesystem {
  class Environment;
  /**
   * A type definition object.
   */
  class TypeDefinition: public Self<TypeDefinition>
  {
    /** A constructor function */
  public:
    using ConstructorPtr = ::std::shared_ptr<const Constructor>;

    /**
     * Create a new type definition,
     * @param defName the name of the definition itself
     * @param typeName the name of the type
     * @param ctor the type constructor
     * @param constraints the constraints that the parameters need to satisfy
     */
  public:
    TypeDefinition(::std::string defName, ConstructorPtr ctor, ExprPtr constraints);

    /** Destructor */
  public:
    ~TypeDefinition();

    /**
     * Create a type constructor.
     *
     * @param typeName the name of the type and the type definition
     * @param t the type parameters
     * @param p the  parameters
     * @param constraints a boolean expression that depends only on the parameters
     * @return an expression that constructs an instance of a type
     */
  public:
    static ::std::shared_ptr<const TypeDefinition> of(::std::string defName,
        ConstructorPtr constructor, ExprPtr constraints = nullptr);

    /**
     * Create a type constructor.
     *
     * @param typeName the name of the type and the type definition
     * @param t the type parameters
     * @param p the  parameters
     * @param constraints a boolean expression that depends only on the parameters
     * @return an expression that constructs an instance of a type
     */
  public:
    static ::std::shared_ptr<const TypeDefinition> of(::std::string defName, ::std::string typeName,
        Function::TypeParameters t, Function::Parameters p = { }, ExprPtr constraints = nullptr);

    /**
     * Create a type constructor.
     *
     * @param typeName the name of the type and the type definition
     * @param p the type parameters
     * @param constraints a boolean expression that depends only on the parameters
     * @return an expression that constructs an instance of a type
     */
  public:
    static ::std::shared_ptr<const TypeDefinition> of(::std::string typeName,
        Function::TypeParameters t, Function::Parameters p = { }, ExprPtr constraints = nullptr);

    /**
     * Create a type constructor.
     *
     * @param typeName the name of the type and the type definition
     * @param t the type parameters
     * @param p the  parameters
     * @param constraints a boolean expression that depends only on the parameters
     * @return an expression that constructs an instance of a type
     */
  public:
    static ::std::shared_ptr<const TypeDefinition> of(::std::string defName, ::std::string typeName,
        Function::Parameters p = { }, ExprPtr constraints = nullptr);

    /**
     * Create a type constructor.
     *
     * @param typeName the name of the type and the type definition
     * @param p the type parameters
     * @param constraints a boolean expression that depends only on the parameters
     * @return an expression that constructs an instance of a type
     */
  public:
    static ::std::shared_ptr<const TypeDefinition> of(::std::string typeName,
        Function::Parameters p = { }, ExprPtr constraints = nullptr);

    /**
     * Get a constructor for this type.
     *
     * Arguments that are not specified, are replaced with variables.
     *
     * @param arguments additional arguments
     * @return a TypeExpr for this type
     */
  public:
    ExprPtr callConstructor(Function::Arguments args = { }) const;

    /**
     * Get the constructor as a pattern.
     *
     * Arguments that are not specified, are replaced with variables.
     *
     * @param args the arguments for the constructor
     * @return the constructor pattern
     */
  public:
    Pattern getConstructorPattern(::std::vector<Pattern> args = { }) const;

    /**
     * Create a type definition that refines this type.
     *
     * The refined type is compatible with this type and both
     * can be used in each others place.
     *
     * @param name the name of this type
     * @param c a new constraint expression
     * @return new type definition
     */
  public:
    ::std::shared_ptr<const TypeDefinition> refine(::std::string name, ExprPtr c) const;

    /** The constructor function */
  public:
    const ConstructorPtr constructor;

    /** The name of this type */
  public:
    const ::std::string name;

    /** The constraints and parameters for this type */
  public:
    const ExprPtr constraints;
  };
}
#endif
