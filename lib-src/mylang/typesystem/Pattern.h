#ifndef CLASS_MYLANG_TYPESYSTEM_PATTERN_H
#define CLASS_MYLANG_TYPESYSTEM_PATTERN_H

#include <map>
#include <mylang/typesystem/typesystem.h>
#include <mylang/typesystem/FreeVariables.h>

namespace mylang::typesystem {

  /**
   * A pattern.
   *
   * A pattern is an expression that consists entirely of variables,
   * literals, or constructors-like expressions.
   *
   * @see predicates::IsPattern
   */
  class Pattern
  {
    /**
     * Create a pattern.
     * @param expr an expression
     * @throws ::std::invalid_argument if the expression is not a pattern.
     */
  public:
    explicit Pattern(ExprPtr expr);

    /** Default move constructor */
  public:
    Pattern(Pattern &&that) = default;

    /** Default move constructor */
  public:
    Pattern(const Pattern &that) = default;

    /** Destructor */
  public:
    ~Pattern();

    /**
     * Access the pattern as a pointer
     */
  public:
    inline const exprs::Expr* operator->() const
    {
      return expression.get();
    }

    /**
     * Check if two patterns match.
     */
  public:
    bool matches(const Pattern &that) const;

    /** The expression that is the pattern. */
  public:
    ExprPtr expression;

    /** The free variables in this pattern */
  public:
    FreeVariables freeVariables;
  };
}
#endif
