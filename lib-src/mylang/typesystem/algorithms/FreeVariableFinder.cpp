#include <mylang/typesystem/algorithms/FreeVariableFinder.h>
#include <mylang/typesystem/FreeVariables.h>
#include <mylang/typesystem/exprs/ExprTransform.h>
#include <mylang/typesystem/exprs/BLiteral.h>
#include <mylang/typesystem/exprs/ConstructExpr.h>
#include <mylang/typesystem/exprs/ErrorExpr.h>
#include <mylang/typesystem/exprs/ILiteral.h>
#include <mylang/typesystem/exprs/ListLiteral.h>
#include <mylang/typesystem/exprs/TLiteral.h>
#include <mylang/typesystem/exprs/TypeExpr.h>
#include <mylang/typesystem/types/WType.h>
#include <mylang/typesystem/typesystem.h>
#include <memory>
#include <map>
#include <set>

namespace mylang::typesystem::algorithms {
  using namespace exprs;

  FreeVariables FreeVariableFinder::find(ExprPtr e, const Environment::Bindings &bindings)
  {
    struct V: public ExprTransform
    {
      V(const Environment::Bindings &xbindings)
          : bindings(xbindings)
      {
      }

      ~V()
      {
      }

      Tx<ExprPtr> transform(ExprPtr e) override
      {
        if (auto t = e->type->self<types::WType>(); t) {
          freeTypes.insert(t->name);
        }
        return ExprTransform::transform(e);
      }

      Tx<FunctionPtr> transformFunction(const FunctionPtr &f) override
      {
        ExprTransform::transformFunction(f);
        for (auto p : f->parameters) {
          freeVariables.erase(p->name);
        }
        return Tx<FunctionPtr>::txOld(f);
      }

      Tx<ExprPtr> visit(const ::std::shared_ptr<const CallExpr> &e) override
      {
        transformList(e->arguments);
        transformFunction(e->target);
        return visitDefault(e);
      }

      Tx<ExprPtr> visit(const ::std::shared_ptr<const LetExpr> &e) override
      {
        transform(e->value);
        transform(e->result);
        freeVariables.erase(e->variable);
        return visitDefault(e);
      }

      Tx<ExprPtr> visit(const ::std::shared_ptr<const VarExpr> &e) override
      {
        if (bindings.find(e->name) == nullptr) {
          freeVariables.emplace(e->name, e->type);
        }
        return visitDefault(e);
      }

      const Environment::Bindings &bindings;

      /** The unbound variables and their types */
    public:
      ::std::map<VarName, TypePtr> freeVariables;

      /** The type variables */
    public:
      ::std::set<VarName> freeTypes;
    };

    V v(bindings);
    v.transform(e);

    FreeVariables res;
    res.freeVariables = ::std::move(v.freeVariables);
    res.freeTypes = ::std::move(v.freeTypes);
    return res;
  }

}
