#ifndef CLASS_MYLANG_TYPESYSTEM_ALGORITHMS_FRESHEN_H
#define CLASS_MYLANG_TYPESYSTEM_ALGORITHMS_FRESHEN_H

#include <mylang/typesystem/typesystem.h>
#include <mylang/typesystem/Environment.h>
#include <mylang/typesystem/RBTree.h>
#include <mylang/typesystem/VarName.h>
#include <mylang/typesystem/Tx.h>
#include <utility>

namespace mylang::typesystem::algorithms {

  /**
   * Replace any local variables in the expression with new local variables.
   * This function guarantees uniqueness of all variables that are introduced
   * via let bindings in the entire expression tree.
   *
   * Shadowed variables are explicitly supported:
   * <code>
   * let a=1 in
   * let a=2 in a
   * </code>
   * is rewritten to something like
   * <code>
   * let fresh1=1 in
   * let fresh2=2 in fresh2
   * </code>
   */
  struct Freshen
  {

    /**
     * Freshen all variables in the expression.
     *
     * @param e an expression
     * @param initial an existing bindings
     * @return a the freshened expression
     */
    static Tx<ExprPtr> apply(ExprPtr e, const Environment &initial = Environment());
    /**
     * Freshen all variables in the expression.
     *
     * @param e an expression
     * @param freshenFreeVariables  if true, then freshen any free variables, false otherwise.
     * @param initial an existing bindings
     * @return a the freshened expression and an updated mapping.
     */
    static Tx<::std::pair<ExprPtr, Environment>> apply(ExprPtr e, bool freshenFreeVariables,
        const Environment &initial = Environment());
  };

}

#endif
