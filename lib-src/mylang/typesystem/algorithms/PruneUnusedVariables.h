#ifndef CLASS_MYLANG_TYPESYSTEM_ALGORITHMS_PRUNEUNUSEDVARIABLES_H
#define CLASS_MYLANG_TYPESYSTEM_ALGORITHMS_PRUNEUNUSEDVARIABLES_H

#include <mylang/typesystem/typesystem.h>
#include <mylang/typesystem/Environment.h>
#include <mylang/typesystem/exprs/ExprTransform.h>

namespace mylang::typesystem::algorithms {

  /**
   * Find variables in an expression that are not bound in the environment.
   */
  struct PruneUnusedVariables
  {
    static Tx<ExprPtr> prune(ExprPtr e);
  };

}

#endif
