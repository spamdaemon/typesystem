#include <mylang/typesystem/algorithms/Unify.h>
#include <mylang/typesystem/predicates/IsConstant.h>
#include <mylang/typesystem/predicates/HasFreeVariables.h>
#include <mylang/typesystem/eval/Eval.h>
#include <mylang/typesystem/exprs/TransformVisitor.h>
#include <mylang/typesystem/exprs/BLiteral.h>
#include <mylang/typesystem/exprs/BuiltinExpr.h>
#include <mylang/typesystem/exprs/ConstructExpr.h>
#include <mylang/typesystem/exprs/ErrorExpr.h>
#include <mylang/typesystem/exprs/ILiteral.h>
#include <mylang/typesystem/exprs/ListLiteral.h>
#include <mylang/typesystem/exprs/TLiteral.h>
#include <mylang/typesystem/exprs/TypeExpr.h>
#include <mylang/typesystem/typesystem.h>
#include <mylang/typesystem/TypeDefinition.h>
#include <mylang/typesystem/types/types.h>
#include <mylang/typesystem/types/TypeVisitor.h>
#include <mylang/typesystem/visitors/Printer.h>
#include <memory>
#include <iostream>

namespace mylang::typesystem::algorithms {
  using namespace exprs;

  Unify::Status Unify::updateStatus(Unify::Status cur, Unify::Status newStatus)
  {
    if (cur == Unify::FAILED || newStatus == Unify::FAILED) {
      return Unify::FAILED;
    }
    if (cur == Unify::BINDINGS || newStatus == Unify::BINDINGS) {
      return Unify::BINDINGS;
    }
    assert(cur == Unify::UNIFIED && newStatus == Unify::UNIFIED);
    return Unify::UNIFIED;
  }

  Unify::Status Unify::updateResult(Unify::Result &cur, const Unify::Result &result)
  {
    cur.first = result.first;
    return updateStatus(cur.second, result.second);
  }

  Unify::Result Unify::apply(const Pattern &left, const Pattern &right, const Bindings &env)
  {
    struct V: public TransformVisitor<Result>
    {
      V(const ExprPtr &xleft, const ExprPtr &xright, Bindings xenv)
          : left(xleft), right(xright), env(::std::move(xenv))
      {
      }

      ~V()
      {
      }
      const ExprPtr &left;
      const ExprPtr &right;
      Bindings env;

      static Unify::Result unify(ExprPtr left, ExprPtr right, const Bindings &env)
      {
        // if the two expressions are identical, then they are unifiable (unless they are errors)
        if (left == right) {
          return {env, left->self<ErrorExpr>() ? FAILED : UNIFIED};
        }

        // we need to unify the types and get rid of WType instances
        Unify::Result res = Unify::apply(left->type, right->type, env);
        if (res.second == FAILED) {
          return res;
        }

        return V(left, right, res.first).transform(left);
      }

      static Bindings updateBindings(const Bindings &env, VarName n, ExprPtr v)
      {
        return {env.vars.insert_or_assign(n, v),env.typeVars};
      }

      static Environment::Bindings::DataPtr lookupBinding(const Bindings &env, const VarName &n)
      {
        return env.vars.find(n);
      }

      Unify::Result visitDefault(const ExprPtr&) override
      {
        if (auto r = right->self<LetExpr>(); r) {
          return unify(r, left, env);
        }

        if (auto v = left->self<VarExpr>(); v) {
          auto value = lookupBinding(env, v->name);
          if (value) {
            return unify(value->second, right, env);
          } else {
            return {updateBindings(env,v->name,right), BINDINGS};
          }
        }
        if (auto v = right->self<VarExpr>(); v) {
          auto value = lookupBinding(env, v->name);
          if (value) {
            return unify(value->second, left, env);
          } else {
            return {updateBindings(env,v->name,left), BINDINGS};
          }
        }

        // cannot unify
        return {env,FAILED};
      }

      Unify::Result visit(const ::std::shared_ptr<const ErrorExpr>&) override
      {
        return {env,FAILED};
      }

      Unify::Result visit(const ::std::shared_ptr<const BLiteral> &e) override
      {
        auto that = right->self<BLiteral>();
        if (!that) {
          return visitDefault(e);
        }

        return {env,that && that->value==e->value ? UNIFIED : FAILED};
      }
      Unify::Result visit(const ::std::shared_ptr<const ILiteral> &e) override
      {
        auto that = right->self<ILiteral>();
        if (!that) {
          return visitDefault(e);
        }
        return {env,that && that->value==e->value ? UNIFIED : FAILED};
      }
      Unify::Result visit(const ::std::shared_ptr<const TLiteral> &e) override
      {
        auto that = right->self<TLiteral>();
        if (!that) {
          return visitDefault(e);
        }
        return {env,that && that->value==e->value ? UNIFIED : FAILED};
      }

      Unify::Result visit(const ::std::shared_ptr<const ConstructExpr> &e) override
      {
        auto that = right->self<ConstructExpr>();
        if (!that) {
          return visitDefault(e);
        }

        // we've already checked the type, so no worries about
        if (e->arguments.size() != that->arguments.size()) {
          return {env,FAILED};
        }

        Bindings newEnv = env;
        Status unified = UNIFIED;
        for (size_t i = 0; i < e->arguments.size(); ++i) {
          auto r = unify(e->arguments.at(i), that->arguments.at(i), newEnv);
          unified = updateStatus(unified, r.second);
          newEnv = ::std::move(r.first);
        }
        return {newEnv,unified};
      }

      Unify::Result visit(const ::std::shared_ptr<const LetExpr> &e) override
      {
        // at this point, the local variable is a globally unique variable
        Bindings newEnv = updateBindings(env, e->variable, e->value);
        return unify(e->result, right, newEnv);
      }

      Unify::Result visit(const ::std::shared_ptr<const ListLiteral> &e) override
      {
        auto that = right->self<ListLiteral>();
        if (!that) {
          return visitDefault(e);
        }
        if (that->entries.size() != e->entries.size()) {
          return {env, FAILED};
        }
        Bindings newEnv(env);
        Status unified = UNIFIED;
        for (size_t i = 0; i < e->entries.size(); ++i) {
          auto r = unify(e->entries[i], that->entries[i], newEnv);
          unified = updateStatus(unified, r.second);
          newEnv = ::std::move(r.first);
        }
        return {newEnv,unified};
      }

      Unify::Result visit(const ::std::shared_ptr<const TypeExpr> &e) override
      {
        auto that = right->self<TypeExpr>();
        if (!that) {
          return visitDefault(e);
        }

        // unify the names
        auto [newEnv, unified] = unify(e->name, that->name, env);

        auto r = unify(e->argument, that->argument, newEnv);
        unified = updateStatus(unified, r.second);
        newEnv = ::std::move(r.first);

        return {newEnv,unified};
      }

      Unify::Result visit(const ::std::shared_ptr<const VarExpr> &e) override
      {
        auto that = right->self<VarExpr>();
        if (!that) {
          return visitDefault(e);
        }
        if (that->name == e->name) {
          // look like it may be a free variable
          return {env,UNIFIED};
        }

        auto leftValue = lookupBinding(env, e->name);
        auto rightValue = lookupBinding(env, that->name);

        // if both variables are bound, then we unify them both
        if (leftValue && rightValue) {
          return unify(leftValue->second, rightValue->second, env);
        }
        if (leftValue) { // but not right value
          return {updateBindings(env,that->name,leftValue->second),BINDINGS};
        }
        if (rightValue) { // but not left value
          return {updateBindings(env,e->name,rightValue->second),BINDINGS};
        }

        // create a fresh variable and map the expression to that new variable
        auto fresh = VarExpr::of(e->type, VarName());
        return {
          updateBindings(updateBindings(env,that->name,fresh),e->name,fresh),
          BINDINGS
        };
      }

    };

#if 0
    ::std::cerr << "UNIFY " << ::std::endl << " L: ";
    visitors::Printer::print(::std::cerr, left.expression) << ::std::endl << " R: ";
    visitors::Printer::print(::std::cerr, right.expression) << ::std::endl;
#endif
    return V::unify(left.expression, right.expression, env);
  }

  Unify::Result Unify::apply(const TypePtr &left, const TypePtr &right, const Bindings &binds)
  {
    using namespace types;

    struct V: types::TypeVisitor
    {
      V(TypePtr r, const Bindings &b)
          : right(r), binds(b), result { b, FAILED }
      {
      }
      TypePtr right;
      const Bindings &binds;
      Result result;

      static Unify::Result unify(const TypePtr &left, const TypePtr &right, const Bindings &binds)
      {
        // since unification is symmetric, we can just flip left and right,
        // if right is also a WType
        if (left->self<WType>() == nullptr && right->self<WType>() != nullptr) {
          return unify(right, left, binds);
        }

        V v(right, binds);
        left->accept(v);
        return v.result;
      }

      static Environment::TypeBindings::DataPtr lookupTypeBinding(const Bindings &env,
          const VarName &n)
      {
        return env.typeVars.find(n);
      }

      static Bindings updateTypeBindings(const Bindings &env, VarName n, TypePtr v)
      {
        return {env.vars,env.typeVars.insert_or_assign(n, v)};
      }

      void visit(const ::std::shared_ptr<const BType>&) override final
      {
        if (right->self<BType>()) {
          result = { binds, UNIFIED };
        }
      }
      void visit(const ::std::shared_ptr<const IType>&) override final
      {
        if (right->self<IType>()) {
          result = { binds, UNIFIED };
        }
      }
      void visit(const ::std::shared_ptr<const LType> &left) override final
      {
        if (auto r = right->self<LType>(); r) {
          result = unify(left->element, r->element, binds);
        }
      }

      void visit(const ::std::shared_ptr<const SType> &left) override final
      {
        if (auto r = right->self<SType>(); r) {
          if (left->members.size() != r->members.size()) {
            return;
          }
          SType::Members m;
          m.reserve(left->members.size());
          result = { binds, UNIFIED };
          for (size_t i = 0; i < left->members.size() && result.second != FAILED; ++i) {
            auto tmp = unify(left->members.at(i), r->members.at(i), result.first);
            result = { tmp.first, updateStatus(result.second, tmp.second) };
          }
        }
      }
      void visit(const ::std::shared_ptr<const TType>&) override final
      {
        if (right->self<TType>()) {
          result = { binds, UNIFIED };
        }
      }
      void visit(const ::std::shared_ptr<const WType> &left) override final
      {
        if (auto r = right->self<WType>(); r) {
          if (left->name == r->name) {
            result = { binds, UNIFIED };
            return;
          }
          auto leftType = lookupTypeBinding(binds, left->name);
          auto rightType = lookupTypeBinding(binds, r->name);

          // if both variables are bound, then we unify them both
          if (leftType && rightType) {
            result = unify(leftType->second, rightType->second, binds);
            return;
          }
          if (leftType) { // but not right value
            result = { updateTypeBindings(binds, r->name, leftType->second), BINDINGS };
            return;
          }
          if (rightType) { // but not left value
            result = { updateTypeBindings(binds, left->name, rightType->second), BINDINGS };
            return;
          }

          // create a fresh variable and map the expression to that new variable
          auto fresh = WType::create();
          result = { updateTypeBindings(updateTypeBindings(binds, r->name, fresh), left->name,
              fresh), BINDINGS };
          return;
        } else {
          auto leftType = lookupTypeBinding(binds, left->name);
          if (leftType == nullptr) {
            result = { updateTypeBindings(binds, left->name, right), BINDINGS };
          } else {
            result = unify(leftType->second, right, binds);
          }
        }
      }
      void visit(const ::std::shared_ptr<const XType> &left) override final
      {
        if (auto r = right->self<XType>(); r) {
          result = unify(left->arguments, r->arguments, binds);
        }
      }
    };

    Result res = { binds, BINDINGS };
    while (res.second == BINDINGS) {
      res = V::unify(left, right, res.first);
    }
    return res;

  }

}
