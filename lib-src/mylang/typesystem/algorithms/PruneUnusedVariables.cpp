#include <mylang/typesystem/algorithms/PruneUnusedVariables.h>
#include <mylang/typesystem/exprs/ExprTransform.h>
#include <mylang/typesystem/exprs/exprs.h>
#include <mylang/typesystem/typesystem.h>
#include <set>

namespace mylang::typesystem::algorithms {
  using namespace exprs;

  Tx<ExprPtr> PruneUnusedVariables::prune(ExprPtr e)
  {
    struct V: public ExprTransform
    {
      V()
      {
      }

      ~V()
      {
      }

      Tx<ExprPtr> visit(const ::std::shared_ptr<const CallExpr> &e) override
      {
        auto args = transformList(e->arguments);
        auto f = transformFunction(e->target);
        if (args.isNew || f.isNew) {
          return newChange(CallExpr::of(f.actual, ::std::move(args.actual)));
        }
        return noChange(e);
      }

      Tx<ExprPtr> visit(const ::std::shared_ptr<const LetExpr> &e) override
      {
        // transform the result
        auto result = transform(e->result);
        if (used.count(e->variable) == 0) {
          // the variable isn't used, so just return the result
          return newChange(result.actual);
        }
        auto value = transform(e->value);
        if (result.isNew || value.isNew) {
          return newChange(
              LetExpr::of(e->variable, ::std::move(value.actual), ::std::move(result.actual)));
        }
        return noChange(e);
      }

      Tx<ExprPtr> visit(const ::std::shared_ptr<const VarExpr> &e) override
      {
        used.insert(e->name);
        return noChange(e);
      }

      ::std::set<VarName> used;
    };

    V v;
    return v.transform(e);
  }

}
