#ifndef CLASS_MYLANG_TYPESYSTEM_ALGORITHMS_UNIFY_H
#define CLASS_MYLANG_TYPESYSTEM_ALGORITHMS_UNIFY_H

#include <mylang/typesystem/typesystem.h>
#include <mylang/typesystem/Environment.h>
#include <mylang/typesystem/Pattern.h>
#include <optional>
#include <utility>

namespace mylang::typesystem::algorithms {

  /**
   *  Unify two expression patterns.
   */
  class Unify
  {
  public:
    enum Status
    {
      /** Cannot unify */
      FAILED = 0,

      /** Not unified, but the environment has changed */
      BINDINGS = 1,

      /** Unified */
      UNIFIED = 2,
    };

  public:
    struct Bindings
    {
      /** The variables */
      Environment::Bindings vars;
      /** The type variables */
      Environment::TypeBindings typeVars;
    };

    /**
     * Create a unification function.
     */
  private:
    Unify() = delete;

    /**
     * The status of an attempt to unify
     *
     * If unification was successful, then the boolean value will be true
     * and the environment will be setup such evaluation of an expression
     * yields a non-error constant. If the environment still contains variables,
     * then those variables are free variables and can be assigned any value.
     *
     * If unification was unsuccessful, then the boolean value will be false,
     * and the environment will the contain the most resolved variant.
     */
  public:
    using Result = ::std::pair<Bindings, Status>;

    /**
     * Incorporate a new result into an existing result.
     * @param curResult an existing result
     * @param newStatus the new result to incorporate
     * @return a status
     */
    static Unify::Status updateResult(Result &curResult, const Result &newResult);

    /**
     * Incorporate a new status
     * @param cur the current status
     * @param newStatus the new status to incorporate
     * @return a status
     */
    static Unify::Status updateStatus(Unify::Status cur, Unify::Status newStatus);

    /**
     * Unify two expression subject to some environment.
     *
     * The expressions must be patterns, otherwise, they won't unify.
     *
     * @param left an expression
     * @param right an expression
     * @param env an initial environment
     * @return the result of unification
     */
  public:
    static Result apply(const Pattern &left, const Pattern &right, const Bindings &env);

    /**
     * Unify two types.
     * @param left a left type
     * @param right a right type
     * @param bindings the type bindings
     * @return a result of failure
     */
  public:
    static Result apply(const TypePtr &left, const TypePtr &right, const Bindings &bindings);
  };

}

#endif
