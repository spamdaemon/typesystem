#include <mylang/typesystem/algorithms/Freshen.h>
#include <mylang/typesystem/exprs/ExprTransform.h>
#include <mylang/typesystem/exprs/LetExpr.h>
#include <mylang/typesystem/exprs/VarExpr.h>
#include <mylang/typesystem/types/TypeVisitor.h>
#include <mylang/typesystem/RBTree.h>
#include <mylang/typesystem/Tx.h>
#include <iostream>

namespace mylang::typesystem::algorithms {
  using namespace exprs;
  using FreshenResult = ::std::pair<ExprPtr, Environment>;

  static Tx<TypePtr> freshenType(TypePtr e, Environment &global)
  {
    using namespace types;

    struct V: types::TypeVisitor
    {
      V(Environment &xglobal)
          : global(xglobal)
      {
      }
      Environment &global;
      TypePtr res;

      Tx<TypePtr> visit(const TypePtr t)
      {
        res = nullptr;
        t->accept(*this);
        return Tx<TypePtr>(res, res != t);
      }

      void visit(const ::std::shared_ptr<const BType> &t) override final
      {
        res = t;
      }
      void visit(const ::std::shared_ptr<const IType> &t) override final
      {
        res = t;
      }
      void visit(const ::std::shared_ptr<const TType> &t) override final
      {
        res = t;
      }
      void visit(const ::std::shared_ptr<const WType> &t) override final
      {
        if (auto i = global.typeBindings.find(t->name); i) {
          res = i->second;
          return;
        }
        res = WType::create();
        global.updateBinding(t->name, res);
      }

      void visit(const ::std::shared_ptr<const SType> &t) override final
      {
        SType::Members m;
        bool isNew = false;
        m.reserve(t->members.size());
        for (const auto &ty : t->members) {
          auto tmp = visit(ty);
          if (tmp.isNew) {
            isNew = true;
          }
          m.push_back(tmp.actual);
        }

        if (isNew) {
          res = SType::create(::std::move(m));
        } else {
          res = t;
        }
      }
      void visit(const ::std::shared_ptr<const LType> &t) override final
      {
        auto elemTy = visit(t->element);
        if (elemTy.isNew) {
          res = LType::create(elemTy.actual);
        } else {
          res = t;
        }
      }
      void visit(const ::std::shared_ptr<const XType> &t) override final
      {
        auto argTy = visit(t->arguments);
        if (argTy.isNew) {
          res = XType::create(argTy.actual);
        } else {
          res = t;
        }
      }
    };
    V v(global);

    return v.visit(e);
  }

  Tx<ExprPtr> Freshen::apply(ExprPtr e, const Environment &initial)
  {
    auto res = apply(e, false, initial);
    if (res.isNew) {
      return Tx<ExprPtr>::txNew(res.actual.first);
    } else {
      return Tx<ExprPtr>::txOld(res.actual.first);
    }
  }

  Tx<FreshenResult> Freshen::apply(ExprPtr e, bool freshenFreeVars, const Environment &initial)
  {
    struct V: public ExprTransform
    {
      V(bool freshenAll, Environment &reboundVars, const Environment &locals)
          : freshenFreeVars(freshenAll), rebound(reboundVars), locallyRebound(locals)
      {
      }

      ~V()
      {
      }

      Tx<TypePtr> freshen(TypePtr e) const
      {
        return freshenType(e, rebound);
      }

      Tx<ExprPtr> visit(const ::std::shared_ptr<const BuiltinExpr> &e) override final
      {
        auto ty = freshen(e->type);
        auto args = transformList(e->operands);
        for (auto ex : args.actual) {
          if (ex->self<ErrorExpr>()) {
            return newChange(ErrorExpr::of(e->type));
          }
        }
        if (args.isNew || ty.isNew) {
          return newChange(BuiltinExpr::of(ty.actual, e->op, ::std::move(args.actual)));
        } else {
          return noChange(e);
        }
      }

      Tx<ExprPtr> visit(const ::std::shared_ptr<const CallExpr> &e)
      {
        auto ty = freshen(e->type);
        auto args = transformList(e->arguments);
        for (const auto &arg : args.actual) {
          if (arg->self<ErrorExpr>()) {
            return newChange(ErrorExpr::of(e->type));
          }
        }

        if (args.isNew || ty.isNew) {
          return newChange(CallExpr::of(e->target, ::std::move(args.actual)));
        }
        return noChange(e);
      }

      Tx<ExprPtr> visit(const ::std::shared_ptr<const ListLiteral> &e)
      {
        auto ty = freshen(e->type->self<types::LType>()->element);
        auto args = transformList(e->entries);
        for (auto ex : args.actual) {
          if (ex->self<ErrorExpr>()) {
            return newChange(ErrorExpr::of(e->type));
          }
        }
        if (args.isNew || ty.isNew) {
          return newChange(
              ListLiteral::of(types::LType::create(ty.actual), ::std::move(args.actual)));
        }
        return noChange(e);
      }

      Tx<ExprPtr> visit(const ::std::shared_ptr<const LetExpr> &e) override
      {
        auto value = transform(e->value).actual;
        auto newVar = VarExpr::of(value->type);
        // we are able to deal with shadowed variables
        Environment newEnv(locallyRebound);
        newEnv.bindings = newEnv.bindings.insert_or_assign(e->variable, newVar);
        V v(freshenFreeVars, rebound, newEnv);
        auto res = v.transform(e->result).actual;

        return newChange(LetExpr::of(newVar->name, value, res));
      }

      Tx<ExprPtr> visit(const ::std::shared_ptr<const VarExpr> &e) override
      {
        if (auto i = locallyRebound.bindings.find(e->name); i) {
          return newChange(i->second);
        }

        if (auto i = rebound.bindings.find(e->name); i) {
          return newChange(i->second);
        }

        if (freshenFreeVars) {
          auto ty = freshen(e->type);
          auto newVar = VarExpr::of(ty.actual, e->name.freshen());
          rebound.bindings = rebound.bindings.insert_or_assign(e->name, newVar);
          return newChange(newVar);
        }
        return noChange(e);
      }
      const bool freshenFreeVars;
      Environment &rebound;
      const Environment &locallyRebound;
    };

    Environment globallyRebound = initial;
    Environment local;
    V v(freshenFreeVars, globallyRebound, local);
    auto res = v.transform(e);
    if (res.isNew) {
      return Tx<FreshenResult>::txNew( { res.actual, v.rebound });
    } else {
      return Tx<FreshenResult>::txOld( { res.actual, v.rebound });
    }
  }

}
