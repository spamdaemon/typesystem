#ifndef CLASS_MYLANG_TYPESYSTEM_ALGORITHMS_FREEVARIABLES_H
#define CLASS_MYLANG_TYPESYSTEM_ALGORITHMS_FREEVARIABLES_H

#include <mylang/typesystem/typesystem.h>
#include <mylang/typesystem/FreeVariables.h>
#include <mylang/typesystem/Environment.h>

namespace mylang::typesystem::algorithms {

  /**
   * Find variables in an expression that are not bound in the environment.
   */
  struct FreeVariableFinder
  {
    static FreeVariables find(ExprPtr e, const Environment::Bindings &bindings = { });
  };

}

#endif
