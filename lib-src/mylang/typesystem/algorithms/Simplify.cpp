#include <mylang/typesystem/algorithms/Simplify.h>
#include <mylang/typesystem/exprs/ExprTransform.h>
#include <mylang/typesystem/exprs/exprs.h>
#include <mylang/typesystem/typesystem.h>
#include <set>

namespace mylang::typesystem::algorithms {
  using namespace exprs;

  Tx<ExprPtr> Simplify::simplify(ExprPtr e)
  {
    struct V: public ExprTransform
    {
      V()
      {
      }

      ~V()
      {
      }

      ExprPtr simplifyAdd(BuiltinExpr::Operands&)
      {
        return nullptr;
      }
      ExprPtr simplifyAll(BuiltinExpr::Operands &args)
      {
        if (args.empty()) {
          return BLiteral::of(true);
        }
        BuiltinExpr::Operands newArgs;
        bool changed = false;
        for (auto arg : args) {
          if (auto b = arg->self<BLiteral>(); b) {
            if (b->value) {
              continue; // just drop a literal true since it contributes nothing
            } else {
              return b;
            }
          }
          if (auto b = arg->self<BuiltinExpr>(); b) {
            if (b->op == BuiltinExpr::Operator::ALL) {
              newArgs.insert(newArgs.end(), b->operands.begin(), b->operands.end());
              changed = true;
            } else {
              newArgs.push_back(arg);
            }
          } else {
            newArgs.push_back(arg);
          }
        }

        if (newArgs.size() == 1) {
          return newArgs.at(0);
        }
        if (changed) {
          return BuiltinExpr::allOf(::std::move(newArgs));
        }
        return nullptr;
      }

      ExprPtr simplifyAny(BuiltinExpr::Operands &args)
      {
        if (args.empty()) {
          return BLiteral::of(false);
        }
        BuiltinExpr::Operands newArgs;
        bool changed = false;
        for (auto arg : args) {
          if (auto b = arg->self<BLiteral>(); b) {
            if (b->value) {
              return b;
            } else {
              continue; // false does not contribute to the result
            }
          }
          if (auto b = arg->self<BuiltinExpr>(); b) {
            if (b->op == BuiltinExpr::Operator::ANY) {
              newArgs.insert(newArgs.end(), b->operands.begin(), b->operands.end());
              changed = true;
            } else {
              newArgs.push_back(arg);
            }
          } else {
            newArgs.push_back(arg);
          }
        }

        if (newArgs.size() == 1) {
          return newArgs.at(0);
        }
        if (changed) {
          return BuiltinExpr::anyOf(::std::move(newArgs));
        }
        return nullptr;
      }
      ExprPtr simplifyAt(BuiltinExpr::Operands&)
      {
        return nullptr;
      }
      ExprPtr simplifyCons(BuiltinExpr::Operands&)
      {
        return nullptr;
      }
      ExprPtr simplifyEq(BuiltinExpr::Operands&)
      {
        return nullptr;
      }
      ExprPtr simplifyLength(BuiltinExpr::Operands&)
      {
        return nullptr;
      }
      ExprPtr simplifyLt(BuiltinExpr::Operands&)
      {
        return nullptr;
      }
      ExprPtr simplifyLte(BuiltinExpr::Operands&)
      {
        return nullptr;
      }
      ExprPtr simplifyNegate(BuiltinExpr::Operands &args)
      {
        if (auto b = args.at(0)->self<BuiltinExpr>(); b && b->op == BuiltinExpr::Operator::NEGATE) {
          return b->operands.at(0);
        }
        return nullptr;
      }
      ExprPtr simplifyNot(BuiltinExpr::Operands &args)
      {
        if (auto b = args.at(0)->self<BuiltinExpr>(); b) {
          if (b->op == BuiltinExpr::Operator::NOT) {
            return b->operands.at(0);
          }
        }
        return nullptr;
      }

      Tx<ExprPtr> visit(const ::std::shared_ptr<const BuiltinExpr> &e) override
      {
        BuiltinExpr::Operands ops;
        ops.reserve(e->operands.size());
        bool isNew = false;
        for (auto op : e->operands) {
          auto tmp = transform(op);
          if (tmp.isNew) {
            isNew = true;
          }
          ops.push_back(tmp.actual);
        }
        ExprPtr simplified;
        switch (e->op) {
        case BuiltinExpr::Operator::ADD:
          simplified = simplifyAdd(ops);
          break;
        case BuiltinExpr::Operator::ALL:
          simplified = simplifyAll(ops);
          break;
        case BuiltinExpr::Operator::ANY:
          simplified = simplifyAny(ops);
          break;
        case BuiltinExpr::Operator::AT:
          simplified = simplifyAt(ops);
          break;
        case BuiltinExpr::Operator::CONS:
          simplified = simplifyCons(ops);
          break;
        case BuiltinExpr::Operator::EQ:
          simplified = simplifyEq(ops);
          break;
        case BuiltinExpr::Operator::LENGTH:
          simplified = simplifyLength(ops);
          break;
        case BuiltinExpr::Operator::LT:
          simplified = simplifyLt(ops);
          break;
        case BuiltinExpr::Operator::LTE:
          simplified = simplifyLt(ops);
          break;
        case BuiltinExpr::Operator::NEGATE:
          simplified = simplifyNegate(ops);
          break;
        case BuiltinExpr::Operator::NOT:
          simplified = simplifyNot(ops);
          break;

        }
        if (simplified) {
          return newChange(simplified);
        } else if (isNew) {
          assert(ops.size() == e->operands.size());
          return newChange(BuiltinExpr::of(e->type, e->op, ::std::move(ops)));
        } else {
          return noChange(e);
        }
      }
    };

    V v;
    return v.transform(e);
  }

}
