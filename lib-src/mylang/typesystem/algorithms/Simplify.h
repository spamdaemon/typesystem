#ifndef CLASS_MYLANG_TYPESYSTEM_ALGORITHMS_SIMPLIFY_H
#define CLASS_MYLANG_TYPESYSTEM_ALGORITHMS_SIMPLIFY_H

#include <mylang/typesystem/typesystem.h>
#include <mylang/typesystem/Environment.h>
#include <mylang/typesystem/exprs/ExprTransform.h>

namespace mylang::typesystem::algorithms {

  /**
   * Simplify an expression.
   */
  struct Simplify
  {
    static Tx<ExprPtr> simplify(ExprPtr e);
  };

}

#endif
