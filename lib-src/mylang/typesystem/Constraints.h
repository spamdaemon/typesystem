#ifndef CLASS_MYLANG_TYPESYSTEM_CONSTRAINTS_H
#define CLASS_MYLANG_TYPESYSTEM_CONSTRAINTS_H

#include <mylang/typesystem/typesystem.h>
#include <string>
#include <map>
#include <memory>
#include <optional>

namespace mylang::typesystem {
  class Environment;

  /**
   * A constraints object defines both the parameters and constraints on a type.
   */
  class Constraints
  {
    /** The parameters of the type */
  public:
    using Parameters = ::std::map<::std::string,VarPtr>;

    /** The arguments for generating the type */
  public:
    using Arguments = ::std::map<::std::string,ExprPtr>;

    /**
     * Default constructor.
     */
  public:
    Constraints();

    /**
     * Create a new type definition
     * @param p the type parameters
     * @param c an expression of type boolean that must be true when evaluated with the type
     */
  public:
    Constraints(Parameters p, ExprPtr constraintExpr);

    /** Destructor */
  public:
    ~Constraints();

    /**
     * Create a constraints that are the conjunction of two other constraints
     * @param left a constraints object
     * @param right a constraints object
     * @return the result of the conjunction or nullptr if not possible
     */
  public:
    static ::std::shared_ptr<const Constraints> conjunctionOf(
        const ::std::shared_ptr<const Constraints> &left,
        const ::std::shared_ptr<const Constraints> &right);

    /**
     * Create new constraints by constraing the given object.
     * @param constraint a new constraint expression
     * @return new type definition
     */
  public:
    Constraints constrain(ExprPtr constraintExpr) const;

    /**
     * Evaluate the constraints of this definition.
     *
     * @param args the arguments
     * @param env an environment
     * @return true if the constraints are satisfied, false if not, and nothing if we cannot tell yet.
     * @throws invalid_argument if the argument don't match the parameters
     */
  public:
    ::std::optional<bool> test(const Arguments &args, Environment env) const;

    /**
     * Update the environment with the arguments.
     *
     * The current bindings of the environment are replaced with
     * new bindings based on the arguments.
     *
     * @param args arguments
     * @param env an environment
     * @return an environment based on the provided environment
     */
  public:
    Environment getEnvironment(const Arguments &args, const Environment &env) const;

    /** The arguments */
  public:
    const Parameters parameters;

    /** An expression that must evaluate to true; the expression is evaluated in the context of the expression
     * for the definition
     */
  public:
    const ExprPtr constraints;
  };
}
#endif
