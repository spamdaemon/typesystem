#include <mylang/typesystem/Constructor.h>
#include <mylang/typesystem/FreeVariables.h>
#include <mylang/typesystem/exprs/TypeExpr.h>
#include <mylang/typesystem/exprs/VarExpr.h>

namespace mylang::typesystem {
  using namespace exprs;

  namespace {
    static TypeExprPtr mkTypeExpr(::std::string typeName, const Constructor::Parameters &p)
    {
      Constructor::Arguments args(p.begin(), p.end());
      return TypeExpr::ofArgs(::std::move(typeName), ::std::move(args));
    }
  }

  Constructor::Constructor(::std::string typeName, TypeParameters t, Parameters p)
      : Function(::std::move(t), p, mkTypeExpr(::std::move(typeName), p)), pattern(body)
  {
  }

  Constructor::~Constructor()
  {
  }

  ::std::shared_ptr<const Constructor> Constructor::of(::std::string xname, TypeParameters t,
      Parameters p)
  {
    return ::std::make_shared<Constructor>(xname, ::std::move(t), ::std::move(p));
  }

  ::std::shared_ptr<const Constructor> Constructor::of(::std::string xname, Parameters p)
  {
    const FreeVariables freeVars(p.begin(), p.end());
    ::std::vector<VarName> freeTypes(freeVars.freeTypes.begin(), freeVars.freeTypes.end());
    return of(xname, ::std::move(freeTypes), p);
  }

  TypeExprPtr Constructor::getBody() const
  {
    return body->self<TypeExpr>();
  }

  Pattern Constructor::getCallPattern(const ::std::vector<Pattern> &patternArgs) const
  {
    ::std::vector<ExprPtr> args;
    args.reserve(parameters.size());
    for (const auto &p : patternArgs) {
      args.push_back(p.expression);
    }

    if (auto err = checkArguments(args); err) {
      throw ::std::invalid_argument(err.value());
    }
    return Pattern(getBody()->replaceArguments(args));
  }

}
