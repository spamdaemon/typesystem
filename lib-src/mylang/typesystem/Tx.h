#ifndef MYLANG_TYPESYSTEM_TX_H
#define MYLANG_TYPESYSTEM_TX_H

#include <functional>

namespace mylang::typesystem {

  /**
   * A generic class that represents the result
   * of transformation.
   */
  template<class T>
  struct Tx
  {
    inline Tx(T e, bool s)
        : actual(::std::move(e)), isNew(s)
    {
    }

    inline static Tx<T> txNew(T e)
    {
      return Tx<T>(::std::move(e), true);
    }

    inline static Tx<T> txOld(T e)
    {
      return Tx<T>(::std::move(e), false);
    }

    inline Tx& operator<<=(::std::function<Tx(const T&)> f)
    {
      auto x = f(actual);
      actual = x.actual;
      isNew = isNew || x.isNew;
      return *this;
    }

    T actual;
    bool isNew;
  };
}
#endif
