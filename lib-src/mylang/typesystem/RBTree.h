#ifndef CLASS_MYLANG_TYPESYSTEM_RBTREE_H
#define CLASS_MYLANG_TYPESYSTEM_RBTREE_H

#include <algorithm>
#include <cassert>
#include <functional>
#include <memory>
#include <stddef.h>
#include <utility>
#include <vector>

namespace mylang::typesystem {

  /**
   * A simple implementation of an immutable RBTree.
   *
   * The tree supports insertion and removal. All operations
   * are O(log n), unless otherwise indicated.
   *
   * Even though this tree is efficient there is is still quite a bit
   * of room for future improvements.
   */
  template<class K, class V, class Ord = ::std::less<K>>
  class RBTree
  {
  private:
    using Color = char;

    static constexpr Color MINUS_BLACK = -1;
    // we choose RED=0, so that the black-height of a red node is 0
    static constexpr Color RED = 0;
    static constexpr Color BLACK = 1;
    static constexpr Color DOUBLE_BLACK = 2;

    /** The key type */
  public:
    using key_type = K;

    /** The value type */
  public:
    using value_type = V;

    /** The comparator that establishes the ordering for the value in the tree.
     * The ordering
     * function may not take all states of the value into acnElements. */
  public:
    using key_compare = Ord;

    /** The data at a node */
  public:
    using Data = ::std::pair<const key_type,value_type>;

    /** A data pointer */
  public:
    using DataPtr = ::std::shared_ptr<const Data>;

    /** A tree node */
  public:
    struct Node
    {
      /**
       * Create a new node
       * @param k the key type
       * @param v the value type
       * @param left the left node
       * @param right the right node
       * @param color the color
       */
      inline Node(DataPtr xdata, ::std::shared_ptr<const Node> xlt,
          ::std::shared_ptr<const Node> xgt, Color col)
          : data(::std::move(xdata)), lt(::std::move(xlt)), gt(::std::move(xgt)), color(col)
      {
      }

      /** The value stored in the node */
      DataPtr data;

      /** The children of the node */
      ::std::shared_ptr<const Node> lt, gt;

      /** The color (this is mutable) */
      mutable Color color;
    };

    /** A node pointer */
  public:
    using NodePtr = ::std::shared_ptr<const Node>;

  private:
    RBTree(size_t n, NodePtr xroot, NodePtr xBlackLeaf)
        : blackLeaf(::std::move(xBlackLeaf)), root(::std::move(xroot)), nElements(n)
    {
    }

  public:
    RBTree(RBTree &&t)
        : blackLeaf(::std::move(t.blackLeaf)), root(::std::move(t.root)), nElements(t.nElements)
    {
    }

  public:
    RBTree(const RBTree &t)
        : blackLeaf(t.blackLeaf), root(t.root), nElements(t.nElements)
    {
    }

  public:
    RBTree()
        : blackLeaf(make_node(nullptr, nullptr, nullptr, BLACK)), root(blackLeaf), nElements(0)
    {
    }

  public:
    RBTree& operator=(const RBTree &tree)
    {
      root = tree.root;
      nElements = tree.nElements;
      return *this;
    }

  public:
    RBTree& operator=(RBTree &&tree)
    {
      root = ::std::move(tree.root);
      nElements = tree.nElements;
      tree.root = tree.blackLeaf;
      tree.nElements = 0;
      return *this;
    }

    /**
     * Create an RBTree from a list of pairs.
     * If there are duplicate keys in teh pairs, then only the last will be retained.
     */
  public:
    static RBTree of(const ::std::vector<::std::pair<K, V>> &v)
    {
      RBTree res;
      for (const auto &p : v) {
        res = res.insert(p.first, p.second);
      }
      return res;
    }

    /**
     * Get the number of elements in this tree.
     *
     * This is a O(1) time operation.
     *
     * @return the size of this tree
     */
  public:
    inline size_t size() const
    {
      return this->nElements;
    }

    /**
     * Compare two trees.
     * @param a a tree
     * @param b a tree
     * @return true if both trees are the same
     */
    friend bool operator==(const RBTree &a, const RBTree &b)
    {
      return a.root == b.root;
    }

    /**
     * Compare two trees.
     * @param a a tree
     * @param b a tree
     * @return true if both trees have different roots
     */
    friend bool operator!=(const RBTree &a, const RBTree &b)
    {
      return a.root != b.root;
    }

    /**
     * Insert a new key-value mapping into this tree.
     *
     * This method inserts a new key into the tree and
     * associates a value with it. If there was already
     * an entry for this key, then no change is made.
     *
     * If the tree was updated, then a new version will have
     * been created. If the key was not inserted, then the
     * same tree will be returned.
     *
     * @param k a key
     * @param v a value
     * @return a tree
     */
  public:
    inline RBTree insert(const key_type &k, const value_type &v) const
    {
      bool rebalance = false;
      NodePtr n = insertNode(root, k, v, false, rebalance);
      if (n == root) {
        return *this;
      }
      assert(!isBlackLeaf(n));
      n->color = BLACK;
      return RBTree(nElements + 1, ::std::move(n), blackLeaf);
    }

    /**
     * Associate a new value with key, or update an existing mapping for the key.
     *
     * This method always updates the tree and thus returns a new version.
     *
     * @param k a key
     * @param v a value
     * @return a new tree
     */
  public:
    inline RBTree insert_or_assign(const key_type &k, const value_type &v) const
    {
      bool rebalance = false;
      NodePtr n = insertNode(root, k, v, true, rebalance);
      assert(!isBlackLeaf(n));
      n->color = BLACK;
      return RBTree(nElements + 1, ::std::move(n), blackLeaf);
    }

    /**
     * Associate a new value with key, or update an existing mapping for the key.
     *
     * This method always updates the tree and thus returns a new version.
     *
     * @param k a key
     * @param v a value
     * @return a new tree
     */
  public:
    inline RBTree insert_or_alter(const key_type &k, const value_type &v) const
    {
      bool rebalance = false;
      NodePtr n = insertNode(root, k, v, true, rebalance);
      assert(!isBlackLeaf(n));
      n->color = BLACK;
      return RBTree(nElements + 1, ::std::move(n), blackLeaf);
    }

    /**
     * Remove the value associated with the specified key.
     * @param k a key
     * @return a tree
     */
  public:
    inline RBTree remove(const key_type &k) const
    {
      NodePtr n = removeNode(root, k);
      if (n == root) {
        return *this;
      }
      n->color = BLACK;
      return RBTree(nElements - 1, ::std::move(n), blackLeaf);
    }

    /**
     * Find the value with the specified key.
     * @param k a key
     * @return a pointer or nullptr if not found
     */
  public:
    DataPtr find(const key_type &k) const
    {
      // we're using raw pointers to avoid ref-nElementsing
      // which should improve performance a bit
      const Node *r = root.get();
      const Data *d = r->data.get();
      while (d) {
        if (less(k, *d)) {
          r = r->lt.get();
          d = r->data.get();
        } else if (less(*d, k)) {
          r = r->gt.get();
          d = r->data.get();
        } else {
          // found the data
          return r->data;
        }
      }
      return nullptr;
    }

    /**
     * Determine if this tree contains an entry for the specified key.
     * @param k a key
     * @return true if this tree contains the specified key.
     */
  public:
    inline bool contains(const key_type &k) const
    {
      return find(k) != nullptr;
    }

    /**
     * Get the number of values associated with the key.
     * @param k a key
     * @return number of occurrences of the key in this tree
     */
  public:
    inline size_t count(const key_type &k) const
    {
      return contains(k) ? 1 : 0;
    }

    /**
     * Get the list of items
     *
     * This is a O(n) time operation.
     *
     * @return a list of references to the data items according to the ordering.
     */
  public:
    inline ::std::vector<DataPtr> elements() const
    {
      ::std::vector<DataPtr> v;
      v.reserve(size());
      elements(root, v);
      return v;
    }

    /**
     * Get the data that whose key is not less than the specified key.
     * @param k a key
     * @param data whose whose k is at least k or nullptr if not found
     */
  public:
    inline DataPtr lowerBound(const key_type &k) const
    {
      return lowerBound(root, k);
    }

    /**
     * Get the min value
     * @return the data with the minimum key or nullptr if the tree is empty
     */
    inline DataPtr min() const
    {
      if (isBlackLeaf(root)) {
        return nullptr;
      }
      const Node *n = root.get();

      while (!isBlackLeaf(n->lt)) {
        n = n->lt.get();
      }
      return n->data;
    }

    /**
     * Get the max value
     * @return the data with the maximum key or nullptr if the tree is empty
     */
    inline DataPtr max() const
    {
      if (isBlackLeaf(root)) {
        return nullptr;
      }
      const Node *n = root.get();
      while (!isBlackLeaf(n->gt)) {
        n = n->gt.get();
      }
      return n->data;
    }

    /**
     * Get the data with the smallest key that is greater than the specified key.
     * @param k a key
     * @param data or nullptr if not found
     */
  public:
    inline DataPtr upperBound(const key_type &k) const
    {
      return upperBound(root, k);
    }

  private:
    static DataPtr lowerBound(const NodePtr &n, const key_type &k)
    {
      if (isBlackLeaf(n)) {
        return nullptr;
      }
      if (less(*n->data, k)) {
        return lowerBound(n->gt, k);
      } else if (less(k, *n->data)) {
        if (auto tmp = lowerBound(n->lt, k); tmp) {
          return tmp;
        }
      }
      return n->data;
    }

    /**
     * Get the data with the smallest key that is greater than the specified key.
     * @param k a key
     * @param data or nullptr if not found
     */
  private:
    static DataPtr upperBound(const NodePtr &n, const key_type &k)
    {
      if (isBlackLeaf(n)) {
        return nullptr;
      }
      if (less(k, *n->data)) {
        if (auto tmp = upperBound(n->lt, k); tmp) {
          return tmp;
        }
        return n->data;
      } else {
        return upperBound(n->gt, k);
      }
    }

    /** Enumerate the elements and but them into the list */
  private:
    void elements(const NodePtr &n, ::std::vector<DataPtr> &v) const
    {
      if (isBlackLeaf(n)) {
        return;
      }
      elements(n->lt, v);
      v.push_back(n->data);
      elements(n->gt, v);
    }

  private:
    inline static NodePtr make_node(DataPtr data, ::std::shared_ptr<const Node> lt,
        ::std::shared_ptr<const Node> gt, Color color)
    {
      return ::std::make_shared<Node>(::std::move(data), ::std::move(lt), ::std::move(gt), color);
    }

  private:
    static inline NodePtr repaint(const NodePtr &n, Color color)
    {
      return make_node(n->data, n->lt, n->gt, color);
    }

    /**
     * Rebalance the node
     * @param n a node
     * @return a new balanced node
     */
  private:
    static NodePtr balance(const NodePtr &n)
    {
      NodePtr left0 = n->lt;
      NodePtr right0 = n->gt;

      if (n->color >= 0) {
        if (left0->color == RED) {
          if (left0->lt->color == RED) {
            NodePtr right = make_node(n->data, left0->gt, right0, BLACK);
            NodePtr left = repaint(left0->lt, BLACK);
            return make_node(left0->data, left, right, n->color - 1);
          } else if (left0->gt->color == RED) {
            NodePtr left = make_node(left0->data, left0->lt, left0->gt->lt, BLACK);
            NodePtr right = make_node(n->data, left0->gt->gt, right0, BLACK);
            return make_node(left0->gt->data, left, right, n->color - 1);
          }
        }
        if (right0->color == RED) {
          if (right0->gt->color == RED) {
            NodePtr left = make_node(n->data, left0, right0->lt, BLACK);
            NodePtr right = repaint(right0->gt, BLACK);
            return make_node(right0->data, left, right, n->color - 1);
          } else if (right0->lt->color == RED) {
            NodePtr left = make_node(n->data, left0, right0->lt->lt, BLACK);
            NodePtr right = make_node(right0->data, right0->lt->gt, right0->gt, BLACK);
            return make_node(right0->lt->data, left, right, n->color - 1);
          }
        }
        if (left0->color == MINUS_BLACK) {
          NodePtr leftleft = repaint(left0->lt, RED);
          NodePtr left = balance(make_node(left0->data, leftleft, left0->gt->lt, BLACK));
          NodePtr right = make_node(n->data, left0->gt->gt, n->gt, BLACK);
          return make_node(left0->gt->data, left, right, BLACK);
        }
        if (right0->color == MINUS_BLACK) {
          NodePtr rightright = repaint(right0->gt, RED);
          NodePtr right = balance(make_node(right0->data, right0->lt->gt, rightright, BLACK));
          NodePtr left = make_node(n->data, n->lt, right0->lt->lt, BLACK);
          return make_node(right0->lt->data, left, right, BLACK);
        }
      }

      return make_node(n->data, left0, right0, n->color);
    }

    /**
     * Insert a node into under the specified leaf.
     * @param n the node at or under which to insert
     * @param k the key to insert
     * @param v the value to associated with the key
     * @param alter true if the value should be altered
     * @return a new node that may be RED
     */
  private:
    NodePtr insertNode(const NodePtr &n, const key_type &k, const value_type &v, bool alter,
        bool &rebalance) const
    {
      rebalance = true;
      if (isBlackLeaf(n)) {
        DataPtr data = ::std::make_shared<Data>(k, v);
        return make_node(::std::move(data), n, n, RED);
      }
      NodePtr r;
      if (less(k, *n->data)) {
        auto lt = insertNode(n->lt, k, v, alter, rebalance);
        if (lt == n->lt) {
          // since this is the same pointer, nothing has changed in the tree
          rebalance = false;
          return n;
        }
        r = make_node(n->data, lt, n->gt, n->color);
      } else if (less(*n->data, k)) {
        auto gt = insertNode(n->gt, k, v, alter, rebalance);
        if (gt == n->gt) {
          // since this is the same pointer, nothing has changed in the tree
          rebalance = false;
          return n;
        }
        r = make_node(n->data, n->lt, gt, n->color);
      } else {
        // since we're not altering the structure of the tree, we don't need to rebalance
        rebalance = false;
        if (alter) {

          r = make_node(::std::make_shared<Data>(k, v), n->lt, n->gt, n->color);
        } else {
          r = n;
        }
      }

      if (rebalance) {
        r = balance(r);
      }

      return r;
    }

  private:
    NodePtr remove_fringe(const NodePtr &tree) const
    {
      if (isBlackLeaf(tree->gt)) {
        if (isBlackLeaf(tree->lt)) {
          return tree->color == RED ? blackLeaf : doubleBlackLeaf();
        } else {
          return repaint(tree->lt, BLACK);
        }

      } else {
        assert(isBlackLeaf(tree->lt) && "tree is not on the fringe");
        return repaint(tree->gt, BLACK);
      }
    }

  private:
    NodePtr redden(const NodePtr &tree) const
    {
      // if we have a black-leaf, then we'd be turning it
      // into a red node, which makes no sense
      assert(!isBlackLeaf(tree));

      if (isDoubleBlackLeaf(tree)) {
        return blackLeaf;
      }
      return make_node(tree->data, tree->lt, tree->gt, tree->color - 1);
    }

  private:
    NodePtr bubble(DataPtr data, NodePtr lt, NodePtr gt, Color color) const
    {
      if (lt->color == DOUBLE_BLACK || gt->color == DOUBLE_BLACK) {
        NodePtr l = redden(lt);
        NodePtr r = redden(gt);
        NodePtr res = make_node(::std::move(data), ::std::move(l), ::std::move(r), color + 1);
        return balance(::std::move(res));
      }
      return make_node(::std::move(data), ::std::move(lt), ::std::move(gt), color);
    }

  private:
    NodePtr remove_max(NodePtr tree, DataPtr &maxData) const
    {
      if (isBlackLeaf(tree->gt)) {
        maxData = tree->data;
        return remove_fringe(tree);
      } else {
        NodePtr gt = remove_max(tree->gt, maxData);
        return bubble(tree->data, tree->lt, gt, tree->color);
      }
    }

  private:
    NodePtr removeNode(const NodePtr &tree, const key_type &k) const
    {
      if (isBlackLeaf(tree)) {
        return tree;
      }
      if (less(k, *tree->data)) {
        NodePtr lt = removeNode(tree->lt, k);
        if (lt == tree->lt) {
          return tree;
        }
        return bubble(tree->data, lt, tree->gt, tree->color);
      } else if (less(*tree->data, k)) {
        NodePtr gt = removeNode(tree->gt, k);
        if (gt == tree->gt) {
          return tree;
        }
        return bubble(tree->data, tree->lt, gt, tree->color);
      }

      if (isBlackLeaf(tree->lt) || isBlackLeaf(tree->gt)) {
        return remove_fringe(tree);
      } else {
        DataPtr maxData;
        NodePtr lt = remove_max(tree->lt, maxData);

        return bubble(maxData, lt, tree->gt, tree->color);
      }
    }

  private:
    inline static bool isBlackLeaf(const Node *&n)
    {
      return n->data == nullptr && n->color == BLACK;
    }

  private:
    inline static bool isBlackLeaf(const NodePtr &n)
    {
      return n->data == nullptr && n->color == BLACK;
    }

  private:
    inline static bool isDoubleBlackLeaf(const NodePtr &n)
    {
      return n->data == nullptr && n->color == DOUBLE_BLACK;
    }

  private:
    inline static bool less(const Data &a, const key_type &b)
    {
      return key_compare()(a.first, b);
    }

  private:
    inline static bool less(const key_type &a, const Data &b)
    {
      return key_compare()(a, b.first);
    }

  public:
    size_t check_tree() const
    {
      return check_tree(root);
    }

  private:
    static size_t check_tree(NodePtr tree)
    {
      if (isBlackLeaf(tree)) {
        assert(tree->color == BLACK && "Leaf must be black");
        return 0;
      }
      assert((tree->color == BLACK || tree->color == RED) && "Nodes must either be black or red");
      size_t hLeft = check_tree(tree->lt);
      size_t hRight = check_tree(tree->gt);
      assert(hLeft == hRight && "Tree heights should be the same");
      return tree->color + hLeft;
    }

  private:
    static NodePtr doubleBlackLeaf()
    {
      return make_node(nullptr, nullptr, nullptr, DOUBLE_BLACK);
    }

    /** A black leaf */
  private:
    NodePtr blackLeaf;

    /** The root of this tree  */
  private:
    NodePtr root;

    /** The size of this tree */
  private:
    size_t nElements;
  }
  ;

}
#endif
