#ifndef FILE_MYLANG_TYPESYSTEM_H
#define FILE_MYLANG_TYPESYSTEM_H

#include <memory>

namespace mylang::typesystem {

  class Function;
  class Predicate;
  class Property;
  class TypeDefinition;
  class VarName;

  namespace types {
    class Type;
    class WType;
    class XType;
    class WType;
    using TypePtr = ::std::shared_ptr<const Type>;
    using WTypePtr = ::std::shared_ptr<const WType>;
    using XTypePtr = ::std::shared_ptr<const XType>;
    using WTypePtr = ::std::shared_ptr<const WType>;
  }
  namespace exprs {
    class Expr;
    class CallExpr;
    class TypeExpr;
    class VarExpr;
    using ExprPtr = ::std::shared_ptr<const Expr>;
    using VarPtr = ::std::shared_ptr<const VarExpr>;
    using CallExprPtr = ::std::shared_ptr<const CallExpr>;
    using TypeExprPtr = ::std::shared_ptr<const TypeExpr>;
  }

  using ExprPtr = exprs::ExprPtr;
  using CallExprPtr = exprs::CallExprPtr;
  using VarPtr = exprs::VarPtr;
  using TypeExprPtr = exprs::TypeExprPtr;
  using TypePtr = types::TypePtr;
  using WTypePtr = types::WTypePtr;
  using XTypePtr = types::XTypePtr;
  using WTypePtr = types::WTypePtr;
  using PropertyPtr = ::std::shared_ptr<const Property>;
  using FunctionPtr = ::std::shared_ptr<const Function>;
  using TypeDefinitionPtr = ::std::shared_ptr<const TypeDefinition>;
}
#endif
