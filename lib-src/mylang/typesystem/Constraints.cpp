#include <mylang/typesystem/Constraints.h>
#include <mylang/typesystem/exprs/Expr.h>
#include <mylang/typesystem/exprs/BuiltinExpr.h>
#include <mylang/typesystem/exprs/BLiteral.h>
#include <mylang/typesystem/exprs/TypeExpr.h>
#include <mylang/typesystem/exprs/ExprTransform.h>
#include <mylang/typesystem/types/BType.h>
#include <mylang/typesystem/Environment.h>
#include <mylang/typesystem/FreeVariables.h>
#include <mylang/typesystem/eval/Eval.h>
#include <stdexcept>
#include <mylang/Self.h>
#include <mylang/typesystem/exprs/ErrorExpr.h>
#include <mylang/typesystem/RBTree.h>
#include <mylang/typesystem/typesystem.h>
#include <mylang/typesystem/solver/Solver.h>
#include <pstl/glue_algorithm_defs.h>
#include <algorithm>
#include <memory>
#include <optional>
#include <type_traits>
#include <iostream>

namespace mylang::typesystem {
  using namespace exprs;

  Constraints::Constraints()
      : constraints(BLiteral::of(true))
  {
  }

  Constraints::Constraints(Parameters params, ExprPtr constraintExpr)
      : parameters(::std::move(params)), constraints(::std::move(constraintExpr))
  {
    if (constraints->type->unifiable(types::BType::create()) == false) {
      throw ::std::invalid_argument("Bad constraints expression");
    }

    // ensure that constraints and definition only have the free variables we allow
    ::std::map<VarName, TypePtr> vars;
    for (const auto &p : parameters) {
      vars.insert( { p.second->name, p.second->type });
    }

    for (const auto &v : FreeVariables(constraints).freeVariables) {
      auto i = vars.find(v.first);
      if (i == vars.end()) {
        throw ::std::invalid_argument("Unknown free variables in constraint " + v.first.toString());
      }
      if (v.second->unifiable(i->second) == false) {
        throw ::std::invalid_argument("Bad free variable in constraints " + v.first.toString());
      }
    }
    auto bindings = solver::Solver::eval(constraints, { });
    if (bindings.has_value() == false) {
      throw ::std::invalid_argument("Could not determine satisfiability for constraints");
    }
  }

  Constraints::~Constraints()
  {
  }

  Constraints Constraints::constrain(ExprPtr constraintExpr) const
  {
    if (constraintExpr == nullptr) {
      return *this;
    } else {
      return Constraints(parameters, BuiltinExpr::andOf(constraintExpr, constraints));
    }
  }

  Environment Constraints::getEnvironment(const Arguments &args, const Environment &env) const
  {
    if (args.size() != parameters.size()) {
      throw ::std::invalid_argument("Bad arguments");
    }
    Environment newEnv(env);

    for (const auto &kv : args) {
      auto p = parameters.find(kv.first);
      if (p == parameters.end()) {
        throw ::std::invalid_argument("No such parameters " + kv.first);
      }
      if (!p->second->type->unifiable(kv.second->type)) {
        throw ::std::invalid_argument("Bad argument type for " + kv.first);
      }

      newEnv.bindings = newEnv.bindings.insert_or_assign(p->second->name, kv.second);
    }
    return newEnv;
  }

  ::std::optional<bool> Constraints::test(const Arguments &args, Environment env) const
  {
    env = getEnvironment(args, env);

    if (auto b = constraints->self<BLiteral>(); b) {
      return b->value;
    }

    Tx<ExprPtr> res(constraints, true);
    while (res.isNew) {
      res = eval::Eval::eval(res.actual, env);
    }
    // either, we have a literal value
    if (auto b = res.actual->self<BLiteral>(); b) {
      return b->value;
    }
    // or an error
    if (auto err = res.actual->self<ErrorExpr>(); err) {
      return false;
    }
    // or we don't know yet
    return ::std::nullopt;
  }

  ::std::shared_ptr<const Constraints> Constraints::conjunctionOf(
      const ::std::shared_ptr<const Constraints> &left,
      const ::std::shared_ptr<const Constraints> &right)
  {
    if (!left) {
      return right;
    }
    if (!right) {
      return left;
    }

    // make sure the types match
    Parameters params;
    {
      // generate the union of the parameters

      for (const auto &l : left->parameters) {
        params.insert( { l.first, VarExpr::of(l.second->type) });
      }
      for (const auto &r : right->parameters) {
        if (auto p = params.find(r.first); p != params.end()) {
          if (r.second->type->unifiable(p->second->type) == false) {
            // types of arguments don't match
            return nullptr;
          }
        } else {
          params.insert( { r.first, VarExpr::of(r.second->type) });
        }
      }
    }

    ExprPtr lc = left->constraints;
    for (const auto &p : left->parameters) {
      const auto &param = params[p.first];
      lc = LetExpr::of(p.second->name, param, lc);
    }
    ExprPtr rc = right->constraints;
    for (const auto &p : right->parameters) {
      const auto &param = params[p.first];
      rc = LetExpr::of(p.second->name, param, rc);
    }
    auto newConstraints = BuiltinExpr::andOf(lc, rc);
    return ::std::make_shared<Constraints>(::std::move(params), ::std::move(newConstraints));
  }

}
