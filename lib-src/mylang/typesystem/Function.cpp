#include <mylang/typesystem/Function.h>
#include <mylang/typesystem/types/WType.h>
#include <mylang/typesystem/exprs/Expr.h>
#include <mylang/typesystem/exprs/ExprTransform.h>
#include <mylang/typesystem/Environment.h>
#include <mylang/typesystem/eval/Eval.h>
#include <mylang/typesystem/FreeVariables.h>

#include <map>
#include <stdexcept>

namespace mylang::typesystem {
  using namespace exprs;

  Function::Function(TypeParameters typeParams, Parameters params, ExprPtr b)
      : typeParameters(::std::move(typeParams)), parameters(::std::move(params)),
          body(::std::move(b))
  {
    if (auto err = checkFreeVariables(body); err) {
      throw ::std::invalid_argument(err.value());
    }
  }

  Function::~Function()
  {
  }

  ::std::shared_ptr<const Function> Function::of(TypeParameters typeParams, Parameters params,
      ExprPtr expr)
  {
    return ::std::make_shared<Function>(::std::move(typeParams), ::std::move(params),
        ::std::move(expr));
  }

  ::std::optional<::std::string> Function::checkFreeVariables(const ExprPtr &expr) const
  {
    FreeVariables freeParms(parameters.begin(), parameters.end());
    FreeVariables freeBody(expr);

    for (const auto &p : freeParms.freeTypes) {
      if (freeBody.freeVariables.count(p) != 1) {
        return "conflicting type variable and parameter name " + p.toString();
      }
    }

    for (const auto &p : freeBody.freeTypes) {
      if (freeBody.freeVariables.count(p) != 1) {
        return "conflicting type variable and local variable " + p.toString();
      }
    }

    // remove the type parameters from the free variables
    for (const auto &p : typeParameters) {
      freeParms.freeTypes.erase(p);
      freeBody.freeTypes.erase(p);
    }
    if (freeParms.freeTypes.empty() == false || freeBody.freeTypes.empty() == false) {
      return "Undeclared type parameters found in function signature or body";
    }

    for (const auto &p : parameters) {
      if (auto i = freeBody.freeVariables.find(p->name); i != freeBody.freeVariables.end()) {
        if (i->second->unifiable(p->type) == false) {
          return "Type mismatch for free variable " + p->name.toString();
        }
        freeBody.freeVariables.erase(i);
      }
    }
    return ::std::nullopt;
  }
  ::std::optional<::std::string> Function::checkArguments(const Arguments &args) const
  {
    if (args.size() != parameters.size()) {
      return "Bad arguments";
    }
    for (size_t i = 0; i < parameters.size(); ++i) {
      const auto &p = parameters.at(i);
      const auto &a = args.at(i);

      if (false == a->type->unifiable(p->type)) {
        return "Type mismatch for parameter " + ::std::to_string(i);
      }
    }
    return ::std::nullopt;
  }

  ExprPtr Function::substitute(const Arguments &args) const
  {
    if (auto err = checkArguments(args); err) {
      throw ::std::invalid_argument(err.value());
    }
    ExprPtr res = body;
    for (size_t i = 0; i < args.size(); ++i) {
      const auto &arg = args.at(i);
      const auto &p = parameters.at(i);
      res = LetExpr::of(p->name, arg, res);
    }
    return res;
  }

}
