#include <mylang/typesystem/Pattern.h>
#include <mylang/typesystem/algorithms/Unify.h>
#include <mylang/typesystem/predicates/IsPattern.h>
#include <stdexcept>

namespace mylang::typesystem {
  using namespace exprs;

  Pattern::Pattern(ExprPtr expr)
      : expression(expr), freeVariables(expr)
  {
    if (!predicates::IsPattern::test(expression)) {
      throw ::std::invalid_argument("Not a pattern expression");
    }
  }

  Pattern::~Pattern()
  {
  }

  bool Pattern::matches(const Pattern &that) const
  {
    auto r = algorithms::Unify::apply(*this, that, { });
    return r.second == algorithms::Unify::UNIFIED;
  }

}
