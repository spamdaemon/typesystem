#include <mylang/typesystem/VarName.h>
#include <sstream>

namespace mylang {
  namespace typesystem {

    VarName::VarName()
        : name(::std::make_shared<Impl>(::std::string("fresh")))
    {
    }

    VarName::VarName(::std::string str)
        : name(::std::make_shared<Impl>(::std::move(str)))
    {
    }

    VarName::VarName(const char *cstr)
        : name(::std::make_shared<Impl>(cstr))
    {
    }

    VarName::~VarName()
    {
    }

    VarName VarName::freshen() const
    {
      return VarName(name->prefix);
    }

    ::std::string VarName::toString() const
    {
      ::std::ostringstream out;
      out << name->prefix;
      out << '.';
      out << name;
      return out.str();
    }

  }
}
