#include <mylang/typesystem/visitors/Printer.h>
#include <mylang/typesystem/exprs/ExprVisitor.h>
#include <mylang/typesystem/exprs/exprs.h>
#include <mylang/typesystem/typesystem.h>
#include <mylang/typesystem/ast/AType.h>
#include <mylang/typesystem/ast/AFunctionCall.h>
#include <mylang/typesystem/ast/AValue.h>
#include <mylang/Integer.h>

#include <memory>
#include <string>

namespace mylang::typesystem::visitors {
  using namespace exprs;

  namespace {

    /** The output state */
    struct State
    {
      enum Emit
      {
        NOTHING, SPACE, INDENT, NEWLINE, NEWLINE_OUTDENT, NEWLINE_INDENT
      };

      State(::std::ostream &s, const Printer::Config&)
          : out(s), emit(NOTHING)
      {
      }

      State& before()
      {
        switch (emit) {
        case NEWLINE:
          out << ::std::endl << indentString;
          emit = NOTHING;
          break;
        case NEWLINE_OUTDENT:
          outdent();
          out << ::std::endl << indentString;
          emit = NOTHING;
          break;
        case NEWLINE_INDENT:
          indent();
          out << ::std::endl << indentString;
          emit = NOTHING;
          break;
        case SPACE:
          out << ' ';
          emit = NOTHING;
          break;
        case INDENT:
          out << indentString;
          emit = NOTHING;
          break;
        case NOTHING:
          break;
        }
        return *this;
      }

      State& beforeNoSpace()
      {
        if (emit == SPACE) {
          emit = NOTHING;
        } else {
          before();
        }
        return *this;
      }

      /** Indent */
      State& indent()
      {
        indentString.resize(indentString.size() + 2, ' ');
        return *this;
      }

      State& indented(::std::function<void()> f)
      {
        auto bak = indentString;
        indent();
        f();
        indentString = ::std::move(bak);
        return *this;
      }

      State& outdent()
      {
        indentString.resize(indentString.size() - 2);
        return *this;
      }

      State& paren(const ::std::string_view &first, ::std::function<void()> f)
      {
        before();
        out << '(';
        indent();
        out << first;
        if (!first.empty()) {
          emit = SPACE;
        }
        if (f) {
          f();
        }
        beforeNoSpace();
        out << ')';
        return *this;
      }
      State& paren(const char *first, ::std::function<void()> f)
      {
        ::std::string_view v(first);
        return paren(v, f);
      }

      State& paren(::std::function<void()> f)
      {
        return paren("", f);
      }

      State& emitTextLiteral(const ::std::string &value)
      {
        before();
        out << '"' << value << '"';
        emit = SPACE;
        return *this;
      }

      State& emitValue(const char *&value)
      {
        before();
        out << value;
        emit = SPACE;
        return *this;
      }

      State& emitValue(const ::std::string &value)
      {
        before();
        out << value;
        emit = SPACE;
        return *this;
      }

      State& emitValue(const Integer &value)
      {
        before();
        out << value;
        emit = SPACE;
        return *this;
      }

      ::std::ostream &out;

      /** True if we need a space */
      Emit emit;

      /** The current indent string */
      ::std::string indentString;
    };

    struct PrintVisitor: public ExprVisitor
    {
      State &out;

      PrintVisitor(State &s)
          : out(s)
      {
      }

      ~PrintVisitor()
      {
      }

      void print(const ast::AType::Ptr &e)
      {
        out.paren("atype", [&]() {
          e->pattern.expression->accept(*this);
        });
      }
      void print(const ast::ASignature::Ptr &e)
      {
        out.emit = State::NEWLINE;
        out.paren("sig", [&]() {
          out.paren([&]() {
                for (const auto &p : e->parameters) {
                  print(p);
                }
              });
          print(e->returnType);
        });
      }

      void print(const ast::AExpression::Ptr &e)
      {
        out.emit = State::NEWLINE;
        if (auto fn = e->self<ast::AFunctionCall>(); fn) {

          out.paren("apply", [&]() {
            out.emit=State::NEWLINE;
            print(fn->target);
            out.paren([&]() {
                  for (const auto& arg : fn->arguments) {
                    out.emit=State::NEWLINE;
                    print(arg);
                  }
                });
          });
        } else {
          out.paren("value", [&]() {
            print(e->type);
          });
        }
      }

      void print(const ast::AFunctionSpec::Ptr &fn)
      {
        out.paren("fn", [&]() {
          out.emitValue(fn->name.toString());
          print(fn->signature);
        });
      }

      void print(const FunctionPtr &f)
      {
        out.paren("lambda", [&]() {
          out.paren([&] {
                out.paren([&] {
                      for (auto arg : f->parameters) {
                        arg->accept(*this);
                      }});
              });
          out.emit=State::NEWLINE;
          out.indented([&] {
                f->body->accept(*this);});
        });
      }

      void print(const ast::AFunction::Ptr &f)
      {
        out.paren("lambda", [&]() {
          out.paren([&] {
                f->result->accept(*this);
                out.paren([&] {
                      for (auto arg : f->arguments) {
                        arg->accept(*this);
                      }});
              });
          out.emit=State::NEWLINE;
          out.indented([&] {
                f->body->accept(*this);});
        });
      }

      void visit(const ::std::shared_ptr<const VarExpr> &e) override
      {
        out.paren([&]() {
          out.emitValue(e->name.toString());

          // TODO: emit the type

          });
      }

      void visit(const ::std::shared_ptr<const BLiteral> &e) override
      {
        out.emitValue(e->value ? "true" : "false");
      }
      void visit(const ::std::shared_ptr<const ILiteral> &e) override
      {
        out.emitValue(e->value);
      }
      void visit(const ::std::shared_ptr<const TLiteral> &e) override
      {
        out.emitTextLiteral(e->value);
      }
      void visit(const ::std::shared_ptr<const ConstructExpr> &e) override
      {
        out.emit = State::NEWLINE;
        out.paren("tuple", [&]() {
          for (const auto &arg : e->arguments) {
            arg->accept(*this);
          }
        });
      }

      void visit(const ::std::shared_ptr<const ListLiteral> &e) override
      {
        out.emit = State::NEWLINE;
        out.paren("list", [&]() {
          for (const auto &arg : e->entries) {
            arg->accept(*this);
          }
        });
      }

      void visit(const ::std::shared_ptr<const TypeExpr> &e) override
      {
        out.emit = State::NEWLINE;
        out.paren("type", [&]() {
          e->name->accept(*this);
          e->argument->accept(*this);
        });
      }

      void visit(const ::std::shared_ptr<const BuiltinExpr> &e) override
      {
        out.emit = State::NEWLINE;
        ::std::string f("#");
        f += BuiltinExpr::toString(e->op);

        out.paren(f, [&]() {
          for (const auto &arg : e->operands) {
            out.indented([&] {
                  out.emit = State::NEWLINE;
                  arg->accept(*this);
                });
          }
        });
      }
      void visit(const ::std::shared_ptr<const ConditionalExpr> &e) override
      {
        out.emit = State::NEWLINE;
        out.paren("if", [&]() {
          e->condition->accept(*this);
          e->iftrue->accept(*this);
          e->iffalse->accept(*this);
        });
      }

      void visit(const ::std::shared_ptr<const CallExpr> &e) override
      {
        out.emit = State::NEWLINE;
        out.paren("call", [&]() {
          out.emit=State::NEWLINE;
          out.indented([&] {
                out.paren([&] () {
                      for (const auto &arg : e->arguments) {
                        out.indented([&] {
                              arg->accept(*this);
                            });
                      }
                    });
              });
          out.emit=State::NEWLINE;
          out.indented([&] {
                print(e->target);
              });
        });
      }

      void visit(const ::std::shared_ptr<const LetExpr> &e) override
      {
        out.emit = State::NEWLINE;
        out.paren("let", [&]() {
          out.indented([&] {
                out.paren([&]() {
                      out.paren([&] () {
                            out.emitValue(e->variable.toString());
                            e->value->accept(*this);
                          });
                    });
              });
          out.indented([&] {
                e->result->accept(*this);
              });
        });
      }
      void visit(const ::std::shared_ptr<const ErrorExpr>&) override
      {
        out.emit = State::NEWLINE;
        out.paren("error", { });
      }
    };

  }

  Printer::Config::Config()
  {
  }

  void Printer::print(::std::ostream &out, const ast::AExpression::Ptr &e, const Config &cfg)
  {
    State s(out, cfg);
    PrintVisitor v(s);
    v.print(e);
  }

  void Printer::print(::std::ostream &out, const ast::AFunctionSpec::Ptr &f, const Config &cfg)
  {
    State s(out, cfg);
    PrintVisitor v(s);
    v.print(f);
  }

  void Printer::print(::std::ostream &out, const ast::AFunction::Ptr &f, const Config &cfg)
  {
    State s(out, cfg);
    PrintVisitor v(s);
    v.print(f);
  }

  void Printer::print(::std::ostream &out, const ExprPtr &e, const Config &cfg)
  {
    State s(out, cfg);
    PrintVisitor v(s);
    e->accept(v);
    s.emit = State::NEWLINE;
    s.before();
  }

}
