#ifndef CLASS_MYLANG_TYPESYSTEM_PREDICATES_PRINTER_H
#define CLASS_MYLANG_TYPESYSTEM_PREDICATES_PRINTER_H

#include <mylang/typesystem/typesystem.h>
#include <mylang/typesystem/ast/AExpression.h>
#include <mylang/typesystem/ast/AFunction.h>
#include <mylang/typesystem/ast/AFunctionSpec.h>
#include <iostream>

namespace mylang::typesystem::visitors {

  /**
   * Predicate to determine if the specified expression constitutes a pattern.
   */
  struct Printer
  {
    /** The printer configuration */
    struct Config
    {
      Config();
    };
    /** Print an expression. */
    static void print(::std::ostream &out, const ExprPtr &e, const Config &cfg = Config());

    /** Print an AST expression */
    static void print(::std::ostream &out, const ast::AExpression::Ptr &e, const Config &cfg =
        Config());

    /** Print an AST expression */
    static void print(::std::ostream &out, const ast::AFunctionSpec::Ptr &e, const Config &cfg =
        Config());

    /** Print an AST expression */
    static void print(::std::ostream &out, const ast::AFunction::Ptr &e, const Config &cfg =
        Config());
  };

}

#endif
