#ifndef CLASS_MYLANG_TYPESYSTEM_CONSTRUCTOR_H
#define CLASS_MYLANG_TYPESYSTEM_CONSTRUCTOR_H

#include <mylang/Self.h>
#include <mylang/typesystem/Function.h>
#include <mylang/typesystem/Pattern.h>
#include <mylang/typesystem/typesystem.h>
#include <string>

namespace mylang::typesystem {
  class Environment;
  /**
   * A constructor function.
   */
  class Constructor: public Function
  {
    /**
     * Create a new type definition,
     * @param defName the name of the definition itself
     * @param typeName the name of the type itself
     * @param t positional type parameters
     * @param p the positional parameters for the definition
     * @param constraints the constraints that the parameters need to satisfy
     */
  public:
    Constructor(::std::string typeName, TypeParameters t, Parameters p);

    /** Destructor */
  public:
    ~Constructor();

    /**
     * Create a type constructor.
     *
     * @param typeName the name of the type and the type definition
     * @param t the type variables used in this the constructor or type
     * @param p the parameters for the type constructor arguments
     * @return an expression that constructs an instance of a type
     */
  public:
    static ::std::shared_ptr<const Constructor> of(::std::string typeName, TypeParameters t = { },
        Parameters p = { });

    /**
     * Create a type constructor.
     *
     * The type parameters are derived from the parameters in some
     * unspecified order.
     *
     * @param typeName the name of the type and the type definition
     * @param p the parameters for the type constructor arguments
     * @return an expression that constructs an instance of a type
     */
  public:
    static ::std::shared_ptr<const Constructor> of(::std::string typeName, Parameters p);

    /**
     * Get the body if this function.
     * The body of the definition is always a TypeExpr
     */
  public:
    TypeExprPtr getBody() const;

    /**
     * Get a pattern that represents a call to this constructor.
     * @param args the patterns that provide the arguments
     * @return a pattern
     */
  public:
    Pattern getCallPattern(const ::std::vector<Pattern> &args) const;

    /**
     * The constructor as a pattern
     */
  public:
    const Pattern pattern;
  };
}
#endif
