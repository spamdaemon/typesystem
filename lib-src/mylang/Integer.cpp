#include <mylang/Integer.h>
#include <iostream>
#include <cmath>
#include <limits>

namespace mylang {

  const Integer& Integer::PLUS_INFINITY()
  {
    static const Integer value(::std::nullopt, 1);
    return value;
  }

  const Integer& Integer::MINUS_INFINITY()
  {
    static const Integer value(::std::nullopt, -1);
    return value;
  }

  const Integer& Integer::ZERO()
  {
    static const Integer value(BigInt(0));
    return value;
  }

  const Integer& Integer::PLUS_ONE()
  {
    static const Integer value(BigInt(1));
    return value;
  }

  const Integer& Integer::MINUS_ONE()
  {
    static const Integer value(BigInt(-1));
    return value;
  }

  Integer::Integer(::std::optional<BigInt> v, int s)
      : _value(::std::move(v)), _sign((char) s)
  {
    if (_sign < -1 || _sign > 1) {
      throw ::std::invalid_argument("Invalid sign value");
    } else if (_value.has_value() && _sign != _value->sgn()) {
      throw ::std::invalid_argument("Provided sign and sign of the value do not agree");
    } else if (s == 0 && !_value.has_value()) {
      throw ::std::invalid_argument("Infinite value must either be positive or negative");
    }
  }

  Integer::Integer()
      : _value(0), _sign(0)
  {
  }

  Integer::Integer(BigInt v)
      : _value(v), _sign((char) v.sgn())
  {
    if (_sign < -1 || _sign > 1) {
      throw ::std::invalid_argument("Invalid sign value");
    }
  }

  Integer Integer::negate() const
  {
    return Integer(transform([](const BigInt &num) {return num.negate();}), -sign());
  }

  ::std::optional<Integer> Integer::add(const Integer &op2) const
  {
    if (isInfinite() && op2.isInfinite() && sign() != op2.sign()) {
      return ::std::nullopt;
    }
    if (isInfinite()) {
      return *this;
    }
    if (op2.isInfinite()) {
      return op2;
    }
    return value()->add(*op2.value());
  }

  Integer Integer::increment() const
  {
    if (isInfinite()) {
      return *this;
    }
    return value()->add(*PLUS_ONE().value());
  }

  Integer Integer::decrement() const
  {
    if (isInfinite()) {
      return *this;
    }
    return value()->add(*MINUS_ONE().value());
  }

  Integer Integer::towards0() const
  {
    if (isInfinite() || is0()) {
      return *this;
    }
    return value()->add(*(sign() < 0 ? PLUS_ONE() : MINUS_ONE()).value());
  }

  ::std::optional<Integer> Integer::subtract(const Integer &op2) const
  {
    return add(op2.negate());
  }

  Integer Integer::multiply(const Integer &op2) const
  {
    auto s = sign() * op2.sign();
    if (s == 0) {
      return {0 , 0};
    }
    if (isInfinite()) {
      return Integer(value(), s);
    }
    if (op2.isInfinite()) {
      return Integer(op2.value(), s);
    }
    return Integer(value()->multiply(*op2.value()));
  }

  ::std::optional<Integer> Integer::divide(const Integer &op2) const
  {
    if (op2.sign() == 0) {
      return ::std::nullopt;
    }
    if (isInfinite() && op2.isInfinite()) {
      return ::std::nullopt;
    }
    if (isInfinite()) {
      return Integer(::std::nullopt, sign() * op2.sign());
    }
    if (op2.isInfinite()) {
      return Integer();
    }
    return Integer(value()->divide(*op2.value()));
  }

  ::std::optional<Integer> Integer::remainder(const Integer &op2) const
  {
    if (op2.sign() == 0) {
      // division by 0 is never allowed
      return ::std::nullopt;
    }
    if (sign() == 0) {
      // 0/(not-zero) -> 0
      return Integer::ZERO();
    }
    if (isInfinite()) {
      // we cannot determine a definite value if we have a indefinite
      // numerators
      return ::std::nullopt;
    }

    if (op2.isInfinite()) {
      // the numerator is fixed and and we know that the denominator
      // is large in absolute terms, so the result is just the
      // numerator less one.
      // e.g. -10 / +inf -> -9
      return towards0();
    }
    return value()->remainder(*op2.value());
  }

  ::std::optional<Integer> Integer::mod(const Integer &op2) const
  {
    if (sign() < 0 || op2.sign() <= 0) {
      return ::std::nullopt;
    }
    if (isInfinite()) {
      return ::std::nullopt;
    }
    if (op2.isInfinite()) {
      return *this;
    }
    return value()->mod(*op2.value());
  }

  Integer Integer::pow(::std::uint32_t op2) const
  {
    if (isInfinite()) {
      if (op2 == 0) {
        return Integer(1);
      }
      // if we have a -inf, then squaring it gives +inf
      return Integer(::std::nullopt, op2 % 2 == 0 ? 1 : sign());
    }
    return value()->pow(op2);
  }

  int Integer::compare(const Integer &op2) const
  {
    if (sign() != op2.sign()) {
      return sign() < op2.sign() ? -1 : 1;
    }
    if (isInfinite() && op2.isInfinite()) {
      return 0;
    }
    if (isInfinite()) {
      return sign();
    }
    if (op2.isInfinite()) {
      return -op2.sign();
    }
    return value()->compare(*op2.value());
  }

  Integer Integer::abs() const
  {
    return Integer(transform([](const BigInt &num) {return num.abs();}), ::std::abs(_sign));
  }

  Integer Integer::min(const Integer &op2) const
  {
    return compare(op2) < 0 ? *this : op2;
  }

  Integer Integer::max(const Integer &op2) const
  {
    return compare(op2) > 0 ? *this : op2;
  }

  Integer Integer::absMin(const Integer &op2) const
  {
    auto cmp = abs().compare(op2.abs());
    if (cmp < 0) {
      return *this;
    }
    if (cmp > 0) {
      return op2;
    }
    return min(op2);
  }

  Integer Integer::absMax(const Integer &op2) const
  {
    auto cmp = abs().compare(op2.abs());
    if (cmp > 0) {
      return *this;
    }
    if (cmp < 0) {
      return op2;
    }
    return max(op2);
  }

  ::std::ostream& Integer::write(::std::ostream &out) const
  {
    if (isInfinite()) {
      if (sign() < 0) {
        return out << "-inf";
      } else {
        return out << "+inf";
      }
    }
    return value()->write(out);
  }

  ::std::string Integer::toString() const
  {
    if (isInfinite()) {
      if (sign() < 0) {
        return "-inf";
      } else {
        return "+inf";
      }
    }
    return value()->toString();
  }

}
