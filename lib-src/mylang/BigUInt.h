#ifndef CLASS_MYLANG_BIGUINT_H
#define CLASS_MYLANG_BIGUINT_H

#ifndef CLASS_MYLANG_BIGINT_H
#include <mylang/BigInt.h>
#endif
#include <iosfwd>
#include <string>

namespace mylang {

  /** A multiprecision integer class */
  class BigUInt
  {
    /** Default constructor with the value 0 */
  public:
    BigUInt();

    /** Constructor with a builtin integer */
  public:
    BigUInt(unsigned long long int value);

    /**
     * Construct from a big integer.
     * @param integer a positive integer
     * @throws ::std::invalid_argument if integer<0
     */
  public:
    BigUInt(const BigInt &integer);

    /**
     * Destructor.
     */
  public:
    ~BigUInt();

    /**
     * Create a big int from a signed integer
     * @param val a value
     * @return a big int
     */
  public:
    static BigUInt signedValueOf(long long int value);

    /**
     * Create a big int from an unsigned integer
     * @param val a value
     * @return a big int
     */
  public:
    static BigUInt unsignedValueOf(unsigned long long int value);

    /**
     * Parse a string as a signed integer.
     * @param str a string
     * @return a bigint
     */
  public:
    static BigUInt parse(const ::std::string &str);

    /**
     * Get a signed integer.
     * @return the signed value
     * @throws Exception if the value does not fit
     */
  public:
    long long int signedValue() const;

    /**
     * Get a unsigned integer.
     * @return the unsigned value
     */
  public:
    unsigned long long int unsignedValue() const;

    /**
     * Add two big ints.
     * @param y an integer
     * @return a new bigint
     */
  public:
    BigUInt add(const BigUInt &op2) const;

    /**
     * Add two big ints.
     * @param y an integer
     * @return a new bigint
     */
  public:
    BigUInt subtract(const BigUInt &op2) const;

    /**
     * Negate this integer.
     * @return a new bigint
     */
  public:
    BigUInt negate() const;

    /**
     * Add two big ints.
     * @param y an integer
     * @return a new bigint
     */
  public:
    BigUInt multiply(const BigUInt &op2) const;

    /**
     * Add two big ints.
     * @param y an integer
     * @return a new bigint
     */
  public:
    BigUInt divide(const BigUInt &op2) const;

    /**
     * Add two big ints.
     * @param y an integer
     * @return a new bigint
     */
  public:
    BigUInt remainder(const BigUInt &op2) const;

    /**
     * Add two big ints.
     * @param y an integer
     * @return a new bigint
     */
  public:
    BigUInt mod(const BigUInt &op2) const;

    /**
     * Min of this integer and that integer.
     * @param y an integer
     * @return a new BigUInt
     */
  public:
    BigUInt min(const BigUInt &op2) const;

    /**
     * Max of this integer and that integer.
     * @param y an integer
     * @return a new BigUInt
     */
  public:
    BigUInt max(const BigUInt &op2) const;

    /**
     * Raise this value to the specified power.
     * @param exp the unsigned exponent
     * @return this ^ exp
     */
  public:
    BigUInt pow(::std::uint32_t exp) const;

    /**
     * Compare
     * @param y an integer
     * @return -1 if this integer is less, 0 if equal, 1 if greater than y
     */
  public:
    int compare(const BigUInt &op2) const;

    /**
     * Write an int to a stream
     * @param out a stream
     * @return the outoput stream
     */
  public:
    ::std::ostream& write(::std::ostream &out) const;

    /**
     * Get the string representation.
     * @return a string
     */
  public:
    ::std::string toString() const;

    /** The operators */
  public:
    inline friend BigUInt operator+(const BigUInt &x, const BigUInt &y)
    {
      return x.add(y);
    }

    inline friend BigUInt operator-(const BigUInt &x, const BigUInt &y)
    {
      return x.subtract(y);
    }

    inline friend BigUInt operator*(const BigUInt &x, const BigUInt &y)
    {
      return x.multiply(y);
    }

    inline friend BigUInt operator/(const BigUInt &x, const BigUInt &y)
    {
      return x.divide(y);
    }

    inline friend BigUInt operator%(const BigUInt &x, const BigUInt &y)
    {
      return x.mod(y);
    }

    inline friend BigUInt operator-(const BigUInt &x)
    {
      return x.negate();
    }

    inline friend bool operator<(const BigUInt &x, const BigUInt &y)
    {
      return x.compare(y) < 0;
    }

    inline friend bool operator<=(const BigUInt &x, const BigUInt &y)
    {
      return x.compare(y) <= 0;
    }

    inline friend bool operator>(const BigUInt &x, const BigUInt &y)
    {
      return x.compare(y) > 0;
    }

    inline friend bool operator>=(const BigUInt &x, const BigUInt &y)
    {
      return x.compare(y) >= 0;
    }

    inline friend bool operator==(const BigUInt &x, const BigUInt &y)
    {
      return x.compare(y) == 0;
    }

    inline friend bool operator!=(const BigUInt &x, const BigUInt &y)
    {
      return x.compare(y) != 0;
    }

    inline friend ::std::ostream& operator<<(::std::ostream &s, const BigUInt &x)
    {
      return x.write(s);
    }

    /** The integer value, which is guaranteed to be positive */
  private:
    BigInt value;
  };

}
#endif
