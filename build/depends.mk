# this makefile fragment defines the standard rules
# that are used to compile, link, and run tests

$(OBJECT_DIR)/%.depend : %.cpp
	@set -e
	@mkdir -p $(dir $@)
	@rm -f $@
	@$(make.depend.cpp)
	@sed 's,\($*\)\.o[ :]*,\1.o $@ : ,g' < $@ > $@.tmp
	@mv $@.tmp $@

$(OBJECT_DIR)/%.depend : %.c
	@set -e
	@mkdir -p $(dir $@)
	@rm -f $@
	@$(make.depend.c)
	@sed 's,\($*\)\.o[ :]*,\1.o $@ : ,g' < $@ > $@.tmp
	@mv $@.tmp $@

-include $(addsuffix .depend,$(basename $(OBJECT_FILES)))

