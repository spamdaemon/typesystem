# run the generators

# we have 2 types of generators, root generators, and make generators
# the root generators are always invoked, and basically and generate files from nothing
# a root generator is an executable that that has the following interface:
# 1. if invoked with no arguments, then it prints the files it generates on stdout
# 2. if invoked with a single argument, then that argument is folder into which generates files are to be placed
ROOT_SOURCE_GENERATORS := $(shell find $(BASE_DIR)/build/generators $(BASE_DIR)/generators -type f -perm /g+x,u+x,o+x 2> /dev/null )

# make generators are make scripts that defines a rule to generator source file
MAKE_SOURCE_GENERATORS := $(shell find  $(BASE_DIR)/build/generators $(BASE_DIR)/generators -type f -name '*.mk' 2> /dev/null )

DUMMY_GENERATORS := $(patsubst $(BASE_DIR)/%,$(OBJECT_DIR)/.generators/%,$(ROOT_SOURCE_GENERATORS))

$(OBJECT_DIR)/.generators/%: $(BASE_DIR)/%
	@mkdir -p "$(GEN_DIR)";
	@"$<" "$(GEN_DIR)";
	@mkdir -p "$(@D)";
	@touch "$@";


# we cannot generator the 
include $(DUMMY_GENERATORS)

# include the make generators directly
include $(MAKE_SOURCE_GENERATORS)
