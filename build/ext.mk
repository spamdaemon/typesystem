# this makefile fragment defines the external libraries
# for the libraries, examples, tests, and binaries
# 
# the following definitions must be output from this file:
# EXT_LIB_SEARCH_PATH : the search paths for libraries
# EXT_LINK_LIBS       : the libraries with which to link (in the proper link order)
# EXT_INC_SEARCH_PATH : the search paths for include files
# EXT_COMPILER_DEFINES : definitions passed to the compiler (without the -D option)
EXT_LIB_SEARCH_PATH :=
EXT_LINK_LIBS       :=
EXT_INC_SEARCH_PATH :=
EXT_COMPILER_DEFINES :=

# this is an optional definition
#EXT_CPP_OPTS := compiler options for c++ compilation
#EXT_C_OPTS := compiler options for c compilation
#EXT_FORTRAN_OPTS := compiler options for fortran compilation

EXT_CPP_OPTS := 
EXT_C_OPTS := 
EXT_FORTRAN_OPTS := 

# the multiprecision library that we need to use
EXT_LINK_LIBS += -lgmp

# CVC5 support
EXT_LIB_SEARCH_PATH += ${HOME}/cvc5/lib64
EXT_LINK_LIBS += -lcvc5
EXT_INC_SEARCH_PATH += ${HOME}/cvc5/include
EXT_COMPILER_DEFINES += HAVE_CVC5=1

