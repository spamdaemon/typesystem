
# check if a file or directory exists
FILE_EXISTS=$(if $(shell find "$(1)" -mindepth 0 -maxdepth 0 -type f 2>/dev/null),1,0)
DIR_EXISTS=$(if $(shell find "$(1)" -mindepth 0 -maxdepth 0 -type d 2>/dev/null),1,0)

# update the external include and library search paths
APPEND_VARIABLE=$(if $(1),$(2)+=$(3),)

define UPDATE_COMMON=
ifeq ($($(1)),1)
EXT_INC_SEARCH_PATH+=$(2)
EXT_LIB_SEARCH_PATH+=$(3)
EXT_LINK_LIBS+=$(4)
endif
EXT_COMPILER_DEFINES+=$(1)=$($(1))
endef

