# this makefile fragment defines the standard rules
# that are used to compile, link, and run tests
$(BASE_LIB) : $(LIB_OBJECT_FILES)
	@mkdir -p $(dir $@)
	$(create.so)

$(TEST_LIB) : $(TESTLIB_OBJECT_FILES)
	@mkdir -p $(dir $@)
	$(create.so)

$(BASE_DIR)/examples/% : $(OBJECT_DIR)/examples-src/%.o $(BASE_LIB)
	@mkdir -p $(dir $@)
	$(link.bin)

$(BASE_DIR)/bin/% : $(OBJECT_DIR)/bin-src/%.o $(BASE_LIB)
	@mkdir -p $(dir $@)
	$(link.bin)

# initialize the coverage info file; this should be done
# before running the tests
$(COVERAGE_INFO) : 
	@rm -f $@;
	@lcov --zerocounters -o $@;

$(BASE_DIR)/tests/% : $(OBJECT_DIR)/tests-src/%.o $(BASE_LIB)
	@mkdir -p $(dir $@)
	$(link.test)

$(BASE_DIR)/tests/scripts/%.passed : $(BASE_DIR)/tests/scripts/% $(BASE_LIB)  $(BINARIES)
	@rm -f "$@" "$<.failed" "$<.log"
	@mkdir -p "$(dir $@)"
	@if "$<" > "$<.log" 2>&1; then \
	    echo "Test $< passed";\
	    echo "PASSED $<" > "$@";\
        else\
            echo "Test $< failed";\
            echo "Test $< failed" > "$<.failed";\
            false;\
        fi;

$(BASE_DIR)/tests/scripts/% : $(BASE_DIR)/test-scripts/% 
	@mkdir -p $(dir $@);
	@cp "$<" "$@";
	@chmod ugo+x "$@";

$(BASE_DIR)/tests/%.passed : $(BASE_DIR)/tests/% $(MODULES) $(TEST_LIB) $(BINARIES)
	@rm -f "$@" "$<.failed" "$<.log"
	@mkdir -p "$(dir $@)"
	@$(doTest) > "$<.log" 2>$<.stderr; \
	if [ $$? -eq 0 ]; then \
		echo "Test $< passed";\
		echo "PASSED $<" > "$@";\
	else\
		echo "Test $< failed";\
		echo "Test $< failed" > "$<.failed";\
	fi;



