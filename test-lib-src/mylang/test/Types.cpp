#include <mylang/test/Types.h>
#include <mylang/typesystem/exprs/exprs.h>
#include <mylang/typesystem/types/types.h>
#include <mylang/typesystem/ast/AFunction.h>
#include <mylang/typesystem/TypeDefinition.h>

namespace mylang::test {
  using namespace mylang::typesystem;
  using namespace mylang::typesystem::exprs;
  using namespace mylang::typesystem::types;
  using namespace mylang::typesystem::ast;

  const AFunctionName BoolType::t("bool.true");
  const AFunctionName BoolType::f("bool.false");
  const AFunctionName BoolType::NOT("bool.not");
  TypeDefinitionPtr BoolType::putDefinition(AstEnvironment &env)
  {
    auto def = env.putT(getDefinition());
    env.putF(t,
        AFunction::of(AType::ofPattern(def->callConstructor( { BLiteral::of(true) })), { },
            nullptr));
    env.putF(f,
        AFunction::of(AType::ofPattern(def->callConstructor( { BLiteral::of(false) })), { },
            nullptr));

    {
      const VarPtr boolIn = VarExpr::of(BType::create());
      const VarPtr boolOut = VarExpr::of(boolIn->type);

      env.putF(NOT,
          AFunction::ofUnary(AType::ofPattern(def->callConstructor()),
              { AType::ofPattern(def->callConstructor()) }, [&](VarPtr ret, VarPtr arg) {
                return BuiltinExpr::allOf( {
                      BuiltinExpr::eqOf(ret,def->callConstructor( {boolOut})),
                      BuiltinExpr::eqOf(arg,def->callConstructor( {boolIn})),
                      BuiltinExpr::eqOf(boolOut,BuiltinExpr::notOf(boolIn))}
                );
              }));
    }

    return def;
  }

  ExprPtr BoolType::getValue(ExprPtr e)
  {
    return getDefinition()->callConstructor( { e });
  }

  ExprPtr BoolType::getTrue()
  {
    return getValue(BLiteral::of(true));
  }
  ExprPtr BoolType::getFalse()
  {
    return getValue(BLiteral::of(false));
  }

  TypeDefinitionPtr BoolType::getDefinition()
  {
    const VarPtr b = VarExpr::of(BType::create());
    return TypeDefinition::of("bool", { b });
  }

  const AFunctionName BitType::on("bit.on");
  const AFunctionName BitType::off("bit.off");
  const AFunctionName BitType::NOT("bit.not");
  TypeDefinitionPtr BitType::putDefinition(AstEnvironment &env)
  {
    auto def = env.putT(getDefinition());
    env.putF(on,
        AFunction::of(AType::ofPattern(def->callConstructor( { BLiteral::of(true) })), { },
            nullptr));
    env.putF(off,
        AFunction::of(AType::ofPattern(def->callConstructor( { BLiteral::of(false) })), { },
            nullptr));

    {
      const VarPtr boolIn = VarExpr::of(BType::create());
      const VarPtr boolOut = VarExpr::of(boolIn->type);

      env.putF(NOT,
          AFunction::ofUnary(AType::ofPattern(def->callConstructor()),
              { AType::ofPattern(def->callConstructor()) }, [&](VarPtr ret, VarPtr arg) {
                return BuiltinExpr::allOf( {
                      BuiltinExpr::eqOf(ret,def->callConstructor( {boolOut})),
                      BuiltinExpr::eqOf(arg,def->callConstructor( {boolIn})),
                      BuiltinExpr::eqOf(boolOut,BuiltinExpr::notOf(boolIn))}
                );
              }));
    }

    return def;
  }

  TypeDefinitionPtr BitType::getDefinition()
  {
    const VarPtr b = VarExpr::of(BType::create());
    return TypeDefinition::of("bit", { b });
  }

  ExprPtr BitType::getValue(ExprPtr e)
  {
    return getDefinition()->callConstructor( { e });
  }

  ExprPtr BitType::getOn()
  {
    return getValue(BLiteral::of(true));
  }
  ExprPtr BitType::getOff()
  {
    return getValue(BLiteral::of(false));
  }

  const VarPtr IntegerType::min = VarExpr::of(IType::create());
  const VarPtr IntegerType::max = VarExpr::of(min->type);
  const AFunctionName IntegerType::zero("integer.zero");
  const AFunctionName IntegerType::one("integer.one");
  const AFunctionName IntegerType::integer("integer");
  const AFunctionName IntegerType::value("integer.value");
  const AFunctionName IntegerType::succ("integer.succ");
  const AFunctionName IntegerType::pred("integer.pred");
  const AFunctionName IntegerType::maybeZero("integer.maybeZero");
  const AFunctionName IntegerType::nonZero("integer.nonZero");

  TypeDefinitionPtr IntegerType::putDefinition(AstEnvironment &env)
  {
    auto def = env.putT(getDefinition());

    // the integers 0 and 1
    env.putF(zero, AFunction::of(AType::ofPattern(getValue(0)), { }, nullptr));
    env.putF(one, AFunction::of(AType::ofPattern(getValue(1)), { }, nullptr));

    // general constructor
    env.putF(integer,
        AFunction::ofBinary(AType::ofPattern(def->callConstructor( { min, max })),
            AType::of(min->type), AType::of(min->type), [&](VarPtr ret, VarPtr xmin, VarPtr xmax) {
              return BuiltinExpr::allOf( {
                    BuiltinExpr::lteOf(xmin,xmax),
                    BuiltinExpr::eqOf(ret,getBounded(xmin, xmax))});
            }));

    // singleton constructor for an integer corresponding to a single value
    {
      auto v = VarExpr::of(IType::create());
      env.putF(value,
          AFunction::of(AType::ofPattern(def->callConstructor( { v, v })), { }, nullptr));
    }

    // succ and pred functions
    {
      const VarPtr minIN = VarExpr::of(IType::create());
      const VarPtr maxIN = VarExpr::of(min->type);
      const VarPtr minOUT = VarExpr::of(min->type);
      const VarPtr maxOUT = VarExpr::of(min->type);

      env.putF(succ, AFunction::ofUnary(AType::of(def), AType::of(def),
      // do the constraint
          [&](VarPtr ret, VarPtr arg) {
            return BuiltinExpr::allOf( {
                  BuiltinExpr::eqOf(ret,getBounded(minOUT, maxOUT)),
                  BuiltinExpr::eqOf(arg,getBounded(minIN, maxIN)),
                  BuiltinExpr::eqOf(minOUT,BuiltinExpr::sumOf(minIN,ILiteral::of(1))),
                  BuiltinExpr::eqOf(maxOUT,BuiltinExpr::sumOf(maxIN,ILiteral::of(1))),
                });
          }

          ));
      env.putF(pred, AFunction::ofUnary(AType::of(def), AType::of(def),
      // do the constraint
          [&](VarPtr ret, VarPtr arg) {
            return BuiltinExpr::allOf( {
                  BuiltinExpr::eqOf(ret,getBounded(minOUT, maxOUT)),
                  BuiltinExpr::eqOf(arg,getBounded(minIN, maxIN)),
                  BuiltinExpr::eqOf(minOUT,BuiltinExpr::sumOf(minIN,ILiteral::of(-1))),
                  BuiltinExpr::eqOf(maxOUT,BuiltinExpr::sumOf(maxIN,ILiteral::of(-1))),
                });
          }

          ));
    }

    {
      const VarPtr minIN = VarExpr::of(IType::create());
      const VarPtr maxIN = VarExpr::of(min->type);
      const auto zero = ILiteral::of(0);

      const auto retTy = BoolType::putDefinition(env)->getConstructorPattern();

      env.putF(maybeZero, AFunction::ofUnary(AType::of(retTy), AType::of(def),
      // do the constraint
          [&](VarPtr ret, VarPtr arg) {
            return BuiltinExpr::allOf( {
                  BuiltinExpr::eqOf(arg,getBounded(minIN, maxIN)),
                  BuiltinExpr::eqOf(ret,BoolType::getValue(BuiltinExpr::allOf( {
                                BuiltinExpr::lteOf(minIN,zero),
                                BuiltinExpr::lteOf(zero,maxIN)})))
                });
          }

          ));

    }

    {
      const VarPtr minIN = VarExpr::of(IType::create());
      const VarPtr maxIN = VarExpr::of(min->type);
      const auto zero = ILiteral::of(0);

      const auto retTy = BoolType::putDefinition(env)->getConstructorPattern();

      env.putF(nonZero, AFunction::ofUnary(AType::of(retTy), AType::of(def),
      // do the constraint
          [&](VarPtr ret, VarPtr arg) {
            return BuiltinExpr::allOf( {
                  BuiltinExpr::eqOf(arg,getBounded(minIN, maxIN)),
                  BuiltinExpr::eqOf(ret,BoolType::getValue(BuiltinExpr::anyOf( {
                                BuiltinExpr::gtOf(minIN,zero),
                                BuiltinExpr::gtOf(zero,maxIN)})))
                });
          }

          ));

    }

    return def;
  }
  TypeDefinitionPtr IntegerType::getDefinition()
  {

    auto constraints = BuiltinExpr::lteOf(min, max);

    return TypeDefinition::of("integer", { //
        min, max //
        }, BuiltinExpr::lteOf(min, max));
  }

  ExprPtr IntegerType::getUnbounded()
  {
    auto def = getDefinition();
    return def->callConstructor();
  }

  ExprPtr IntegerType::getBounded(ExprPtr xmin, ExprPtr xmax)
  {
    if (xmin == nullptr) {
      xmin = VarExpr::of(min->type);
    }
    if (xmax == nullptr) {
      xmax = VarExpr::of(max->type);
    }
    if (xmin->type->unifiable(xmax->type) == false || xmin->type->unifiable(min->type) == false) {
      throw ::std::invalid_argument("Bad argument types");
    }
    auto def = getDefinition();
    return def->callConstructor( { xmin, xmax });
  }

  ExprPtr IntegerType::getBounded(Integer xmin, Integer xmax)
  {
    return getBounded(ILiteral::of(xmin), ILiteral::of(xmax));
  }

  ExprPtr IntegerType::getValue(Integer v)
  {
    return getBounded(v, v);
  }

  ExprPtr IntegerType::getUnsigned()
  {
    auto def = getDefinition();
    return def->callConstructor( { ILiteral::of(Integer::ZERO()) });
  }

  ExprPtr IntegerType::getUInt8()
  {
    return getBounded(Integer::ZERO(), Integer(255));
  }

  ExprPtr IntegerType::getInt8()
  {
    return getBounded(Integer(-128), Integer(127));
  }

  ExprPtr IntegerType::getIntCmpResult()
  {
    return getBounded(Integer(-1), Integer(1));
  }

  ExprPtr IntegerType::getIntBool()
  {
    return getBounded(Integer(0), Integer(1));
  }

  const VarPtr ArrayType::elem = VarExpr::of(XType::create(WType::create()));
  const VarPtr ArrayType::length = VarExpr::of(IntegerType::getUnbounded()->type);
  const AFunctionName ArrayType::empty("array.empty");
  const AFunctionName ArrayType::singleton("array.singleton");
  const AFunctionName ArrayType::size("array.size");
  const AFunctionName ArrayType::head("array.head");
  const AFunctionName ArrayType::tail("array.tail");
  const AFunctionName ArrayType::cons("array.cons");

  TypeDefinitionPtr ArrayType::putDefinition(AstEnvironment &env)
  {
    auto def = env.putT(getDefinition());
    auto intDef = IntegerType::putDefinition(env);

    // create an empty array
    env.putF(empty, AFunction::of(
    // return type is an empty array
        AType::ofPattern(def->callConstructor( { elem, IntegerType::getValue(0) })), { }, nullptr));

    // create an singleton array
    env.putF(singleton, AFunction::of(
    // return type is an empty array
        AType::ofPattern(def->callConstructor( { elem, IntegerType::getValue(1) })), {
            AType::ofPattern(elem)

        }, nullptr));

    // get the length function
    env.putF(size,
        AFunction::of(AType::ofPattern(length), { AType::ofPattern(def->callConstructor( { elem,
            length })) }, nullptr));

    // get the head of the array
    env.putF(head, AFunction::of(AType::ofPattern(elem), { AType::ofPattern(def->callConstructor( {
        elem, length })) },
    // TODO: need the constraint that length > 0
        nullptr));

    // get the head of the array
    {
      const VarPtr newLength = VarExpr::of(length->type);
      env.putF(tail, AFunction::of(
      // return type
          AType::ofPattern(def->callConstructor( { elem, newLength })),

          { AType::ofPattern(def->callConstructor( { elem, length })) },
          // TODO: need the constraint that length > 0 && newLength = pred(length)
          nullptr));
    }

    // cons an element to the array
    {
      const VarPtr newLength = VarExpr::of(length->type);
      env.putF(cons, AFunction::of(
      // return type
          AType::ofPattern(def->callConstructor( { elem, newLength })),
          //
          {
          // input array
              AType::ofPattern(def->callConstructor( { elem, length })),
              // element to append
              AType::ofPattern(elem) //
          },
          // TODO: need the constraint that length > 0 && newLength = pred(length)
          nullptr));
    }

    return def;
  }
  TypeDefinitionPtr ArrayType::getDefinition()
  {

    ExprPtr constraint = BuiltinExpr::eqOf(length, IntegerType::getDefinition()->callConstructor());

    return TypeDefinition::of("array", { //
        elem, length   //
        }, constraint);
  }
  ExprPtr ArrayType::getUnbounded()
  {
    auto def = getDefinition();
    return def->callConstructor();
  }

  ExprPtr ArrayType::getBounded(TypeExprPtr length)
  {
    auto def = getDefinition();
    return def->callConstructor( { elem, length });
  }

  const VarPtr OptType::constructor = VarExpr::of(TType::create());
  const VarPtr OptType::elem = VarExpr::of(XType::create(WType::create()));
  const AFunctionName OptType::none("optional.none");
  const AFunctionName OptType::some("optional.some");
  const AFunctionName OptType::isNone("optional.isNone");
  const AFunctionName OptType::isSome("optional.isSome");

  TypeDefinitionPtr OptType::putDefinition(AstEnvironment &env)
  {
    auto def = env.putT(getDefinition());
    auto boolDef = env.putT(BoolType::putDefinition(env));

    auto noneE = def->callConstructor( { TLiteral::of("none"), elem });
    auto someE = def->callConstructor( { TLiteral::of("some"), elem });

    // None
    env.putF(OptType::none, AFunction::of(AType::ofPattern(noneE), { }, nullptr));
    // Some
    env.putF(OptType::some,
        AFunction::of(AType::ofPattern(someE), { AType::ofPattern(elem) }, nullptr));
    // isSome/isNone
    env.putF(OptType::isNone,
        AFunction::ofUnary(AType::of(boolDef), { AType::of(def) }, [&](VarPtr res, VarPtr arg) {
          return BuiltinExpr::allOf( {
                BuiltinExpr::eqOf(arg,noneE),
                BuiltinExpr::eqOf(res,boolDef->callConstructor())});
        }));
    env.putF(OptType::isSome,
        AFunction::ofUnary(AType::of(boolDef), { AType::of(def) }, [&](VarPtr res, VarPtr arg) {
          return BuiltinExpr::allOf( {
                BuiltinExpr::eqOf(arg,someE),
                BuiltinExpr::eqOf(res,boolDef->callConstructor())});
        }));

    return def;
  }
  TypeDefinitionPtr OptType::getDefinition()
  {
    return TypeDefinition::of("optional", { constructor, elem });
  }

}
