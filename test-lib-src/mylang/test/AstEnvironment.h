#ifndef TEST_ASTENVIRONMENT_H
#define TEST_ASTENVIRONMENT_H

#include <mylang/typesystem/ast/AEnvironment.h>
#include <map>

namespace mylang::test {

  struct AstEnvironment: public ::mylang::typesystem::ast::AEnvironment
  {

    AstEnvironment();

    ~AstEnvironment();

    inline static ::std::shared_ptr<AstEnvironment> of()
    {
      return ::std::make_shared<AstEnvironment>();
    }

    Ptr replaceEnvironment(::mylang::typesystem::Environment e) const
    {
      auto newEnv = of();
      newEnv->functions = functions;
      newEnv->properties = properties;
      newEnv->env = e;
      return newEnv;
    }

    ::mylang::typesystem::ast::AFunction::Ptr lookupF(
        const ::mylang::typesystem::ast::AFunctionName &name) const override;
    ::mylang::typesystem::ast::AProperty::Ptr lookupP(
        const ::mylang::typesystem::ast::AFunctionName &name,
        const ::std::vector<::mylang::typesystem::Pattern> &names) const override;

    ::mylang::typesystem::Environment getEnvironment() const override;

    ::mylang::typesystem::TypeDefinitionPtr putT(::mylang::typesystem::TypeDefinitionPtr ty);

    ::mylang::typesystem::ast::AFunctionSpec::Ptr putF(
        ::mylang::typesystem::ast::AFunctionName name, ::mylang::typesystem::ast::AFunction::Ptr);

    ::std::map<::mylang::typesystem::ast::AFunctionName, ::mylang::typesystem::ast::AFunction::Ptr> functions;

    ::std::multimap<::mylang::typesystem::ast::AFunctionName,
        ::mylang::typesystem::ast::AProperty::Ptr> properties;

    ::mylang::typesystem::Environment env;
  };

}
#endif
