#include <mylang/test/AstEnvironment.h>
#include <mylang/typesystem/Pattern.h>
#include <mylang/typesystem/exprs/TypeExpr.h>
#include <mylang/typesystem/exprs/VarExpr.h>
#include <mylang/typesystem/TypeDefinition.h>
#include <stdexcept>

namespace mylang::test {
  using namespace ::mylang::typesystem;
  using namespace ::mylang::typesystem::ast;
  using namespace ::mylang::typesystem::exprs;

  AstEnvironment::AstEnvironment()
  {
  }
  AstEnvironment::~AstEnvironment()
  {
  }

  Environment AstEnvironment::getEnvironment() const
  {
    return env;
  }

  TypeDefinitionPtr AstEnvironment::putT(TypeDefinitionPtr ty)
  {
    env.addType(ty->name, ty);
    return ty;
  }

  AFunctionSpec::Ptr AstEnvironment::putF(AFunctionName name, AFunction::Ptr v)
  {
    functions.emplace(name, v);
    return AFunctionSpec::of(::std::move(name), v->signature);
  }

  AFunction::Ptr AstEnvironment::lookupF(const AFunctionName &name) const
  {
    auto i = functions.find(name);
    if (i == functions.end()) {
      throw ::std::out_of_range("No such function");
    }
    return i->second;
  }

  AProperty::Ptr AstEnvironment::lookupP(const AFunctionName &name,
      const ::std::vector<Pattern> &types) const
  {
    auto i = properties.lower_bound(name);
    auto j = properties.upper_bound(name);

    AProperty::Ptr res;
    while (i != j) {
      auto p = i->second->function;
      if (p->signature->parameters.size() == types.size()) {
        bool allMatch = true;
        for (size_t k = 0; k < types.size(); ++k) {
          if (!types.at(k).matches(p->signature->parameters.at(k)->pattern)) {
            allMatch = false;
            break;
          }
        }
        if (allMatch) {
          if (res) {
            // ambiguous match
            throw ::std::out_of_range("Ambiguous match for property " + name.toString());
          }
          res = i->second;
        }
      }
      ++i;
    }
    if (!res) {
      throw ::std::out_of_range("No such property " + name.toString());
    }
    return res;
  }

}
