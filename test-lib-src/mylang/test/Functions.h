#ifndef MYLANG_TEST_FUNCTIONS_H
#define MYLANG_TEST_FUNCTIONS_H

#include <mylang/typesystem/ast/AFunction.h>
#include <mylang/test/AstEnvironment.h>

namespace mylang::test {

  /**
   * Register the equality function. This is generate equality
   */
  ::mylang::typesystem::ast::AFunctionSpec::Ptr registerEq(AstEnvironment &env);

  /**
   * Register the assignment function.
   */
  ::mylang::typesystem::ast::AFunctionSpec::Ptr registerAssign(AstEnvironment &env);

}
#endif
