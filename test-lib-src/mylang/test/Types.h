#ifndef MYLANG_TEST_TYPES_H
#define MYLANG_TEST_TYPES_H

#include <mylang/Integer.h>
#include <mylang/typesystem/exprs/VarExpr.h>
#include <mylang/typesystem/typesystem.h>
#include <mylang/typesystem/exprs/VarExpr.h>
#include <mylang/test/AstEnvironment.h>

namespace mylang::test {
  using ::mylang::typesystem::ExprPtr;
  using ::mylang::typesystem::VarPtr;
  using ::mylang::typesystem::TypeExprPtr;
  using ::mylang::typesystem::TypeDefinitionPtr;
  using ::mylang::typesystem::ast::AFunctionName;

  struct BoolType
  {
    static const AFunctionName t; // true
    static const AFunctionName f; // false
    static const AFunctionName NOT; // false
    static TypeDefinitionPtr putDefinition(AstEnvironment &env);
    static TypeDefinitionPtr getDefinition();
    static ExprPtr getValue(ExprPtr value);
    static ExprPtr getTrue();
    static ExprPtr getFalse();

  };

  struct BitType
  {
    static const AFunctionName on;
    static const AFunctionName off;
    static const AFunctionName NOT; // false
    static TypeDefinitionPtr putDefinition(AstEnvironment &env);
    static TypeDefinitionPtr getDefinition();
    static ExprPtr getValue(ExprPtr value);
    static ExprPtr getOn();
    static ExprPtr getOff();
  };

  struct IntegerType
  {
    static const VarPtr min;
    static const VarPtr max;
    static const AFunctionName zero;
    static const AFunctionName one;
    static const AFunctionName integer;
    static const AFunctionName value;
    static const AFunctionName pred;
    static const AFunctionName succ;

    // a function that returns true if the value of the integer
    // may be zero
    static const AFunctionName maybeZero;
    static const AFunctionName nonZero;

    // get the default definition of an integer
    static TypeDefinitionPtr putDefinition(AstEnvironment &env);
    static TypeDefinitionPtr getDefinition();
    // get a bounded integer
    static ExprPtr getUnbounded();
    // a specific value
    static ExprPtr getValue(Integer value);
    static ExprPtr getBounded(ExprPtr min = nullptr, ExprPtr max = nullptr);
    static ExprPtr getBounded(Integer min, Integer max);
    // get the constraints for an unsigned integer
    static ExprPtr getUnsigned();
    // get the constraints for an 8 bit unsigned integer
    static ExprPtr getUInt8();
    // get the constraints for an 8 bit signed integer
    static ExprPtr getInt8();
    // get the constraints for the results of a comparison (-1,0,1)
    static ExprPtr getIntCmpResult();
    // get the constraints for an integer that is a boolean (0,1)
    static ExprPtr getIntBool();

  };

  struct ArrayType
  {
    static const VarPtr length;
    static const VarPtr elem;
    static const AFunctionName empty;
    static const AFunctionName singleton;
    static const AFunctionName size;
    static const AFunctionName head;
    static const AFunctionName tail;
    static const AFunctionName cons;

    static TypeDefinitionPtr putDefinition(AstEnvironment &env);
    static TypeDefinitionPtr getDefinition();
    static ExprPtr getUnbounded();
    static ExprPtr getBounded(TypeExprPtr length);
  };
  struct OptType
  {
    static const VarPtr constructor;
    static const VarPtr elem;
    static const AFunctionName none;
    static const AFunctionName some;
    static const AFunctionName isNone;
    static const AFunctionName isSome;
    static TypeDefinitionPtr putDefinition(AstEnvironment &env);
    static TypeDefinitionPtr getDefinition();
  };
}
#endif
