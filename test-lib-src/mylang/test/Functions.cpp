#include <mylang/test/Functions.h>
#include <mylang/typesystem/TypeDefinition.h>
#include <mylang/typesystem/exprs/exprs.h>
#include <mylang/typesystem/types/types.h>

#include <mylang/test/Types.h>

using namespace mylang::typesystem;
using namespace mylang::typesystem::ast;
using namespace mylang::typesystem::exprs;
using namespace mylang::typesystem::types;

namespace mylang::test {

  // a generate eq function that works on all types
  // the EQ function:
  AFunctionSpec::Ptr registerEq(AstEnvironment &env)
  {
    // eq :: a(m::w) -> a(n::w) -> boolTy()
    auto a = VarExpr::of(TType::create());
    auto w = WType::create();
    auto m = VarExpr::of(w);
    auto n = VarExpr::of(w);

    auto resTy = AType::of(BoolType::putDefinition(env)->getConstructorPattern());

    // lhs = a(m::w)
    // rhs = a(n::w)

    auto lhs = AType::ofPattern(TypeExpr::of(a, m));
    auto rhs = AType::ofPattern(TypeExpr::of(a, n));

    return env.putF("eq", AFunction::ofBinary( //
        resTy, //
        rhs, //
        lhs, //
        [&](VarPtr, VarPtr l, VarPtr r) {
          // we can always two instances of a type for equality
            return BLiteral::of(true);
          }
          ));
  }

  // a generate eq function that works on all types
  // the EQ function:
  AFunctionSpec::Ptr registerAssign(AstEnvironment &env)
  {
    // eq :: a(m::w) -> a(n::w) -> boolTy()
    auto a = VarExpr::of(TType::create());
    auto w = WType::create();
    auto m = VarExpr::of(w);
    auto n = VarExpr::of(w);

    auto resTy = AType::of(BoolType::putDefinition(env)->getConstructorPattern());

    // lhs = a(m::w)
    // rhs = a(n::w)

    auto lhs = AType::ofPattern(TypeExpr::of(a, m));
    auto rhs = AType::ofPattern(TypeExpr::of(a, n));

    return env.putF("assign", AFunction::ofBinary( //
        resTy, //
        rhs, //
        lhs, //
        [&](VarPtr, VarPtr l, VarPtr r) {
          return BuiltinExpr::eqOf(l,r);
        }
        ));
  }

}
